function [p,Re,Ge,fe,Gam_td] = fitpt(Dd,B,R,N,dt)

Wft = flattrian(N*2, 0.5);% create flat triangular window
Wft1 = zeros(N+1,1);
Wft1(1:N) = Wft(N:2*N-1);
% Rw = wincor(R, Wft1);%window the empirical CF matrix
% [f0, G0] = cor2spec(Rw, dt); %convert empirical CF matrix to full spectrum SD
    
Dc = log(Dd)/dt;
% estimate the modal participation and CF matrix (time domain)
% B2 are the unit normalized modes from B
[~,Gam1,Re] = estgamt(R,B,Dd);
% sort the participation vectors by frequency
[~, Gam_td, ~] = modsort(Dc, Gam1);

%calculate the TD estimated SD matrix (for comparison with G0)
%basically an SVD of the full estimated CF matrix (Re)
[fe, Ge] = cor2spec(wincor(Re, Wft1), dt);

% calculate participation FACTOR
% (p) from cont poles and participation vectors, both time and
% freq domains
pa = sqrt(diag(Gam_td'*Gam_td)); % absolute modal participation
pr = 100*(pa'.^2)/(pa'*pa); % relative modal participation (pi)
p = [pr;pa'];