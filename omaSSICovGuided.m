function [modObj, spec] = omaSSICovGuided(y,dt,settings,targetModes,targetI,varargin)
% Encapsulating function for all variations of OMA via Stochastic Subpace
% Identification

%% Handle inputs
y = y'; %function convention is for channel rows rather than channel columns

sz = numel(varargin);
if mod(sz,2) ~= 0
    error('Optional arguments must have valid Name, Value pairs');
end

%defaults
UNCALC = false;
PFCALC  = false;
MORSOLVE = false;

%assignments
for k = 1:2:(sz-1)
    switch varargin{k}
        case 'uncert'
            UNCALC = varargin{k+1}; %perform uncertainty calculation
        case 'partfac'
            PFCALC = varargin{k+1}; %calculate participation factors
        case 'reduction'
            MORSOLVE = varargin{k+1}; %reduced order modelling
        case 'plot'
            PLT = varargin{k+1}; %plot results
    end
end

%% ====================== SSI-COV ======================
% STEP 0: build spectral density matrices
% Note: these are not used in the main algorithm, but are used for
% stabilization diagram display and participation factor calculations

Gtimer = tic;
[fy,Gy] = fftspec(y, settings.N, dt); % full spectrum SD using welch method
% [fhy, Ghy] = halfspecw(y_norm, N, dt); %empirical half spectrum SD matrix
% NOTE: Both Gy and Ghy are similar, however the columns of Ghy are
% proportional to the modeshapes, thus is better for FD estimation
disp(['SD Matrix calculation (s): ' num2str(toc(Gtimer))]);

%projection channel SD matrix (used for stabilization diagram)
Gp = Gy(settings.projChan,:,:);

% Calculate the projection channel SD matrix SVD
[ns,~,nf] = size(Gp);
sv = zeros(nf,ns);
for k=1:nf
    [~,S,~] = svd(Gp(:,:,k));
    if ns ~= 1
        sv(k, :) = diag(S)';
    else
        sv(k, :) = S(1);
    end
end

% Assign to spectrum structure
if size(y,1) > 3
    nsv = min(numel(settings.projChan),4);
    spec.P = sv(:,1:nsv);
else
    spec.P = sv(:,1:size(y,1));
end
spec.f = fy;
if isfield(settings,'units')
    spec.units = ['(' settings.units '^2)/Hz'];
else
    spec.units = '[-]';
end

if PFCALC %if participation factor calculation is desired
    %Calculate the empirical CF matrix (used for participation
    %factor estimation)
    Rtimer = tic;
    R = dircor(y, settings.N*2); %filtered positive lag correlation (CF Matrix)
    % NOTE: Is there an option to average R's to generate cleaner CF matrices??
    % NOTE 2: This takes a long time, can it be more efficient?
    disp(['CF Matrix calculation (s): ' num2str(toc(Rtimer))])
end


% STEP 1: build the covariance matrix
if UNCALC
    [Hcov,ddim,hdim,Tcov] = buildHcov(y,...
        settings.blockRows,...
        settings.projChan,...
        settings.nb);
else
    [Hcov,ddim,hdim] = buildHcov(y,...
        settings.blockRows,...
        settings.projChan);
end

%STEP 2: Iterate through the model orders and implement SSI-cov, match
%modes against target modes, and if thresholds are not satisfied, continue
%to the next model order.
modeRange = settings.modeRange; %vector of mode quantities to solve
nIter = numel(modeRange); %number of iterations

% initialize the modal solution
modObj = modeobj(targetModes.nModes,size(y,1));
mode_err = ones(targetModes.nModes,1);
assigned = false(targetModes.nModes,1);
f_th = 0.02;
z_th = 0.5;
imac_th = 0.3;

if PLT
    figure
    ax1 = axes;
    hold on
end

if MORSOLVE
    %Computationally efficient model reduction approach
    
    b = settings.blockRows; %number of blocks
    ssiTimer = tic;
    % Run the SSI/ref cov algorithm for largest m
    m = max(modeRange);
    [A,C,aux] = ssicov(Hcov,m,ddim,hdim);
    
    % Construct a state space matrix
    %sys = ss(A,aux.G_ref,C,0,1);
    
    % Form the observability gramian
    %Q1  = gram(sys,'o');
    Q2 = dlyap(A',C'*C); %
    % The last line is just to make sure computations are correct.
    % Q1 should be equal to Q2, which is the case,
    %disp(norm(Q1-Q2)/norm(Q1))
    
    % sval, singular values of Q, only depend on A and C.
    % hsv, Hankel singualr values, depend on A, C, and Gref.
    sval = svd(Q2); sval = sval/sval(1);
    
    %     figure
    %     semilogy(sval,'-*')
    %     title('SV decay');
    %     xlabel('Singular Value #');
    %     ylabel('Singular Value');
    
    % We will now reduce the system and compute the poles
    % using Q1 (only depends on A and C) and
    % using sys (depend on A, C, and Gref)
    
    [V,~,~]=svd(Q2);
    
    % In these options we can specify frequency range etc.
    %OPT = balredOptions('StateElimMethod','Truncate');
    
    for k = 1:nIter %iteration index
        n = modeRange(k)*2; %SVs are twice the number of poles (complex conjugates)
        
        disp(['Block Row Size: ' num2str(b) ', No. of SVs: ' num2str(n)...
            ' Option: Reduction'])
        
        % poles from Q1 ( A and C only)
        Vr = V(:,1:n);
        Ar = Vr'*A*Vr; %reduced A matrix
        Cr = C*Vr;
        
        % poles from A, C, and Gref
        %         sys2 = balred(sys,n,OPT);
        %         Ar = sys2.A;
        %         Cr = sys2.C;
        
        % Convert to modal parameters
        [fz,U] = ss2modal(Ar,Cr,dt);
        
        if PLT
            plot(ax1,fz(1,:),repmat(n/2,size(fz,2)),'.b','MarkerSize', 10)
        end
        
        % Calculate differences and update
        for i = 1:targetModes.nModes
            f_diff = fz(1,:)-targetModes.f(i);
            [~,matchI] = min(abs(f_diff));
            f_erri = abs(fz(1,matchI)-targetModes.f(i))/targetModes.f(i);
            z_erri = abs(fz(2,matchI)-targetModes.z(i))/targetModes.z(i);
            imaci = 1-calculateMAC(U(targetI,matchI),targetModes.u(:,i));
            better = mode_err(i) > imaci;
            
            if PLT
                if f_erri < f_th
                    plot(ax1,fz(1,matchI),n/2,'b+')
                end
                if z_erri < z_th
                    plot(ax1,fz(1,matchI),n/2,'bx')
                end
                if imaci < imac_th
                    plot(ax1,fz(1,matchI),n/2,'bs')
                end
            end
            
            if f_erri < f_th && z_erri < z_th && imaci < imac_th && better
                modObj.f(i) = fz(1,matchI);
                modObj.z(i) = fz(2,matchI);
                modObj.u(:,i) = U(:,matchI);
                mode_err(i) = imaci;
                assigned(i) = true;
                
                if PLT
                    plot(ax1,fz(1,matchI),n/2,'ko','MarkerSize',8)
                end
            end
        end
        
        
        if PFCALC
            %Calculate the discrete-time poles
            [Dd,Vd] = ss2dtp(Ar,Cr);
            
            % fit the participation factors (time domain)
            [pt,Re,Ge,fe,Gam_td] = fitpt(Dd,Vd,R,N,dt);
            
            % Assign to the modeobj
            modObj.pf(i) = pt(matchI);
        end
        
        if sum(assigned) == numel(assigned)
            %all modes have been assigned
            break
        end
    end
    disp(['Computation time (s): ' num2str(toc(ssiTimer))])
else
    ssiTimer = tic;
    for k = 1:nIter %iteration index
        m = modeRange(k);
        b = settings.blockRows; %number of blocks
        disp(['Block Row Size: ' num2str(b) ', No. of SVs: ' num2str(2*m)])
        
        if UNCALC %if uncertainty calculation is desired
            % Run the SSI/ref cov algorithm, include Tcov for uncertainty calc
            [A,C,aux] = ssicov(Hcov,m,ddim,hdim,'Tcov',Tcov);
            
            % Convert to modal parameters
            [fz,U,fz_cov,U_cov] = ss2modal(A,C,dt,aux);
            
            % Assign to the modal array
            [nDOF,nModes] = size(U);
            modObj(k) = modeobj(nModes,nDOF);
            modObj(k).f = fz(1,:)';
            modObj(k).z = fz(2,:)';
            modObj(k).u = U;
            modObj(k).df = fz_cov(1,1,:);
            modObj(k).dz = fz_cov(2,2,:);
            [modObj(k).du,~] = ucovMat2Vec(U_cov);
        else %if not
            % Run the SSI/ref cov algorithm, don't include Tcov
            [A,C] = ssicov(Hcov,m,ddim,hdim);
            
            % Convert to modal parameters
            [fz,U] = ss2modal(A,C,dt);
            
            if PLT
                plot(ax1,fz(1,:),repmat(m,size(fz,2)),'.b','MarkerSize', 10)
            end
            
            % Calculate differences and update
            for i = 1:targetModes.nModes
                f_diff = fz(1,:)-targetModes.f(i);
                [~,matchI] = min(abs(f_diff));
                f_erri = abs(fz(1,matchI)-targetModes.f(i))/targetModes.f(i);
                z_erri = abs(fz(2,matchI)-targetModes.z(i))/targetModes.z(i);
                imaci = 1-calculateMAC(U(targetI,matchI),targetModes.u(:,i));
                better = mode_err(i) > imaci;
                
                if PLT
                    if f_erri < f_th
                        plot(ax1,fz(1,matchI),m,'b+')
                    end
                    if z_erri < z_th
                        plot(ax1,fz(1,matchI),m,'bx')
                    end
                    if imaci < imac_th
                        plot(ax1,fz(1,matchI),m,'bs')
                    end
                end
                
                if f_erri < f_th && z_erri < z_th && imaci < imac_th && better
                    modObj.f(i) = fz(1,matchI);
                    modObj.z(i) = fz(2,matchI);
                    modObj.u(:,i) = U(:,matchI);
                    mode_err(i) = imaci;
                    assigned(i) = true;
                    
                    if PLT
                        plot(ax1,fz(1,matchI),m,'ko','MarkerSize',8)
                    end
                end
            end
        end
        
        if PFCALC
            %Calculate the discrete-time poles
            [Dd,Vd] = ss2dtp(A,C);
            
            % fit the participation factors (time domain)
            [pt,Re,Ge,fe,Gam_td] = fitpt(Dd,Vd,R,N,dt);
            
            % Assign to the modal array
            modObj(k).pf = pt;
        end
        
        if sum(assigned) == numel(assigned)
            %all modes have been assigned
            break
        end
    end
    disp(['Computation time (s): ' num2str(toc(ssiTimer))])
end

if PLT
    for k = 1:targetModes.nModes
        plot(ax1,repmat(targetModes.f(k),1,2),ylim(ax1),'k-')
    end
    yyaxis right
    plot(spec.f,spec.P)
    set(gca,'yscale','log');
    ylabel('SVD Spectrum');
end