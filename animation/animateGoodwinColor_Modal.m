% Plots a mode shape on top goodwin geometry
% mode: 119x7 double array
%      mode(:,1) hold the node numbers
%      mode(:,2) mode mag X-axis, mode(:,3) mode phase X-axis
%      mode(:,4:7) are for Y and Z axes
% amp: mode amplification factor
% loops: number of loops to animate (mode cycles)
% fpl:  number of frames per loop
% f:    frequency in hertz, for display only
% F:   frame object for movie
function F = animateGoodwinColor_Modal(mode,f,amp,P,L,S,loops,fpl)

% figure
% Z = peaks;
% surf(Z)
% axis tight manual
% ax = gca;
% ax.NextPlot = 'replaceChildren';
% 
% loops = 100;
% F(loops) = struct('cdata',[],'colormap',[]);
% for j = 1:loops
%     X = sin(j*pi/50)*Z;
%     surf(X,Z)
%     drawnow
%     F(j) = getframe;
% end

figure(gcf)
wireframe0(P,L(1,:)) 
hold on
wireframe0(P,L(2:end,:)) %plot the geometry lines
try
    load azel % Azimuth and elevation for figure vies
    view([az, el]);
catch err
    warning('No azel file found');
end
disp('adjust rotation and press any key to store the preferred view')
pause
[az, el] = view;
disp('view stored')
save azel az el

xlims = xlim;
ylims = ylim;
zlims = zlim;
clf

axis tight manual
ax = gca;
ax.NextPlot = 'replaceChildren';


if (amp > 0),
    fak = 1;
else
    fak = -1;
    amp = abs(amp);
end

frames = loops*fpl;
F(frames) = struct('cdata',[],'colormap',[]);

%Calculate mode shape for normalization
x = mode(:,2);
y = mode(:,4);
z = mode(:,6);
l2 = x'*x + y'*y + z'*z; %sqr magnitude of mode vector
dPm = [x, y, z]/sqrt(l2); %full modal displacment
dPm_norm = sqrt(dPm(:,1).^2 + dPm(:,2).^2 + dPm(:,3).^2); %full modal norm
Cmax = max(dPm_norm); % maximum color intensity corresponds to max displacment
Cmin = 0; %minimum intensity corresponds to zero displacement

%Animate the mode shape
for j = 1:frames
    %x = mag*cos(timestep+phase)
    x = mode(:,2).*cos((j-1)*2*pi/fpl+mode(:,3));
    y = mode(:,4).*cos((j-1)*2*pi/fpl+mode(:,5));
    z = mode(:,6).*cos((j-1)*2*pi/fpl+mode(:,7));
    dP = [x, y, z]/sqrt(l2); %normalized instantaneous displacment
    P1 = P + fak*dP*1.5.^amp; %displaced node locations, with amplification
    
    C = sqrt(dP(:,1).^2 + dP(:,2).^2 + dP(:,3).^2); %color corresponds displament norm
    trisurf(S,P1(:,1),P1(:,2),P1(:,3),C,'linestyle','none','facecolor','interp');
    caxis([Cmin Cmax]);
    hold on
    wireframe(P1,L) %plot the geometry lines
    hold off
    view(az,el)
    title([num2str(f) ' Hz']);
    
    xlim(xlims+[-2 2]*amp)
    ylim(ylims+[-2 2]*amp)
    zlim(zlims+[-2 2]*amp)
    drawnow
    axis off
    % axis vis3d
    % axis tight
    F(j) = getframe;
end