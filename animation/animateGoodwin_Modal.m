% Plots a mode shape on top goodwin geometry
% mode: 119x7 double array
%      mode(:,1) hold the node numbers
%      mode(:,2) mode mag X-axis, mode(:,3) mode phase X-axis
%      mode(:,4:7) are for Y and Z axes
% amp: mode amplification factor
% loops: number of loops to animate (mode cycles)
% fpl:  number of frames per loop
% f:    frequency in hertz, for display only
% F:   frame object for movie
function F = animateGoodwin_Modal(mode,f,amp,P,L,loops,fpl)

% figure
% Z = peaks;
% surf(Z)
% axis tight manual
% ax = gca;
% ax.NextPlot = 'replaceChildren';
% 
% loops = 100;
% F(loops) = struct('cdata',[],'colormap',[]);
% for j = 1:loops
%     X = sin(j*pi/50)*Z;
%     surf(X,Z)
%     drawnow
%     F(j) = getframe;
% end


figure('position',[100,100,895,600])
load azel % Azimuth and elevation for figure vies
wireframe0(P,L(1,:)) 
hold on
wireframe0(P,L(2:end,:)) %plot the geometry lines
view([az, el]);
figure(gcf)
disp('adjust rotation and press any key to store the preferred view')
pause
[az, el] = view;
disp('view stored')
save azel az el

xlims = xlim;
ylims = ylim;
zlims = zlim;
clf

axis tight manual
ax = gca;
ax.NextPlot = 'replaceChildren';


if (amp > 0),
    fak = 1;
else,
    fak = -1;
    amp = abs(amp);
end

frames = loops*fpl;
F(frames) = struct('cdata',[],'colormap',[]);

%Calculate mode shape for normalization
x = mode(:,2).*cos(mode(:,3));
y = mode(:,4).*cos(mode(:,5));
z = mode(:,6).*cos(mode(:,7));
l2 = x'*x + y'*y + z'*z; %sqr magnitude of mode vector

%Animate the mode shape
for j = 1:frames
    %x = mag*cos(timestep+phase)
    x = mode(:,2).*cos((j-1)*2*pi/fpl+mode(:,3));
    y = mode(:,4).*cos((j-1)*2*pi/fpl+mode(:,5));
    z = mode(:,6).*cos((j-1)*2*pi/fpl+mode(:,7));
    dP = [x, y, z]/sqrt(l2); %normalize
    P1 = P + fak*dP*1.5.^amp;
    wireframe(P1,L(1,:)) 
    hold on
    wireframe(P1,L(2:end,:)) %plot the geometry lines
    hold off
    view(az,el)
    title([num2str(f) ' Hz']);
    
    xlim(xlims+[-2 2]*amp)
    ylim(ylims+[-2 2]*amp)
    zlim(zlims+[-2 2]*amp)
    drawnow
    % axis vis3d
    % axis tight
    F(j) = getframe;
end