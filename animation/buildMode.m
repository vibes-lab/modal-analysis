function mode = buildMode(B,dir,loc,P)
% B: raw mode vector (num sensors)x1
% dir: vector of node directions (num sensors)x1, where 1 = X, 2 = Y, 3 = Z.
% mesh: vector of dim (num sensors)x3, with the x,y and z coordinates for
% the each node
% geoFile: Excel File with nodes, lines, and surfaces

nodeIDs = 1:size(P,1);

N_chan = length(B);
[N_node,~] = size(P);
node = zeros(N_node,3);

sensNodes = assignNodes(nodeIDs,P,loc);

for k = 1:N_chan
    id = sensNodes(k);
    node(id,dir(k)) = B(k);
end

% Convert to mode format
mode = zeros(N_node,7);
mode(:,1) = sensNodes(:,1);
mode(:,2) = abs(node(:,1));
mode(:,3) = angle(node(:,1));
mode(:,4) = abs(node(:,2));
mode(:,5) = angle(node(:,2));
mode(:,6) = abs(node(:,3));
mode(:,7) = angle(node(:,3));