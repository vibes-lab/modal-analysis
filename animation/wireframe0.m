%wireframe0(P, L)
%Plots a series of lines specified in the point matrix P and the line
%matrix L. The matrices are assumed to have SVS format. Lines are plotted
%as dotted lines in double thickness.

%All rights reserved. Rune Brincker, Aug 2012.

function wireframe0(P, L)
[nl, nc] = size(L);
for n=1:nl,
    plot3([P(L(n,1), 1); P(L(n,2), 1)], [P(L(n,1), 2); P(L(n,2), 2)], [P(L(n,1), 3); P(L(n,2), 3)], 'k:', 'LineWidth',2);
end
axis equal 