function [U,P,L,S] = buildAnimationMode(B,geo)
% B: raw mode vector (num sensors)x1
% DATA: data structure with all sensor info (num sensors)x1
% geoFile: Excel File with nodes, lines, and surfaces
% Load goodwin geometry

P = geo.nodes; %graph node points
L = geo.lines; %lines connecting nodes
S = geo.surfs; %surfaces of 3 nodes        


N_chan = length(B);
[N_node,~] = size(P);
node = zeros(N_node,3);

nodeID = geo.measNodes;

for k = 1:N_chan
    if isnan(nodeID(k))
        continue
    end
    
    orient = geo.measOrient(k,:)/norm(geo.measOrient(k,:));
    
    node(nodeID(k),1) = B(k)*orient(1);
    node(nodeID(k),2) = B(k)*orient(2);
    node(nodeID(k),3) = B(k)*orient(3);
end


% Convert to mode format
U = zeros(N_node,7);
U(:,1) = 1:N_node;
U(:,2) = abs(node(:,1));
U(:,3) = angle(node(:,1));
U(:,4) = abs(node(:,2));
U(:,5) = angle(node(:,2));
U(:,6) = abs(node(:,3));
U(:,7) = angle(node(:,3));