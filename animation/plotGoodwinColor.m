% Plots a mode shape on top goodwin geometry
% mode: 119x7 double array
%      mode(:,1) hold the node numbers
%      mode(:,2) mode mag X-axis, mode(:,3) mode phase X-axis
%      mode(:,4:7) are for Y and Z axes
% amp: mode amplification factor
function plotGoodwinColor(mode,amp,P,L,S)

if exist('azel','var')
    load azel % Azimuth and elevation for figure views
else
    az = -20;
    el = 34;
end
wireframe0(P,L(1,:)) 
hold on
wireframe0(P,L(2:end,:)) %plot the geometry lines
view([az, el]);

% disp('adjust rotation and press any key to store the preferred view')
% pause
% [az, el] = view;
% disp('view stored')
% save azel az el

if (amp > 0),
    fak = 1;
else
    fak = -1;
    amp = abs(amp);
end
x = mode(:,2).*cos(mode(:,3));
y = mode(:,4).*cos(mode(:,5));
z = mode(:,6).*cos(mode(:,7));
l2 = x'*x + y'*y + z'*z;
dP = [x, y, z]/sqrt(l2);
dP_norm = sqrt(dP(:,1).^2 + dP(:,2).^2 + dP(:,3).^2);
P1 = P + fak*dP*1.5.^amp;

Cmax = max(dP_norm); 
Cmin = 0; 
C = dP_norm;
trisurf(S,P1(:,1),P1(:,2),P1(:,3),C,'linestyle','none','facecolor','interp');
caxis([Cmin Cmax]);
hold on
wireframe(P1,L) %plot the geometry lines
hold off
view(az,el)

axis vis3d
axis tight
axis off
% xlabel('x')
% ylabel('y')
% zlabel('z')
figure(gcf)
hold off