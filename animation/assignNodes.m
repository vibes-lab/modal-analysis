function [nodeID, nodeDist] = assignNodes(nodeLoc,sensLoc)

[Nn,~] = size(nodeLoc);
[Ns,~] = size(sensLoc);
nodeID = zeros(500,1);
nodeDist = zeros(500,1);

for count = 1:Ns
    if ~isnan(sum(sensLoc(count,:)))
        difference = nodeLoc - repmat(sensLoc(count,:),Nn,1);
        distance = diag(difference*difference'); % norms of differences
        [nodeD,nodeIndx] = min(distance);
        nodeID(count) = nodeIndx;
        nodeDist(count) = nodeD;
    else
        nodeID(count) = nan;
        nodeDist(count) = nan;
    end
end

nodeID = nodeID(1:count);
nodeDist = nodeDist(1:count);