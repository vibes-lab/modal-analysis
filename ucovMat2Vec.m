function [uR_cov,uI_cov] = ucovMat2Vec(ucov)

[s1,~,s3] = size(ucov);

dof = s1/2;

uR_cov = zeros(dof,s3);
uI_cov = zeros(dof,s3);

for k =1:s3
    uR_cov(:,k) = diag(squeeze(ucov(1:dof,1:dof,k)));
    uI_cov(:,k) = diag(squeeze(ucov(dof+1:2*dof,dof+1:2*dof,k)));
end
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    