function MPC = phaseCollinearity(phi)

phiT = phi-mean(phi);
eMPC = (norm(imag(phiT))^2-norm(real(phiT))^2)/(2*real(phiT')*imag(phiT));
thetaMPC = atan(abs(eMPC)+sign(eMPC)*sqrt(1+eMPC^2));

MPC = (norm(real(phiT))^2+(1/eMPC)*real(phiT')*imag(phiT)*...
    (2*(eMPC^2+1)*sin(thetaMPC)^2-1))/...
    (norm(real(phiT))^2+norm(imag(phiT))^2);