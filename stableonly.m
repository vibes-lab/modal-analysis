% [Vs,Ds] = stableonly(V,D)
%This function return the set of stable modes Vs, Ds from a set of modes V,
%D that might contain both stable and unstable modes. A stable mode means 
%a mode with positive damping. The function takes both matrices with 1 
%mode shape per mode and two mode shapes per mode, the only restriction is 
%that the mode shape matrix V must correspond to the diagonal matrix D 
%containing the discrete time poles mu = exp(lam*dt) where lam are the 
%continous time poles.

%All rights reserved, Rune Brincker, Jun 2010, Mar 2012, Aug 2012.
%Updated by Rodrigo Sarlo to deal with real modes

function [V1,D1] = stableonly(V,D,zm)
if (nargin == 2), zm = 1; end;
Nlam = length(diag(D));
lam = diag(log(D))';
fz = pol2mod(lam);
m1 = 0;
V1 = [];
D1 = [];
Vim = sum(abs(imag(V))); % Added by R. Sarlo
for m=1:Nlam,
    if (fz(2,m) > 0 && fz(2,m) < zm && Vim(m)>0) % Edit by R. Sarlo
        m1 = m1+1;
        D1(m1,m1) = D(m,m);
        V1(:,m1) = V(:,m);
    end
end