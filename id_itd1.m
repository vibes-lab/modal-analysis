% [V,D] = id_itd1(y)
%SIMO version of the Ibrahim time domain (ITD) identification technique.
%One free decay is defined as the columns in the 2D input array y. Only 
%stable modes (positive damping) are returned.
%y:     2D input data matrix, y(r,k) defines the free decay as y(r,k)
%       where k is the time index and r is the spatial index providing the
%       signal from different sensors.
%V:     Mode shape vector matrix of double length in state space format
%D:     Diagonal matrix containing the corresponding discrete eigenvalues
%Modal amplitudes must be calculated separately.

%All rights reserved, Rune Brincker, Jun 2010, Mar 2012, Aug 2012.

function [V1,D1] = id_itd1(y)
[Nr, K] = size(y);
S = 2;
S1 = S*Nr;
H = hankel(y, S);
H1 = H(1:S1,:);
H2 = H(S1+1:2*S1,:);
T11 = H1*H1';
T21 = H2*H1';
T12 = H1*H2';
T22 = H2*H2';
A1 = T21*pinv(T11);
A2 = T22*pinv(T12);
A = (A1 + A2)/2;
[V,D] = eig(A);
D = sqrt(D);
[V1,D1] = stableonly(V,D);
