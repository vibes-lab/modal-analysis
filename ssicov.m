function [A,C,aux] = ssicov(Hcov,m,datadim,hankeldim,varargin)
%[A,C,aux] = ssicov(Hcov,m,datadim,hankeldim,varargin)
%Identitification using covariance-based reference stochastic subspace 
%identification (SSI/ref) according to the reference channel techinque by 
%Reynders et al., "Uncertainty bounds on modal parameters obtained from
%SSI."

%INPUTS
%Hcov:  Covariance Hankel matrix (from buildHcov)
%m:     Number of modes to be returned in the identication (singular
%       values restricted to n, where n=2m)
%datadim: Dimensions of the data [no. of sensors, no. projection channels]
%hankeldim: Dimension of the hankel matrix
%Tcov (optional): T matrix for uncertainty calculation. Name: 'Tcov'
%L (optional); Output matrix for B and D matrix estimation Name: 'L'

%NOTE: if Tcov is not provided the algorithm skips the uncertainty
%calculation.

%OUTPUTS
%A:     estimate system matrix (2m x 2m)
%C:     Estimate output matrix (l x 2m) l = total number of channels in y
%aux:    Structure of auxilary matrices (used with ss2modal to extract
%       modal parameter uncertainties). Only returns if Tcov is an input.

%Example: function [A,C,cv] = ssicov(Hcov,m,datadim,hankeldim,...
%               'Tcov',Tcov,'L',L)

%Rodrigo Sarlo, Feb 2017

%% Handle arguments
sz = numel(varargin);
if mod(sz,2) ~= 0
    error('Optional arguments must have valid Name, Value pairs');
end

UNBOOL = false;
INBOOL  = false;
for k1 = 1:2:(sz-1)
    switch varargin{k1}
        case 'Tcov'
            T = varargin{k1+1};
            UNBOOL = true; %perform uncertainty calculation
        case 'L'
            L = varargin{k1+1};
            INBOOL = true; %estimate B and D matrices
    end
end

%% ============= Main Algorithm ===========================
%% Extract data dimensions
l = datadim(1); %total number of sensors
r = datadim(2); %number of projection channels
q = hankeldim(1); %number of "past block rows"
p = hankeldim(2); %number of "future block rows"

%% Build selector matrices
S1 = [eye(p*l) zeros(p*l,l)]; %selects all but last block row
S2 = [zeros(p*l,l) eye(p*l)]; %selects all but first block row
S3 = [eye(l) zeros(l,p*l)]; %selects only first block row
S4 = [zeros(r*p,r); eye(r)]; %selects last block row

%% Calculate SVD and truncate at nth order
[U,S,V] = svd(Hcov);
n = 2*m; %desired system order based on number of modes

Un = U(:,1:n);
Vn = V(:,1:n);
Sn = S(1:n,1:n);
sn = Sn(1:n+1:end).';

Oi = Un*sqrt(Sn); %Observability matrix
Ci_ref = sqrt(Sn)*Vn.';%reference controllability matrix (not used)

%% Extract system matrices
Oup = S1*Oi;
Odw = S2*Oi;
A = pinv(Oup)*Odw;
C = (S3*Oi);
aux.G_ref = Ci_ref*S4;


%% ============== Calculate Modal parameter uncertainty ================
if UNBOOL
    [NN,nb] = size(T); %number of hankel samples for variance estimation
    
    %% Aux GJe
    N1 = q*r; 
    N2 = (p+1)*l;
    I = uint32(1:N1).';
    J = uint32(1:N2:NN).';
    Tj1 = zeros(N1,nb);
    Tj2 = zeros(N2,nb);
    
    %% Build auxilary matrices
    
    uTimer = tic;
    Q1 = zeros(n^2,nb); Q2 = Q1; Q3 = Q1;
    Q4 = zeros(l*n,nb);
    for k1 = 1:n
        % [1](29)
        Kj = eye(N1) + [zeros(N1-1,N1); 2*Vn(:,k1).']-Hcov.'*Hcov/sn(k1)^2;
        Bj1 = [eye(N2)+Hcov/Kj/sn(k1)*(Hcov.'/sn(k1) -[zeros(N1-1,N2);Un(:,k1).']) Hcov/Kj/sn(k1)];
         %     Cj = (1/sig_n(k1))*...
    %         [(eye(s*l)-Un(:,k1)*Un(:,k1).')*kron(Vn(:,k1).',eye(s*l));...
    %         (eye(s*r)-Vn(:,k1)*Vn(:,k1).')*kron(eye(s*r),Un(:,k1).')];

         % Tj1 = kron(eye(q*r),Un(:,k1).')*T; % SLOW
         % Tj2 = kron(Vn(:,k).',eye((p+1)*l))*T; % SLOW
         for i = 1:N1
            Tj1(i,:) = Un(:,k1).'*T((i-1)*N1+I,:);
         end         
         for i = 1:N2
            Tj2(i,:) = Vn(:,k1).'*T(i-1+J,:);
         end
         
        JOHTj = 1/sqrt(sn(k1))*0.5*Un(:,k1)*(Vn(:,k1).'*Tj1) + ...
            1/sqrt(sn(k1))*Bj1*[Tj2-Un(:,k1)*(Un(:,k1).'*Tj2) ; Tj1-Vn(:,k1)*(Vn(:,k1).'*Tj1)];
        Q1((k1-1)*n+1:k1*n,:) = Oup.'*S1*JOHTj;
        Q2((k1-1)*n+1:k1*n,:) = Odw.'*S1*JOHTj;
        Q3((k1-1)*n+1:k1*n,:) = Oup.'*S2*JOHTj;
        Q4((k1-1)*l+1:k1*l,:) = [eye(l) zeros(l,p*l)]*JOHTj;
    end
    %%
    Pnn = zeros(n^2);
    for k1 = 1:n
        ek = zeros(n,1);
        ek(k1) = 1;
        Pnn(:,(k1-1)*n+1:k1*n) = kron(eye(n),ek);
    end
    %%

    % P1 = zeros(s*l*n);
    % for k1 = 1:n
    %     for k2 = 1:s*l
    %         P1(k1+(k2-1)*n,(k1-1)*s*l+k2) = 1;
    %     end
    % end
    OO = pinv(Oup.'*Oup);
    PQ1 = (Pnn+eye(n^2))*Q1;
    PQ23 = Pnn*Q2+Q3;
%     JAO = kron(eye(n),OO)*(-kron(A.',eye(n))*(Pnn+eye(n^2))*kron(eye(n),Oup.'*S1)...
%         +Pnn*kron(eye(n),Odw.'*S1)+kron(eye(n),Oup.'*S2));
    JAO = []; % GJe: not used directly to compute eigcov, can be skipped for now
    % JAO2 = kron(eye(n),pinv(Oup)*S2)-kron(A.',pinv(Oup)*S1)...
    %     +kron(Odw.'*S1-A.'*Oup.'*S1,OO)*P1;
    % nn = norm(JAO-JAO2);
    
    aux.OO = OO;
    aux.PQ1 = PQ1;
    aux.PQ23 = PQ23;
    aux.Q4 = Q4;
    aux.JAO = JAO;

    disp(['Uncertainty computation time (s): ' num2str(toc(uTimer))]) 
elseif INBOOL
    p = size(L,1)/q;
    UT = U(:,n+1:end)';
    P = UT*Hcov*pinv(L);
    F = zeros(q*l,l+n);
    Pp = zeros(q*q*l-q*n,p);
    for k1 = 1:q
        if k1 == q
            UUL = zeros(l*q-n,l+n);
        else
            UUL = [UT(:,((k1-1)*l+1):k1*l) UT(:,(k1*l+1):q*l)*Oi(1:(l*(q-k1)),:)];
        end

        F(((k1-1)*(l*q-n)+1):k1*(l*q-n),:) = UUL;
        Pp(((k1-1)*(l*q-n)+1):k1*(l*q-n),:) = P(:,(p*(k1-1)+1):k1*p);  
    end
    DB = pinv(F)*Pp;
    D = DB(1:l,:);
    B = DB((l+1):end,:);
    aux.D = D;
    aux.B = B;
end

    