function [I,score] = cpsddeim(X,wdwsize,flim,fs)
%CPSDDEIM rank of measurement time series
%   [I,score] = CPSDDEIM(y) returns an ordered list of indeces I
%   corresponding to the columns of y. The indeces are in order of
%   importance, score. It applies the Discrete Empirical Interpolation 
%   Method in the frequency domain. It uses the 
%   cross power spectral densities between the sensor with highest
%   correlation to all other sensors. The score value is equivalent to the
%   singular value.

%   [I,score] = CPSDDEIM(y,wdwsize) The argument wdwsize [double or vector] 
%   specifies the spectral density window (see cpsd).
%
%   [I,score] = CPSDDEIM(y,...,flim,fs)  specifies the range of 
%   frequencies on which base the ranking. Argument flim is [2x1] double
%   specifing the frequencies and fs is a double specifying the sampling
%   rate. If not specified, fs is assumed to be 1.  

%defaults
for k = 1:(4-nargin)
    switch k
        case 1 
            fs = 1;
        case 2 
            flim = [0 .5];
        case 3
            wdwsize = floor(length(y_sim)/20); % PSD window size
        case 4
            error('Not enough input arguments.');
    end
end

%% Calculate correlations
[n,c] = size(X); %n=number of observations, c=number of channels
R = cov(X)*n; %correlation matrix 
R2 = R.^2; %squared correlation matrix (not sure about this step)
Rdiag = diag(R); %vector of autocorrelations
Ri = repmat(Rdiag,1,c); %row autocorr matrix
Rj = repmat(Rdiag',c,1); %column autocorr matrix
C2 = R2./(Ri.*Rj); %Normalized Correlation coefficient matrix (0-1)

%% Find the "reference" channel
%The channel with the largest sum of correlations serves as the reference
%channel
Wi = sum(abs(C2),2);
[~,Iref] = max(Wi);

%% Cross Power Spectral density of sensors
%Normalize the signal by variance
Xvar = var(X,[],1);
Xn = X./repmat(Xvar,length(X),1);

nfft = max(256,2^nextpow2(wdwsize)); % no. of fft points should equal
% closest power of 2 of window size, but no less that 256.

% compute one-sided psd
[Paa,faa] = cpsd(Xn(:,Iref),Xn,wdwsize,floor(wdwsize/2),nfft,fs);

%% Apply frequency limits
fI = faa >= flim(1) & faa <= flim(2);

%% DEIM
[I,score] = qdeim(Paa(fI,:)');
