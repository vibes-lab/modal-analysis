function [I,score] = rankcorr(X,thresh)

%THIS FUNCTION IS NOT FINISHED

%Rank the columns of a time series matrix X [n x c], whose rows
%represent observations and columns represent measurement channels.
%High-ranking rows are those that contain the least amount of
%redundant information, found using the correlation technique outlined by 
%Brinker et al. in "Applications of Frequency Domain Curve-fitting in the
%EFDD Technique"
%Returns:
%   I [nMax x 1] double vector of channel rankings
%   score [nMax x 1] correlation score of the each channel

%NOTE: Correlation refers to the unscaled covariance, without any time lag
%R(0)

%(c) Rodrigo Sarlo, 2016 (sarlo@vt.edu)
%% Calculate correlations
[n,c] = size(X); %n=number of observations, c=number of channels
R = cov(X)*n; %correlation matrix 
R2 = R.^2; %squared correlation matrix (not sure about this step)
Rdiag = diag(R); %vector of autocorrelations
Ri = repmat(Rdiag,1,c); %row autocorr matrix
Rj = repmat(Rdiag',c,1); %column autocorr matrix
C2 = R2./(Ri.*Rj); %Normalized Correlation coefficient matrix (0-1)

%% Find the first projection channel
%The channel with the largest sum of correlations contains the "most"
%useful information
Wi = sum(abs(C2),2);
[~,pc1] = max(Wi);

%% Search for the remaining projection channels (PC)
PC_list = zeros(maxN,1); %List of PC indeces 
PC_list(1) = pc1; %Assign the first PC 

for p = 2:maxN
    pc_ind = PC_list(1:p-1); %list of PC indeces so far
    c2 = C2(:,pc_ind); %Corr coeff matrix of selected PCs
    
    wi = sum(abs(c2),2); %Sum of corr coeff
    
    switch noiseElimType
        case 'threshold'
            % Find channel with the LEAST correlation to the selected PCs.
            % Only accept values above a min threshold to avoid "dead"
            % channels
            [cmin,pci] = min(wi);
            while cmin < value
                wi(pci) = inf;
                [cmin,pci] = min(wi);
            end
        case 'maxcorr'
            %Return the channels with maximum correlation. 
            full_ind = 1:c; %Full index list
            diff_ind = setdiff(full_ind,pc_ind); %List of non PC indeces
            c2diff = C2(:,diff_ind); %Corr coeff matrix of non PCs
            wi_diff = sum(abs(c2diff),2); %Corr sum of non PCs
            [cmax,pci] = max(wi_diff);
    end
    
    PC_list(p) = pci; %Assign the next PC
    
end
PC = X(:,PC_list); % Reduce X to only the PCs
I = PC_list; %Return list of indeces
