function [p,sv] = qdeim(x)

[U,sv0,~] = svd(x,'econ');
[~,~,p0] = lu(U,'vector');
p = p0';
sv = diag(sv0);
