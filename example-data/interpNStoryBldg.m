%ModeShape Interpolation function for 5 story building with two X and one Y
%axis sensors per floor

%bMode is a Mx3 matrix, where M is the number of node locations and 
%the columns represent the modal displacement in the X,Y, and Z directions.


%nodes is an Mx3 matrix of nodal coodinates, where M is the 
% the number of node locations and columns represent X,Y, and Z coordinates

%The interpolation outputs a new M x 3 modeshape where zero valued elements
%in bMode are interpolated from its non-zero entries using the assumption
%of a rigid floor

%The function
function bMode1 = interpNStoryBldg(bMode,nodes)
    [nNode,~] = size(bMode);
    nFloors = (nNode-4)/4; %assume 4 nodes per floor, don't count ground floor
    
    bMode1 = bMode;
    for floor = 1:nFloors
       
            %Find floor dimensions first
            a = (nodes(1+(floor-1)*4,1) - nodes(2+(floor-1)*4,1))/2; %x dimension
            b = (nodes(1+(floor-1)*4,2) - nodes(3+(floor-1)*4,2))/2; %y dimension
            
            xtr = bMode(1+(floor-1)*4,1); %the top right x sensor for this floor
            ytr = bMode(1+(floor-1)*4,2); %the top right y sensor for this floor
            xbl = bMode(3+(floor-1)*4,1); %the bottom left sensor for this floor
            
            %Linear system of equations to solve for floor rotation and
            %translation
            
            B = [xtr;ytr;xbl];
            A = [1 0 -b;0 1 a; 1 0 b];
            S = pinv(A)*B;
            x0 = S(1);
            y0 = S(2);
            theta = S(3);
            
            %Interpolate using the solution
            xtl = x0 -theta*b;
            ytl = y0 -theta*a;
            ybl = y0 -theta*a;
            xbr = x0 +theta*b;
            ybr = y0 +theta*a;
            
            bMode1(2+(floor-1)*4,1) = xtl;
            bMode1(2+(floor-1)*4,2) = ytl;
            bMode1(3+(floor-1)*4,2) = ybl;
            bMode1(4+(floor-1)*4,1) = xbr;
            bMode1(4+(floor-1)*4,2) = ybr;
            
        
    end  
end