%ModeShape Interpolation function for Goodwin Hall

%bMode is a Mx3 matrix, where M is the number of node locations and 
%the columns represent the modal displacement in the X,Y, and Z directions.


%nodes is an Mx3 matrix of nodal coodinates, where M is the 
% the number of node locations and columns represent X,Y, and Z coordinates

%The interpolation outputs a new M x 3 modeshape where zero valued elements
%in bMode are interpolated from its non-zero entries using the assumption
%of a rigid floor

%The function
function bMode1 = interpGoodwin(bMode,nodes)
   

bMode1 = bMode;

% Goodwin interpolation equations
bMode1(2,1)=50* bMode1(1,1)/100+50*bMode1(4,1)/100;
bMode1(3,1)=30* bMode1(1,1)/100+70*bMode1(4,1)/100;
bMode1(6,1)=90* bMode1(5,1)/100+10*bMode1(7,1)/100;
bMode1(8,1)=65* bMode1(7,1)/100+35*bMode1(10,1)/100;
bMode1(9,1)=60* bMode1(7,1)/100+40*bMode1(10,1)/100;
bMode1(11,1)=100* bMode1(10,1)/100+0*bMode1(1,1)/100;
bMode1(14,1)=65* bMode1(13,1)/100+35*bMode1(16,1)/100;
bMode1(15,1)=35* bMode1(13,1)/100+65*bMode1(16,1)/100;
bMode1(17,1) = (0.059106*bMode1(10,1) + 0.056683*bMode1(29,1))*8.636425;
bMode1(18,1) = (0.062460*bMode1(37,1) + 0.061703*bMode1(1,1))*8.053963;
bMode1(19,1) = (0.098446*bMode1(37,1) + 0.064047*bMode1(1,1))*6.154100;
bMode1(21,1)=50* bMode1(20,1)/100+50*bMode1(23,1)/100;
bMode1(22,1)=30* bMode1(20,1)/100+70*bMode1(23,1)/100;
bMode1(25,1)=90* bMode1(24,1)/100+10*bMode1(26,1)/100;
bMode1(27,1)=65* bMode1(26,1)/100+35*bMode1(29,1)/100;
bMode1(28,1)=60* bMode1(26,1)/100+40*bMode1(29,1)/100;
bMode1(30,1)=100* bMode1(29,1)/100+0*bMode1(1,1)/100;
bMode1(33,1)=65* bMode1(32,1)/100+35*bMode1(35,1)/100;
bMode1(34,1)=35* bMode1(32,1)/100+65*bMode1(35,1)/100;
bMode1(36,1) = (0.062875*bMode1(102,1) + 0.057293*bMode1(29,1))*8.321624;
bMode1(39,1)=50* bMode1(38,1)/100+50*bMode1(41,1)/100;
bMode1(40,1)=30* bMode1(38,1)/100+70*bMode1(41,1)/100;
bMode1(43,1)=90* bMode1(42,1)/100+10*bMode1(44,1)/100;
bMode1(45,1)=65* bMode1(44,1)/100+35*bMode1(47,1)/100;
bMode1(46,1)=60* bMode1(44,1)/100+40*bMode1(47,1)/100;
bMode1(48,1)=100* bMode1(47,1)/100+0*bMode1(1,1)/100;
bMode1(51,1)=65* bMode1(50,1)/100+35*bMode1(53,1)/100;
bMode1(52,1)=35* bMode1(50,1)/100+65*bMode1(53,1)/100;
bMode1(54,1) = (0.073399*bMode1(47,1) + 0.069544*bMode1(65,1))*6.995812;
bMode1(55,1) = (0.064023*bMode1(77,1) + 0.060801*bMode1(41,1))*8.011274;
bMode1(56,1) = (0.171095*bMode1(37,1) + 0.051692*bMode1(38,1))*4.488594;
bMode1(57,1)=70* bMode1(59,1)/100+30*bMode1(67,1)/100;
bMode1(58,1)=90* bMode1(38,1)/100+10*bMode1(41,1)/100;
bMode1(61,1)=90* bMode1(60,1)/100+10*bMode1(62,1)/100;
bMode1(63,1)=65* bMode1(62,1)/100+35*bMode1(65,1)/100;
bMode1(64,1)=60* bMode1(62,1)/100+40*bMode1(65,1)/100;
bMode1(66,1)=100* bMode1(65,1)/100+0*bMode1(1,1)/100;
bMode1(68,1)=35* bMode1(59,1)/100+65*bMode1(67,1)/100;
bMode1(69,1)=50* bMode1(49,1)/100+50*bMode1(50,1)/100;
bMode1(70,1)=40* bMode1(59,1)/100+60*bMode1(67,1)/100;
bMode1(71,1)=40* bMode1(49,1)/100+60*bMode1(38,1)/100;
bMode1(72,1) = (0.076596*bMode1(50,1) + 0.065386*bMode1(32,1))*7.043157;
bMode1(73,1)=70* bMode1(50,1)/100+30*bMode1(38,1)/100;
bMode1(74,1) = (0.097821*bMode1(53,1) + 0.077162*bMode1(35,1))*5.714845;
bMode1(75,1)=50* bMode1(50,1)/100+50*bMode1(38,1)/100;
bMode1(76,1) = (50*bMode1(59,1) + 50*bMode1(65,1))/100;
bMode1(79,1)=60* bMode1(103,1)/100+40*bMode1(102,1)/100;
bMode1(82,1)=50* bMode1(81,1)/100+50*bMode1(83,1)/100;
bMode1(84,1)=70* bMode1(83,1)/100+30*bMode1(77,1)/100;
bMode1(85,1)=50* bMode1(59,1)/100+50*bMode1(76,1)/100;
bMode1(86,1)=50* bMode1(60,1)/100+50*bMode1(76,1)/100;
bMode1(87,1)=50* bMode1(62,1)/100+50*bMode1(76,1)/100;
bMode1(88,1)=100* bMode1(101,1)/100+0*bMode1(1,1)/100;
bMode1(89,1)=100* bMode1(101,1)/100+0*bMode1(1,1)/100;
bMode1(90,1)=100* bMode1(101,1)/100+0*bMode1(1,1)/100;
bMode1(91,1)=100* bMode1(101,1)/100+0*bMode1(1,1)/100;
bMode1(92,1)=100* bMode1(70,1)/100+0*bMode1(1,1)/100;
bMode1(93,1) = (0.102517*bMode1(44,1) + 0.092356*bMode1(26,1))*5.131553;
bMode1(94,1) = (50*bMode1(44,1) + 50*bMode1(43,1))/100;
bMode1(95,1)=72* bMode1(72,1)/100+28*bMode1(70,1)/100;
bMode1(96,1)=50* bMode1(74,1)/100+50*bMode1(57,1)/100;
bMode1(97,1)=50* bMode1(77,1)/100+50*bMode1(80,1)/100;
bMode1(2,2)=50* bMode1(1,2)/100+50*bMode1(4,2)/100;
bMode1(3,2)=30* bMode1(1,2)/100+70*bMode1(4,2)/100;
bMode1(6,2)=90* bMode1(5,2)/100+10*bMode1(7,2)/100;
bMode1(8,2)=65* bMode1(7,2)/100+35*bMode1(10,2)/100;
bMode1(9,2)=60* bMode1(7,2)/100+40*bMode1(10,2)/100;
bMode1(11,2)=100* bMode1(10,2)/100+0*bMode1(1,2)/100;
bMode1(14,2)=65* bMode1(13,2)/100+35*bMode1(16,2)/100;
bMode1(15,2)=35* bMode1(13,2)/100+65*bMode1(16,2)/100;
bMode1(17,2) = (0.059106*bMode1(10,2) + 0.056683*bMode1(29,2))*8.636425;
bMode1(18,2) = (0.062460*bMode1(37,2) + 0.061703*bMode1(1,2))*8.053963;
bMode1(19,2) = (0.098446*bMode1(37,2) + 0.064047*bMode1(1,2))*6.154100;
bMode1(21,2)=50* bMode1(20,2)/100+50*bMode1(23,2)/100;
bMode1(22,2)=30* bMode1(20,2)/100+70*bMode1(23,2)/100;
bMode1(25,2)=90* bMode1(24,2)/100+10*bMode1(26,2)/100;
bMode1(27,2)=65* bMode1(26,2)/100+35*bMode1(29,2)/100;
bMode1(28,2)=60* bMode1(26,2)/100+40*bMode1(29,2)/100;
bMode1(30,2)=100* bMode1(29,2)/100+0*bMode1(1,2)/100;
bMode1(33,2)=65* bMode1(32,2)/100+35*bMode1(35,2)/100;
bMode1(34,2)=35* bMode1(32,2)/100+65*bMode1(35,2)/100;
bMode1(36,2) = (0.062875*bMode1(102,2) + 0.057293*bMode1(29,2))*8.321624;
bMode1(39,2)=50* bMode1(38,2)/100+50*bMode1(41,2)/100;
bMode1(40,2)=30* bMode1(38,2)/100+70*bMode1(41,2)/100;
bMode1(43,2)=90* bMode1(42,2)/100+10*bMode1(44,2)/100;
bMode1(45,2)=65* bMode1(44,2)/100+35*bMode1(47,2)/100;
bMode1(46,2)=60* bMode1(44,2)/100+40*bMode1(47,2)/100;
bMode1(48,2)=100* bMode1(47,2)/100+0*bMode1(1,2)/100;
bMode1(51,2)=65* bMode1(50,2)/100+35*bMode1(53,2)/100;
bMode1(52,2)=35* bMode1(50,2)/100+65*bMode1(53,2)/100;
bMode1(54,2) = (0.073399*bMode1(47,2) + 0.069544*bMode1(65,2))*6.995812;
bMode1(55,2) = (0.064023*bMode1(77,2) + 0.060801*bMode1(41,2))*8.011274;
bMode1(56,2) = (0.171095*bMode1(37,2) + 0.051692*bMode1(38,2))*4.488594;
bMode1(57,2)=70* bMode1(59,2)/100+30*bMode1(67,2)/100;
bMode1(58,2)=90* bMode1(38,2)/100+10*bMode1(41,2)/100;
bMode1(61,2)=90* bMode1(60,2)/100+10*bMode1(62,2)/100;
bMode1(63,2)=65* bMode1(62,2)/100+35*bMode1(65,2)/100;
bMode1(64,2)=60* bMode1(62,2)/100+40*bMode1(65,2)/100;
bMode1(66,2)=100* bMode1(65,2)/100+0*bMode1(1,2)/100;
bMode1(68,2)=35* bMode1(59,2)/100+65*bMode1(67,2)/100;
bMode1(69,2)=50* bMode1(49,2)/100+50*bMode1(50,2)/100;
bMode1(70,2)=40* bMode1(59,2)/100+60*bMode1(67,2)/100;
bMode1(71,2)=40* bMode1(49,2)/100+60*bMode1(38,2)/100;
bMode1(72,2) = (0.076596*bMode1(50,2) + 0.065386*bMode1(32,2))*7.043157;
bMode1(73,2)=70* bMode1(50,2)/100+30*bMode1(38,2)/100;
bMode1(74,2) = (0.097821*bMode1(53,2) + 0.077162*bMode1(35,2))*5.714845;
bMode1(75,2)=50* bMode1(50,2)/100+50*bMode1(38,2)/100;
bMode1(76,2) = (50*bMode1(59,2) + 50*bMode1(65,2))/100;
bMode1(79,2)=80* bMode1(103,2)/100+20*bMode1(80,2)/100;
bMode1(82,2)=50* bMode1(81,2)/100+50*bMode1(83,2)/100;
bMode1(84,2)=70* bMode1(83,2)/100+30*bMode1(77,2)/100;
bMode1(85,2)=50* bMode1(59,2)/100+50*bMode1(76,2)/100;
bMode1(86,2)=50* bMode1(60,2)/100+50*bMode1(76,2)/100;
bMode1(87,2)=50* bMode1(62,2)/100+50*bMode1(76,2)/100;
bMode1(88,2)=100* bMode1(101,2)/100+0*bMode1(1,2)/100;
bMode1(89,2)=100* bMode1(101,2)/100+0*bMode1(1,2)/100;
bMode1(90,2)=100* bMode1(101,2)/100+0*bMode1(1,2)/100;
bMode1(91,2)=100* bMode1(101,2)/100+0*bMode1(1,2)/100;
bMode1(92,2)=100* bMode1(70,2)/100+0*bMode1(1,2)/100;
bMode1(93,2) = (0.102517*bMode1(44,2) + 0.092356*bMode1(26,2))*5.131553;
bMode1(94,2) = (50*bMode1(44,2) + 50*bMode1(43,2))/100;
bMode1(95,2)=72* bMode1(72,2)/100+28*bMode1(70,2)/100;
bMode1(96,2)=50* bMode1(74,2)/100+50*bMode1(57,2)/100;
bMode1(97,2)=50* bMode1(77,2)/100+50*bMode1(80,2)/100; 
end