function [] = plotModalPhase(x,xcol,map,ax)
% Copyright: Ellis Kessler, 2020
%%
% Inputs:
%           x:      matrix of complex column-vector modeshapes
%           xcol:   matrix corresponding to how the points should be colored (for example real(x) or  imag(x))
%           map:    the colormap used for plotting the mode shape
%           ax:    	axes target for the plot
%%
if nargin < 4
    figure;
    ax = axes();
end
for k = 1:size(x,2)
    c = x(:,k)/max(abs(x(:,k)));
    plot(ax,cos(linspace(0,2*pi,1000)),sin(linspace(0,2*pi,1000)),'k','LineWidth',1.2);
    hold on; axis equal;
    plot([-1.05 -.975],[0 0],'-k','LineWidth',1.2);
    plot([1.05 .975],[0 0],'-k','LineWidth',1.2);
    text(1.1,.02,'0^\circ','HorizontalAlignment','left');
    text(-1.1,.02,'180^\circ','HorizontalAlignment','right');
    scatter(ax,real(c./abs(c)),imag(c./abs(c)),140*abs(c)+10,xcol(:,k),'o','filled','MarkerFaceAlpha',.5,'MarkerEdgeColor','k');
    colormap(map);
    caxis([-max(abs(c)) max(abs(c))]);
    axis off;
end