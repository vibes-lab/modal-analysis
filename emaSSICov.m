function [modalArray, spec] = emaSSICov(u,y,dt,blockRows,opts)
%EMASSICOV Estimate modal parameters for a system with measured inputs u and
%   outputs y using covariance based stochastic subspace identification.

%   MODALARRAY = EMASSICOV(U,Y,DT,BLOCKROWS) returns an array of modal
%   objects MODALARRAY, with the modal parameters for a system of input 
%   excitations U and output responses Y. DT specifies the sampling period 
%   and BLOCKROWS is the number of block rows in the Hankel matrix.

%   MODALARRAY = EMASSICOV(...,OPTS) allows the used to modify the modal
%   anlysis method settings by specifying specific parameters.

%INPUT PARAMETERS

%   OPTS is a matlab structure with the following fields

%   modeRange = [vector,5:20 (default)] number of mode orders to calculate for
%       stabilization diagram (No. modes = No. singular values/2)
%   projChan = [vector] indeces of the channels (columns) to be
%       used as projection channels.
%   N = [double] (optional) no. of time samples used to estimate
%       autocorrelations. Used to calculate the spectral density matrix, which
%       is used in the stabilization diagram display, but does not impact the
%       SSI algorithm. Also used to calculate the correlation function matrix,
%       which is used to estimate participation factors (when specified in OPT)
%   nb = [double, 10 (default)] number of Hankel samples for uncertainty
%       estimation (when specified in OPT)
%   UNCALC = [0 (default),1] Compute modal paramater uncertainties.
%   PFCALC = [0 (default),1] Compute modal participation factors.
%   SPEC = [1 (default),0] Compute data spectrum, FRF matrix
%   reduction = [string, 'none' (default)] Iterate models orders by using reduced
%                   order modeling, speeding up computational time. 
%                   Chose between 'none' or 'svd.' The
%                   UNCALC option is ignored if reduction is enabled.
%   geo = [geoviz] object specifying sensor geometry

%(c) Rodrigo Sarlo, 2020
%% Handle inputs
y = y'; %function convention is for channel rows rather than channel columns
u = u';
%% Assign defaults
%default values
modeRange = 5:20;
N = floor(size(y,1)/5);
projChan = size(y,2);
UNCALC = false;
PFCALC = false;
SPEC = true;
reduction = 'none';
units = '[-]';
nb = 1;

%update defaults if specified
if nargin > 3
    if isfield(opts,'modeRange')
        modeRange = opts.modeRange;
    end
    if isfield(opts,'N')
        N = opts.N;
    end
    if isfield(opts,'projChan')
        projChan = opts.projChan;
    end
    if isfield(opts,'nb')
        nb = opts.nb;
    end
    if isfield(opts,'UNCALC')
        UNCALC = opts.UNCALC;
    end
    if isfield(opts,'PFCALC')
        PFCALC = opts.PFCALC;
    end
    if isfield(opts,'SPEC')
        SPEC = opts.SPEC;
    end
    if isfield(opts,'reduction')
        reduction = opts.reduction;
    end
    if isfield(opts,'units')
        units = opts.units;
    end
end

%% ====================== SSI-COV ======================
% STEP 0: build FRF matrices
% Note: these are not used in the main algorithm, but are used for
% stabilization diagram display and participation factor calculations
if SPEC
    Gtimer = tic;
    [Guy,fy] = tfestimate(u',y',N,[],[],1/dt,'mimo');
    %[fy,Gy] = fftspec(y, N, dt); % full spectrum SD using welch method
    % [fhy, Ghy] = halfspecw(y_norm, N, dt); %empirical half spectrum SD matrix
    % NOTE: Both Gy and Ghy are similar, however the columns of Ghy are
    % proportional to the modeshapes, thus is better for FD estimation
    disp(['SD Matrix calculation (s): ' num2str(toc(Gtimer))]);
    
    %projection channel SD matrix (used for stabilization diagram)
    Guy = permute(Guy,[3 2 1]);
    Gp = Guy(:,projChan,:);
    
    % Calculate the projection channel SD matrix SVD
    [nu,ny,nf] = size(Gp);
    sv = zeros(nf,nu);
    for k=1:nf
        [~,S,~] = svd(Gp(:,:,k));
        if nu ~= 1
            sv(k, :) = diag(S)';
        else
            sv(k, :) = S(1);
        end
    end
    
    % Assign to spectrum structure
    n = min(nu,ny);
    if n > 3
        nsv = min(numel(projChan),4);
        spec.P = sv(:,1:nsv);
    else
        spec.P = sv(:,1:n);
    end
    spec.f = fy;
    spec.units = ['FRF: ' units '/' units];
end

if PFCALC %if participation factor calculation is desired
    %Calculate the empirical CF matrix (used for participation
    %factor estimation)
    Rtimer = tic;
    R = dircor(y, N*2); %filtered positive lag correlation (CF Matrix)
    % NOTE: Is there an option to average R's to generate cleaner CF matrices??
    % NOTE 2: This takes a long time, can it be more efficient?
    disp(['CF Matrix calculation (s): ' num2str(toc(Rtimer))])
end


% STEP 1: build the covariance matrix
% This is the main thing that changes between omaSSICov and emaSSICov
[Hcov,ddim,hdim,Tcov,L] = buildHcovINS(u,y,...
    blockRows,...
    projChan,...
    nb);

%STEP 2: Iterate through the model orders and implement SSI-cov
nIter = numel(modeRange); %number of iterations
modalArray = modeobj(0,0); % initialize array of modal solutions

switch reduction
    case 'svd'
        %Computationally efficient model reduction approach
        
        b = blockRows; %number of blocks
        ssiTimer = tic;
        % Run the SSI/ref cov algorithm for largest m
        m = max(modeRange);
        [A,C,aux] = ssicov(Hcov,m,ddim,hdim,'L',L);
        
        % Construct a state space matrix
        %sys = ss(A,aux.G_ref,C,0,1);
        
        % Form the observability gramian
        %Q1  = gram(sys,'o');
        Q2 = dlyap(A',C'*C); %
        % The last line is just to make sure computations are correct.
        % Q1 should be equal to Q2, which is the case,
        %disp(norm(Q1-Q2)/norm(Q1))
        
        % sval, singular values of Q, only depend on A and C.
        % hsv, Hankel singualr values, depend on A, C, and Gref.
        sval = svd(Q2); sval = sval/sval(1);
        
        %     figure
        %     semilogy(sval,'-*')
        %     title('SV decay');
        %     xlabel('Singular Value #');
        %     ylabel('Singular Value');
        
        % We will now reduce the system and compute the poles
        % using Q1 (only depends on A and C) and
        % using sys (depend on A, C, and Gref)
        
        [V,~,~]=svd(Q2);
        
        % In these options we can specify frequency range etc.
        %OPT = balredOptions('StateElimMethod','Truncate');
        
        for k = 1:nIter %iteration index
            n = modeRange(k)*2; %SVs are twice the number of poles (complex conjugates)
            
            disp(['Block Row Size: ' num2str(b) ', No. of SVs: ' num2str(n)...
                ' Option: Reduction'])
            
            % poles from Q1 ( A and C only)
            Vr = V(:,1:n);
            Ar = Vr'*A*Vr; %reduced A matrix
            Cr = C*Vr;
            
            % poles from A, C, and Gref
            %         sys2 = balred(sys,n,OPT);
            %         Ar = sys2.A;
            %         Cr = sys2.C;
            
            % Convert to modal parameters
            [fz,U] = ss2modal(Ar,Cr,dt);
            
            % Assign to the modal array
            [nDOF,nModes] = size(U);
            modalArray(k) = modeobj(nModes,nDOF);
            modalArray(k).f = fz(1,:)';
            modalArray(k).z = fz(2,:)';
            modalArray(k).u = U;
            
            if PFCALC
                %Calculate the discrete-time poles
                [Dd,Vd] = ss2dtp(Ar,Cr);
                
                % fit the participation factors (time domain)
                [pt,Re,Ge,fe,Gam_td] = fitpt(Dd,Vd,R,N,dt);
                
                % Assign to the modal array
                modalArray(k).pf = pt;
            end
        end
        disp(['Computation time (s): ' num2str(toc(ssiTimer))])
    otherwise
        ssiTimer = tic;
        for k = 1:nIter %iteration index
            m = modeRange(k);
            b = blockRows; %number of blocks
            disp(['Block Row Size: ' num2str(b) ', No. of SVs: ' num2str(2*m)])
            
            if UNCALC %if uncertainty calculation is desired
                % Run the SSI/ref cov algorithm, include Tcov for uncertainty calc
                [A,C,aux] = ssicov(Hcov,m,ddim,hdim,'Tcov',Tcov);
                
                % Convert to modal parameters
                [fz,U,fz_cov,U_cov] = ss2modal(A,C,dt,aux);
                
                % Assign to the modal array
                [nDOF,nModes] = size(U);
                modalArray(k) = modeobj(nModes,nDOF);
                modalArray(k).f = fz(1,:)';
                modalArray(k).z = fz(2,:)';
                modalArray(k).u = U;
                modalArray(k).df = sqrt(squeeze(fz_cov(1,1,:)));
                modalArray(k).dz = sqrt(squeeze(fz_cov(2,2,:)));
                [uR_cov,~] = ucovMat2Vec(U_cov);
                modalArray(k).du = uR_cov.^.5;
            else %if not
                % Run the SSI/ref cov algorithm, don't include Tcov
                [A,C,aux] = ssicov(Hcov,m,ddim,hdim,'L',L);
                
                % Convert to modal parameters
                [fz,U] = ss2modal(A,C,dt);
                
                % Assign to the modal array
                [nDOF,nModes] = size(U);
                modalArray(k) = modeobj(nModes,nDOF);
                modalArray(k).f = fz(1,:)';
                modalArray(k).z = fz(2,:)';
                modalArray(k).u = U;
            end
            
            % Define the state space system
            modalArray(k).ss = ss(A,aux.B,C,aux.D,dt);            
            
            if PFCALC
                %TBD for now
%                 % Calculate the discrete-time poles
%                 [Dd,Vd,Wd] = ss2dtp(A);
%                 
%                 %Diagonalize B,C,D matrices
%                 Bdiag = Vd'*aux.B;
%                 Cdiag = C*Vd;                
%                 
%                 % Find the modal residues
%                 pa = zeros(size(A,1)/2,1);
%                 for m = 1:2:size(A,1)-1
%                     pa2 = Bdiag(m)*Cdiag(m,m);
%                     R = diag(Vd(:,m)*Wd(:,m)');
%                     pa((m+1)/2) = sqrt(R'*R);
%                 end
%                 pf = 100*(pa.^2)/(pa'*pa); % relative modal participation (pi)
%                 % Assign to the modal array
%                 modalArray(k).pf = pf;
            end
        end
        disp(['Computation time (s): ' num2str(toc(ssiTimer))])
end
modalArray = modalArray'; %flip array to vertical
