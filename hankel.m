%H = hankel(y, S1)
%Return the block Hankel matrix of the 2D response matrix y where the 
%signals are arranged in the rows, number of sensors equal to the number of
%rows, number of data points equal to the number of columns. S1 is the half
%numnber of block rows on the block Hankel matrix.

%All rights reserved, Rune Brincker, Jun 2010, Mar 2012.

function H = hankel(y, S1)
S = 2*S1;
[Nr, Nc] = size(y);
H = zeros(Nr*S, Nc-S+1);
for s=1:S,
    H(1+Nr*(s-1):Nr*s,:) = y(:,s:Nc-S+s);
end