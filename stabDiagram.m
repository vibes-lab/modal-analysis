function [h,stable] = stabDiagram(h,modalArray,modelOrder,varargin)
%STABDIAGRAM generates a stabilization digram for a modeobj array
%modalArray. The stabilization diagram plots each of the modalArray
%elements vertically according to the model orders specified by modelOrder.
%Those modes which are within the 'stable' threshold of the previous model
%order are labelled as stable, or unstable otherwise.
%
%[H,STABLE] = STABDIAGRAM(H,MODALARRAY) generates the stabilization diagram
%for MODALARRAY, assuming model orders are 1 to N, where N is the number
%of elements in MODALARRAY. Returns the diagram handle H as well as a Nx1
%cell STABLE which contains logical arrays indicating the stable (1) and
%unstable (0) modes. When an axes handle H is provided, plots on the
%corresponing axes, if empty, will plot on the current axes.
%
%[H,STABLE] = STABDIAGRAM(...,MODELORDER) specifies the model orders for
%the diagram,MODELORDER should be an Nx1 vector. If empty, the function
%uses the default (1:N).
%
%[H,STABLE] = STABDIAGRAM(...,Name,Value) returns a stabilization diagram
%handle and stable modes with additional options specified by one or more
%name-value pair arguments. For example, you can set the stable thresholds
%manually.
%
%   The following options control the stabilization diagram thresholds
%
%      Name       Value
%     'df'        Frequency difference ratio. Default is 0.05 (5%).
%     'dz'        Damping difference ratio. Default is 0.1 (10%).
%     'mac'       Minimum MAC value for mode shapes. Default is 0.8.
%
%   The following options control the display
%
%      Name       Value
%     'Spectrum'  Reference spectrum, plotted behind the stabilization
%                 diagram. Object with fields P (the spectrum values), f
%                 (the frequency values), and units (plot units).
%     'StabOnly'  [0 (default),1] Plot only stable modes
%     'Errorbar'  [0 (default),1] Plot modal uncertainties (if defined)
%
%OUTPUTS
%STABLE is an Nx1 cell containing logical arrays. The ith array is mx3,
%where m is the number of modes corresponding to MODALARRAY(i). The columns
%indicate whether that mode is frequency, damping or mode stable,
%respectively.

%% Handle input arguments
orderInd = 1:numel(modalArray); %index of model orders
if nargin < 3 || isempty(modelOrder)
    modelOrder = orderInd;
end

stable = cell(numel(modalArray),1);

%default thresholds, frequency and damping in percent/100, mode shape in MAC
f_th = 0.05; z_th = 0.2; u_th = 0.8;

%default options
PLTSPEC = false;
STABONLY = false;
ERRORBAR = false;
sigma = 1;

%Handle options
sz = numel(varargin);

for k = 1:sz
    if ischar(varargin{k})
        switch varargin{k}
            case 'Spectrum'
                PLTSPEC = true; %plot the spectrum
                spec = varargin{k+1};
                P = spec.P;
                fp = spec.f;
            case 'StabOnly'
                STABONLY = true;
            case 'Errorbar'
                ERRORBAR = true;
                sigma = varargin{k+1};
            case 'df'
                f_th = varargin{k+1}; %frequency diff ratio threshold
            case 'dz'
                z_th = varargin{k+1}; %damping diff ratio threshold
            case 'mac'
                u_th = varargin{k+1}; %frequency ratio threshold
            otherwise
                error('Option %s not recognized',varargin{k});      
        end
    end
end

% Diagram handle
if isempty(h)
    h = gca;
end
hold(h,'on');

%% Plot the reference spectrum (if requested)
if PLTSPEC
    yyaxis right % plot on right axis
    %plot spectra
    for k = 1:size(P,2)
        plot(h,fp,P(:,k),'linewidth',1.5);
        hold on;
    end
    ylabel(spec.units);
    set(h,'yscale','log');
    yyaxis left
end

%% Find the stable modes

%plotting parameters
lw = 2; %linewidth
mss = 10; %marker size (stable)
msu = 5; %marker size (unstable)

% fake starting values for the modal parameters
f_p = 1; z_p = 1; u_p = zeros(size(modalArray(1).u,1),1);
fmin = Inf; fmax = -Inf;

for k = 1:numel(modelOrder) %iterate through the model orders
    
    o_i = orderInd(k); %current model order index
    f = modalArray(o_i).f; %current frequency vector
    z = modalArray(o_i).z; %current damping vector
    u = modalArray(o_i).u; %current mode matrix
    df = modalArray(o_i).df; %current frequency standard deviation
    nModes = numel(f); %number of modes
    stablek = false(nModes,3);
    
    %find min and max frequencies (for x axis limits)
    fmin = min(fmin,min(f));
    fmax = max(fmax,max(f));
    
    for c = 1:nModes %for each mode in the current model order
        
        %calculate differences & MAC vlaues
        f_diff = abs(f_p-f(c))./f_p; %vector of frequency difference ratios
        z_diff =abs(z_p-z(c))./z_p; %vector of damping difference ratios
        u_mac = calculateMAC(u(:,c),u_p); %mac matrix
        
        [f_min, st_index] = min(f_diff); %find the minimum frequency difference
        f_st = f_min < f_th; %test the freq threshold
        %test the damping threshold and also enforce physical limits (0 to
        %10% damping)
        z_st = z_diff(st_index) < z_th && z(c) < 0.1 && z(c) > 0;
        u_st = u_mac(st_index) > u_th; % test the mac threshold
        
        if ERRORBAR
            if (f_st && z_st && u_st) || k == 1
                %all thresholds are met OR first model order => stable mode
                
                %plot errorbars for stable points
                errorbar(h,f(c),modelOrder(k),[],[],sigma*df(c),sigma*df(c),...
                        'b.','linewidth',1,'markersize',0.1);                
            else
                if ~STABONLY
                    % plot errorbars for unstable points
                    errorbar(h,f(c),modelOrder(k),[],[],sigma*df(c),sigma*df(c),...
                        'r.','linewidth',1,'markersize',0.1); 
                end
            end
        end
        
        if (f_st && z_st && u_st) || k == 1
            %all thresholds are met OR first model order => stable mode 
            
            %plot as stable point
            if ~exist('stb','var')
                %store a point for legend
                stb = plot(h,f(c),modelOrder(k),'b.','linewidth',lw,'markersize',mss);
            else
                plot(h,f(c),modelOrder(k),'b.','linewidth',lw,'markersize',mss);
            end
        elseif f_st && u_st
            %plot as damping unstable
            if ~exist('zus','var')
                %store a point for legend
                zus = plot(h,f(c),modelOrder(k),'bo','linewidth',lw-1,'markersize',mss-6);
            else
                plot(h,f(c),modelOrder(k),'bo','linewidth',lw-1,'markersize',mss-6);
            end
        else
            if ~STABONLY
                % plot unstable mode
                if ~exist('usm','var')
                    %store a point for legend
                    usm = plot(h,f(c),modelOrder(k),'rx','linewidth',lw,'markersize',msu);
                else
                    plot(h,f(c),modelOrder(k),'rx','linewidth',lw,'markersize',msu);
                end
            end
        end
        stablek(c,:) = [f_st,z_st,u_st];
    end
    
    stable = stablek;
    f_p = f; z_p = z; u_p = u; %store as previous
end

axes(h)
ylabel('Number of Modes');
xlabel('Frequency (Hz)');
legMrk = [];
legStr = {};
cnt = 0;
if exist('stb','var')
    cnt = cnt+1;
    legMrk(cnt) = stb;
    legStr{cnt} = 'Stable';
end

if exist('zus','var')
    cnt = cnt+1;
    legMrk(cnt) = zus;
    legStr{cnt} = 'Damping Unstable';
end

if exist('usm','var')
    cnt = cnt+1;
    legMrk(cnt) = usm;
    legStr{cnt} = 'Unstable';
end
    
legend(legMrk,legStr,...
    'location','south','AutoUpdate','off');

xlim([fmin-0.5 fmax+0.5]);
ylim(modelOrder([1 end]));
box on
grid on