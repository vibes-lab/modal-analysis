function [DATA,META,timeStamp] = preprocessDATA(DATA,META,index,r,wc)
tic
if nargin < 3
    r = 1;
    wc = 1.5;
elseif nargin < 4
    r = 1;
    wc = 1.5;
elseif nargin < 5
    wc = 1.5;
end

%% Change accel data to matrix for easier processing
sensorR = [DATA(:).accel];

%% Index the requested data points only, default is all
if (~isempty(index))
    sensorR = sensorR(index,:);
else
    index = 1:length(sensorR);
end
toc


%% =========== Meta Info ================
acqdate = META.acquisitionStartTime; %Set the starting date and time for the acquisition

fs = META.fs;
t = 0:1/fs:(META.duration-1/fs);
timeStamp = acqdate+duration(0,0,t(index));

%% =============== Filter and Detrend ==============
if wc > 0
    [b,a] = butter(4,2*wc/fs,'high');
    %sensor_filt = detrend(filtfilt(b,a,sensorR));
    sensor_filt = filtfilt(b,a,sensorR);
    %freqz(b,a)
else
    sensor_filt = detrend(sensorR,'linear');
end

%% ================ Decimate ==================

[nsamp,nsens] = size(sensor_filt);
sensor = zeros(ceil(nsamp/r),nsens);

if r~= 1
    if r> 12
        dec_order = factor(r);
    else
        dec_order = r;
    end
    
    %decimate
    for i = 1:nsens
        temp = sensor_filt(:,i);
        for k = 1:numel(dec_order)
            temp = decimate(temp,dec_order(k));
        end
        sensor(:,i) = temp;
    end
    
    META.fs = META.fs/r;
    META.dt = 1/META.fs;
else
    sensor = sensor_filt;
end

timeStamp = downsample(timeStamp,r);

%% =============== Update DATA and META ======================
for k = 1:nsens
    DATA(k).accel = sensor(:,k);
end

META.acquisitionStartTime = timeStamp(1);
META.duration = numel(timeStamp)/META.fs;
META.N_samples = numel(timeStamp);