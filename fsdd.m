% [V, D] = fsdd(f, Gy, F, dt)
%Identification using frequency spatial domain decomposition (FSDD), 
%expanded to multiple singular vectors. The
%identification is performed on the spectral density function supplied in
%the 3D iput varibale Gy using the identification parameters provided in 
%the input variable F. The mode shapes and poles are returned in the
%matrices V, D, V has 2M number of columns and D is a 2M-by-2M diagonal
%matrix where M is the number of modes. the number of modes are decided by
%the number of columns in the input parameter 2D array F.
%f:     Freqeuncies over which the spectral density matrix is defined
%Gy:    Spectral density matrix, the quantity Gy(:,:,k) is the SD matrix at
%       freqeuncy f = (k-1)*df, where df is the frequency resolution.
%F:     ID input parameters. F(1,m), defines the initial frequency of mode 
%       m, F(2,m), defines the width of the freqeuncy band to be used for 
%       ID and finally F(3,m) defines the number of points to be used for
%       identification of damping on the modal coordinate in time domain.
%dt:    Sampling time step
%D:     Identified poles in the 2M diagonal matrix
%V:     2M identified mode shapes

% References: 
% Brincker & Ventura, (2015) "Introduction to operational modal analysis."
    % Chp.10.
% Brincker et al. (2000) "Modal identification of output-only systems 
    %using frequency domain decomposition."
% Zhang et al. (2005) "?A frequency-spatial domain decomposition (FSDD)
    % technique for operational modal analysis."

function [V2, D2] = fsdd(f, Gy, F, dt)
df = f(2) - f(1); %frequency resolution
[gr, gc, nk] = size(Gy);
[~, nm] = size(F); %number of modes to extract
Vid = zeros(gr, nm); %set of singular vectors for each id freq
Uid = zeros(gc,nm);
D1 = zeros(nm, nm); %discrete time poles
V2 = zeros(gr, 2*nm); %V1 including complex conj
D2 = zeros(nm, 2*nm); %D1 including complex conj
figure
pltspec(Gy, dt, size(F,1), 'k', 'LineWidth',.75);
hold on
for m=1:nm % iterate over id frequencies
    k = ceil(F(1,m)/df)+1; % closest frequency index 
    [V,S,U] = svd(Gy(:,:,k)); 
    Vid(:,m) = V(:,1); %singular vector to id
    Uid(:,m) = U(:,1);
    plot((k-1)*df, S(1,1), 'ok', 'LineWidth',1); %plot frequency
end
Gm = zeros(nm, nk);
Vp = pinv(Vid)'; %modal filter matrix (eq 10.31, pg 272)
Up = pinv(Uid)';
ax = gca;
ax.ColorOrderIndex = 1;
for m=1:nm
    k1 = fix(F(2,m)/df);
    k2 = fix(F(3,m)/df);
    for k=k1:k2
        Gm(m,k) = real(Vp(:,m)'*Gy(:,:,k)*Up(:,m)); %modified modal filter
    end
    plot(f, Gm(m,:), 'LineWidth',2) % plot the filtered modal decompositions
    hold on
end

G = [Gm, conj(fliplr(Gm(:,2:nk-1)))]; %symmetric spectrum
R = ifft(G.'); %calculate CF matrix
R1 = R(1:nk,:).';
figure


col = lines(100);
for m=1:nm
    Re = R1(m,1:F(4,m))*0; %initialize estimate
    [~,d] = id_itd1(R1(m,1:F(4,m))); %1 dof ITD solution
    D1(m,m) = d(1,1); %discrete time pole
    for k=1:F(4,m)
        %R1(m,1) is the initial condition, step through the discrete pole
        %to generate the decaying CF function
        Re(k) = R1(m,1)*real(d(1,1)^(k-1));
    end
    
    subplot(nm,1,m);
    pdata = plot(R1(m,1:F(4,m)), 'k.', 'LineWidth',2);
    hold on
    pest = plot(Re, 'LineWidth',2,'Color',col(m,:));
    title([num2str(F(1,m)) ' Hz'])
    legend([pest pdata],'Data','Estimated');
    ylabel('[Unit squared]')
    xlabel('Discrete time')
    grid
    D2(2*(m-1)+1, 2*(m-1)+1) = D1(m, m);
    D2(2*(m-1)+2, 2*(m-1)+2) = conj(D1(m, m));
    V2(:,2*(m-1)+1) = Vid(:,m);
    V2(:,2*(m-1)+2) = conj(Vid(:,m));
end