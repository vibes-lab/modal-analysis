function [varargout] = ss2modal(A,C,dt,aux)
% SS2MODAL returns continuous time modal parameters for a given state
% space system with a system matrix A and output matrix C and sampling
% period dt. The optional argument aux provides the auxilary matrices
% necessary for calculating the modal parameter uncertainties, obtained
% from the ssicov function.

% [fz,phi] = ss2modal(A,C,dt)
% [fz,phi,fz_cov,phi_cov] = ss2modal(A,C,dt,aux)

% OUTPUTS
% fz: mx2 double, where 2m is the number of stable poles of A. The columns
% contain the natural frequencies and damping ratios of each stable pole.
% phi: lxm double, unity scaled modal vectors (mode shapes) where l
% corresponds to the number of outputs defined by C.
% fz_cov: mx2x2 double, frequency-damping covariance matrix for each stable
% mode
% phi_cov: mxlxl double, covariance matrix for each stable mode shape
%% Extract discrete poles
[Vdn,Dd,Wdn] = eig(A); % distrete time right EVs, poles and left EVs

%% Discard unstable poles
[Vds,Dds] = stableonly(Vdn,Dd);
[Wds,~] = stableonly(Wdn,Dd);
if size(Vds,1) ~= size(Vdn,1)
    warning('There are unstable poles in your system definition. If this causes errors, try increasing your model order (number of DOFs).');
end

%% Extract state matrix covariance
n = size(A,1); %State space order
l = size(C,1); %number of outputs

%% Compute modal parameters
Dc = log(Dds)/dt; %convert to continuous time poles
[lam_c,Vss,I] = modsort(Dc,Vds); %sort by pole magnitude
% note that lam_c is now a vector corresponding to Dc
Wss = Wds(:,I); %sort left EVs in the same way
dd = diag(Dds); %Dds is a diagonal matrix, convert to vector
lam_d = dd(I); %sorted vector of discrete time poles
fz = pol2mod(lam_c); %convert poles to frequency damping
B = C*Vss; %convert to the physical mode shape
[~,nMode] = size(B); %number of modes
[~,ku] = max(abs(B),[],1); %indeces of the max for each mode shape

phi = B*0; %initialize phi
Bmax = zeros(nMode,1); %initialize Bmax
for k1 = 1:nMode %for each mode shape
    Bmax(k1) = B(ku(k1),k1); %get the max value
    phi(:,k1) = B(:,k1)/Bmax(k1); %scaled modal vector by the max
    phi(ku(k1),k1) = 1; % for robustness, force max to be exactly 1 
    %(sometimes the complex math is not exact due to rounding errors)
end

%% Assign outputs
varargout{1} = fz;
varargout{2} = phi;

%% Modal param uncertainty
if nargin == 4
    n_s = numel(lam_c); %number of stable modes

    fz_cov = zeros(2,2,n_s);
    phi_cov = zeros(2*l,2*l,n_s);
    for i = 1:n_s
        lam_ci = lam_c(i);
        lam_di = lam_d(i);

        Qi = kron(Vss(:,i).',eye(n))*(-lam_di*aux.PQ1+aux.PQ23);

        Jlci = [real(lam_di) imag(lam_di);...
                -imag(lam_di) real(lam_di)]/dt/abs(lam_di)^2;
        Jfz = (1/abs(lam_ci))*[real(lam_ci)/2/pi, imag(lam_ci)/2/pi;
                -imag(lam_ci)^2/abs(lam_ci)^2 ,real(lam_ci)*imag(lam_ci)/abs(lam_ci)^2];
        Jfzli = Jfz*Jlci;

        JTli = Wss(:,i)'*aux.OO*Qi/(Wss(:,i)'*Vss(:,i));
        JTpi = pinv(lam_di*eye(n)-A)*...
            (eye(n)-Vss(:,i)*Wss(:,i)'/(Wss(:,i)'*Vss(:,i)))*aux.OO*Qi;
        JpACi = (eye(l)-[zeros(l,ku(i)-1) phi(:,i) zeros(l,l-ku(i))])*...
            (C*JTpi+kron(Vss(:,i).',eye(l))*aux.Q4)/Bmax(i);

        Ufzi = Jfzli*[real(JTli);imag(JTli)];
        Upi = [real(JpACi);imag(JpACi)];

        fz_cov(:,:,i) = Ufzi*Ufzi.';
        phi_cov(:,:,i) = Upi*Upi.';
    end
    
    varargout{3} = fz_cov;
    varargout{4} = phi_cov;
end