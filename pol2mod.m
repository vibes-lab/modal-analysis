% fz = pol2mod(lam)
%Calculates the modal paramters based on the continous time poles. The 
%array fz is 2xN, where N is the number of modes and the first 
%row contains the undamped natural frequencies, the second row contains the
%damping ratios. Lam is a 1xN array containing the poles, one for each
%mode.

% All rights reserved, Rune Brincker, Oct 2010, Jul 2012.

function fz = pol2mod(lam)
M = length(lam);
fz = zeros(2, M);
for m=1:M,
    om0 = abs(lam(m));
    fz(1,m) = om0/(2*pi);
    fz(2,m) = -real(lam(m))/om0;
end