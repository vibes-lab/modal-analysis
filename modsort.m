% [lam1 B1] = modsort(D, V)
%Sorts the eigenvectors in the matrix V and the eigenvalues in the diagonal
%matrix D after ascending frequency and returns the sorted eigenvalues and
%sorted eigenvectors. If the system has N DOF's, then the input eigenvalue
%matrix D is assumed to be 2N x 2N. The eigenvector matrix V is assumed to
%have R x 2N columns, where the number of rows R is arbitrary. 
%The sorted eigenvalues are returned in the 1 x N array lam1 and the 
%correponding eigenvectors are returned in the R x N matrix B1.

% All rights reserved, Rune Brincker, Oct 2010, Jul 2012.

function [lam1, B1, index] = modsort(D, V)
d = diag(D);
[nr, nc] = size(V);
M = nc/2;
om0 = zeros(1,M);
zz = zeros(1,M);
B0 = zeros(nr,M);
for m=1:M,
    n = (m-1)*2 + 1;
    om0(m) = abs(d(n));
    zz(m) = - real(d(n))/om0(m);
    B0(:,m) = V(:,n);
end
[om1, index] = sort(om0, 'ascend');
zz1 = zz(index);
B1 = B0(:,index);
fz = [om1/(2*pi); zz1];
lam1 = mod2pol(fz);
index = index*2-1;
    