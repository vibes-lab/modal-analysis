%H = bhankel(Y, R)
%Return the block Hankel matrix of the 2D or 3D response matrix Y(r,c,k)
%where the r is the row entry, c is the column entry and k is the time lag
%entry such that the response matrix at time lag k*dt is Y(:,:,k) where dt
%is the sampling time step. If a 2D array is provided then the data is
%treated similarly where the response data at time lag k*dt is Y(:,k). R is 
%the number of block rows in the block Hankel matrix.

%All rights reserved, Rune Brincker, Jun 2010, Mar 2012, Aug 2012.

function H = bhankel(Y, R)
[nr, nc, nk] = size(Y);
if (nk == 1),
    nk = nc;
    H = zeros(nr*R, nk-R+1);
    for r=1:R,
        for c=1:nk-R+1
            H(1+nr*(r-1):nr*r,c) = Y(:,r+c-1);
        end
    end
else,
    H = zeros(nr*R, (nk-R+1)*nc);
    for r=1:R,
        for c=1:nk-R+1
            H(1+nr*(r-1):nr*r,1+nc*(c-1):nc*c) = Y(:,:,r+c-1);
        end
    end
end
