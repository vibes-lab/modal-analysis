%% Calculate the modal assurance criterion (MAC)
function MAC = calculateMAC(matrixA,matrixX)
%% User defined parameters
% matrixA = squeeze(modeMatrixFE(:,1,:));
% matrixX = modeMatrixExpSingle;

%% MAC calculation
nModesA = size(matrixA,2);
nModesX = size(matrixX,2);
MAC = zeros(nModesA,nModesX);

for r = 1:nModesA
    for q = 1:nModesX
        phiA = matrixA(:,r);
        phiX = matrixX(:,q);
        nom = abs(phiA'*phiX)^2;
        den = (phiA'*phiA)*(phiX'*phiX);
        MAC(r,q) = abs(nom/den);
    end
end

        