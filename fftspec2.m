function [f, Gyy] = fftspec2(y,N,dt)
%Estimates the spectral density matrix as a function of frequency using the
%Welch averaging technique with a Hanning window and 50 % overlap.
%y:     Signal to be processed, measurement channels arranged in the rows    
%N:     Number of data points in the data segments (window size)
%dt:    Sampling time step
%f:     Vector containing the positive frequency axis
%Gyy:   Spectral density matrices for the positive frequency axis
%The function is using the built-in Matlab routine cpsd.m. If no output
%arguments are used in the call, then the singular values of the spectral
%density matrices are plotted - or the auto spectral density in case of
%uni-variate signal.

%% Main
% Calculate cpsd parameters
[~,nc] = size(y);
nfft = max(256,2^nextpow2(N));
if mod(nfft,2) == 0
    nf = nfft/2+1;
else
    nf = (nfft+1)/2;
end
wdw = hanning(N); %hanning window
novlp = floor(N/2); %50% overlap

%Initialize matrices
Gyy = zeros(nf,nc,nc); %Estimated normal FRF matrix: H(w)'*H(w)
%Take the cross-PSD of every data channel to create G
for r = 1:nc
    for c = r:nc
        [Pxy,f] = cpsd(y(:,r),y(:,c),wdw,novlp,nfft,1/dt);
        Gyy(:,r,c) = Pxy;

        %Symmetric condition
        if r~=c
            Gyy(:,c,r) = Pxy;
        end
    end
end

%% Plot
if (nc == 1)
    Gyy = reshape(Gyy, 1, nf);
end
Gs = zeros(nf, nc);
if (nargout == 0)
    if (nc == 1)
        plot(f, 10*log10(Gyy'), 'k', 'LineWidth',2)
        title('Auto spectral density')
    else
        for k=1:nf
            [U,S,V] = svd(Gyy(:,:,k));
            Gs(k, :) = diag(S)';
        end
        plot(f, 10*log10(Gs), 'k', 'LineWidth',2)
        title('Singular values of spectral matrix')
    end
    ylabel('dB rel. to unit')
    xlabel('Frequency [Hz]')
    figure(gcf)
    grid
end
