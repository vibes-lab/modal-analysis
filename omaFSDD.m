function [modObj, spec, info] = omaFSDD(y,dt,opts)
% OMAFSDD Estimate modal parameters for a system with measured outputs y 
%   using Frequency Spacial Domain Decomposition (FSDD)

%   MODOBJ = OMAFSDD(Y,DT,BLOCKROWS) returns a modal object MODOBJ
%   with the modal parameters for a system of output
%   responses Y. DT specifies the sampling period. Requires the user to
%   select desired spectral peaks via a graphical interface.

%   MODOBJ = OMASFSDD(...,OPTS) allows the used to modify the modal
%   anlysis method settings by specifying specific parameters.

%INPUT PARAMETERS

%   OPTS is a matlab structure with the following fields

%   projChan = [vector] indeces of the channels (columns) to be
%       used as projection channels.
%   N = [double] (optional) no. of time samples used to estimate
%       autocorrelations. Used to calculate the spectral density matrix, which
%       is used in the stabilization diagram display, but does not impact the
%       SSI algorithm. Also used to calculate the correlation function matrix,
%       which is used to estimate participation factors (when specified in OPT)
%   PFCALC = [0 (default),1] Compute modal participation factors.

%Rodrigo Sarlo, Nov 2020
%% Handle input arguments
y = y'; %function convention is for channel rows rather than channel columns

%% Assign defaults
%default values
N = floor(size(y,2)/5);
projChan = size(y,1);
PFCALC = false;
units = '[-]';
selectMethod = 'manual';

%update defaults if specified
if nargin > 2
    if isfield(opts,'selectMethod')
        selectMethod = opts.selectMethod;
    end
    if isfield(opts,'N')
        N = opts.N;
    end
    if isfield(opts,'projChan')
        projChan = opts.projChan;
    end
    if isfield(opts,'PFCALC')
        PFCALC = opts.PFCALC;
    end
    if isfield(opts,'units')
        units = opts.units;
    end
end

%% =================== FSDD Main ==========================
%% STEP 0: Build spectral density matrix
Gtimer = tic;
[fy,Gy] = fftspec(y, N, dt); % full spectrum SD using welch method
%[fy,Gy] = fftspec2(y, settings.N, dt); % full spectrum SD using welch method
% [fhy, Ghy] = halfspecw(y_norm, N, dt); %empirical half spectrum SD matrix
% NOTE: Both Gy and Ghy are similar, however the columns of Ghy are
% proportional to the modeshapes, thus is better for FD estimation
disp(['SD Matrix calculation (s): ' num2str(toc(Gtimer))]);

%projection channel SD matrix (used for stabilization diagram)
Gp = Gy(projChan,:,:);

% Calculate the projection channel SD matrix SVD
[ns,ns2,nf] = size(Gp);
if nf == 1
    Gp = permute(Gp,[1 3 2]);
    nf = ns2;
end

if PFCALC %if participation factor calculation is desired
    %Calculate the empirical CF matrix (used for participation
    %factor estimation)
    Rtimer = tic;
    R = dircor(y, N*2); %filtered positive lag correlation (CF Matrix)
    % NOTE: Is there an option to average R's to generate cleaner CF matrices??
    % NOTE 2: This takes a long time, can it be more efficient?
    disp(['CF Matrix calculation (s): ' num2str(toc(Rtimer))])
end

%% STEP 1: Decompose the SD matrix
sv = zeros(nf,ns);
for k=1:nf
    [~,S,~] = svd(Gp(:,:,k));
    if ns ~= 1
        sv(k, :) = diag(S)';
    else
        sv(k, :) = S(1);
    end
end

% Assign to spectrum structure
if size(y,1) > 3
    nsv = min(numel(projChan),4);
    spec.P = sv(:,1:nsv);
else
    spec.P = sv(:,1:size(y,1));
end
spec.f = fy;
if isfield(opts,'units')
    spec.units = ['(' units '^2)/Hz'];
else
    spec.units = '[-]';
end

%% STEP 2: Choose frequencies and extract modal parameters
switch selectMethod
    case 'manual'
        %User picks manually from spectrum
        %% plot the SVD spectrum
        fig1 = figure;
        h = axes(fig1);
        for k = 1:size(spec.P,2)
            plot(h,spec.f,spec.P(:,k),'k-','linewidth',.75);
            hold on;
        end
        ylabel(spec.units);
        xlabel('Frequency [Hz]');
        title('Select modal ranges from diagram');
        set(h,'yscale','log');
        grid on

        %% ask the user to pick the frequency peaks
        exit = false;
        F = zeros(4,1);
        numModes = 0;
        
        dcm_obj = datacursormode(fig1);
        clc
        disp(['Select a frequency range for which to extract a'...
                ' single mode.']);
        disp(['Select the lower bound, hit enter, then'...
            ' select the upper bound and hit enter again.']);
        disp('Hit Esc after selecting all desired modes.');
        
        %% Selection loop
        col = lines(100);
        c = 1;
        while ~exit
            numModes = numModes+1;
            set(dcm_obj,'DisplayStyle','datatip',...
                'SnapToDataVertex','on','Enable','on')

            %figure(f1)
            curs_freq = zeros(2,1); %initalize selected freq value
            curs_val = zeros(2,1); %initiallize selected sv value
            for k = 1:2
                b = 0;
                while b == 0
                    b = waitforbuttonpress;
                end
                value = double(get(fig1,'CurrentCharacter'));
                exit = value == 27;

                if ~exit
                    c_info = getCursorInfo(dcm_obj);                    
                    c_pos = c_info.Position;

                    curs_freq(k) = c_pos(1);
                    curs_val(k) = c_pos(2);
                    plot(h,curs_freq(k),curs_val(k),'.','MarkerSize',10,...
                        'Color',col(c,:));
                else
                    break
                end
            end
            
            if ~exit
                %% Find the appropriate indeces for the frequency value
                f1 = find(spec.f == curs_freq(1));
                f2 = find(spec.f == curs_freq(2));
                [~,fr_max] = max(spec.P(f1:f2));
                fi_max = fr_max+f1-1; %true index
                f_id = spec.f(fi_max); %the ID frequency is the max in the range (peak)
                plot(h,spec.f(f1:f2),spec.P(f1:f2),'-','Linewidth',2,...
                    'Color',col(c,:)); % Make selected line wider
                plot(h,spec.f(fi_max),spec.P(fi_max),'ok','MarkerSize',6); %mark the ID frequency
                c = c+1; %next color

                %% Build the ID parameter vector 
                % ID frequency is for extracting mode shape
                % Frequency range is for damping estimation
                % Number of points is for damping estimation. N/2 is the
                % maximum length (total number of Gy frequency points).
                % Number is capped at 1000 to avoid excessive computation.
                F(:,numModes) = [f_id;spec.f(f1);spec.f(f2);min(N/2,1000)];
            end
        end
        datacursormode off
        close(fig1);
       
        if ~isempty(F)
            [Vd,Dd] = fsdd(fy,Gy,F,dt); %run FSDD algorithm
            [Vds,Dds] = stableonly(Vd,Dd);%% Discard unstable poles
            %% Compute modal parameters
            Dc = log(Dds)/dt; %convert to continuous time poles
            [lam_c,Vss,~] = modsort(Dc,Vds); %sort by pole magnitude
            fz = pol2mod(lam_c); %convert poles to frequency damping
            
            modObj = modeobj(numel(lam_c),size(Vss,1));
            modObj.f = fz(1,:)';
            modObj.z = fz(2,:)';
            modObj.u = real(Vss);
            info.method = 'fsdd';
            info.F = F;
        else
            modObj = [];
            info = [];
        end
    case 'array'
        try
            F = opts.F;
        catch err
            error('opts.F must be defined');
        end
        if ~isempty(F) && size(F,1) == 4
            [Vd,Dd] = fsdd(fy,Gy,F,dt); %run FSDD algorithm
            [Vds,Dds] = stableonly(Vd,Dd);%% Discard unstable poles
            %% Compute modal parameters
            Dc = log(Dds)/dt; %convert to continuous time poles
            [lam_c,Vss,~] = modsort(Dc,Vds); %sort by pole magnitude
            fz = pol2mod(lam_c); %convert poles to frequency damping
            
            modObj = modeobj(numel(lam_c),size(Vss,1));
            modObj.f = fz(1,:)';
            modObj.z = fz(2,:)';
            modObj.u = real(Vss);
            info.method = 'fsdd';
            info.F = F;
        else
            error('Frequency list (opts.F) was empty or not [4xN].');
        end
    case 'auto'
        % Under development
end
    