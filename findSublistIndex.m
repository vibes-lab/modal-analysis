function [Is,I] = findSublistIndex(List,Sublist)
n = length(List);
I = false(1,n);
Is = zeros(1,length(Sublist));

if ~isnumeric(Sublist)
    if ~iscell(Sublist)
        Sublist = {Sublist};
    end

    for k = 1:length(Sublist)
        searchItem = Sublist{k};
        index = find(strcmp(List,searchItem));
        if isempty(index)
            disp([searchItem ' not found.']);
        else
            I(index) = true;
            Is(k) = index;
        end
    end
else
    for k = 1:length(Sublist)
        searchItem = Sublist(k);
        index = find(List == searchItem);
        if isempty(index)
            disp([num2str(searchItem) ' not found.']);
        else
            I(index) = true;
            Is(k) = index;
        end
    end
end