function [varargout] = buildHcovINS(u,y,s,Ipc,nb)
% Builds an output covariance matrix corresponding to the input null space.
% This can be used to remove the input's influence and make it possible to
% run SSI Cov in an EMA context.

% Refs:
% [1] Dohler et al. (2013) "Efficient multi-order uncertainty..."
% [2] Gandino et al. (2013) "Covariance-driven subspace ID: A complete.."

[l,ny] = size(y);
[n,~] = size(u);
if isempty(Ipc)
    % full SSI
    Ipc = 1:l;
end
q = s;
p = 2*s-q-1;
j = ny-p-q; % column dimension of full Hcov

%% Defaults
if nargin < 4
    Ipc = 1:ny;
    nb = 1;
elseif nargin < 5
    nb = 1;
end

r = length(Ipc); %number of references
datadim = [l,r];
hankeldim = [q,p,j];

%% Build the output matrices
%see Sec 2.3 [1] or Eq. 8 [2]
Ym = zeros(r*q,j);
Yp = zeros(l*q,j);
Um = zeros(n*q,j);
Up = zeros(n*q,j);
for k = 1:q
    Ym((q-k)*r+1:(q-k+1)*r,:) = y(Ipc,k:k+j-1); %assign block row
    Yp((k-1)*l+1:k*l,:) = y(:,q+k:j+q+k-1); %assign block row
    Um((q-k)*n+1:(q-k+1)*n,:) = u(:,k:k+j-1); %assign block row
    Up((k-1)*n+1:k*n,:) = u(:,q+k:j+q+k-1); %assign block row
end

%% Build the covariance matrices
Cpp = Yp*Up.'/j;
Cpm = Yp*Um.'/j;
Cmp = Ym*Up.'/j;
Cmm = Ym*Um.'/j;
Rpp = Up*Up.'/j;
Rpm = Up*Um.'/j;
Rmp = Rpm.';
Rmm = Um*Um.'/j;

R = Yp*Ym.'/j;
Hcov0 = R-[Cpp Cpm]*pinv([Rpp Rpm;Rmp Rmm])*[Cmp.';Cmm.'];

%% Find the covariance of the output covariance!
% see sec 3.2 and 5.1
Nb = floor(j/nb);
T = zeros((p+1)*q*l*r,nb);
Hvec = reshape(Hcov0,[],1);
Hcov = zeros((p+1)*l,q*r);
for k = 1:nb
    Yp_k = Yp(:,(k-1)*Nb+1:k*Nb);
    Ym_k = Ym(:,(k-1)*Nb+1:k*Nb);
    Up_k = Up(:,(k-1)*Nb+1:k*Nb);
    Um_k = Um(:,(k-1)*Nb+1:k*Nb);
    
    Cpp_k = Yp_k*Up_k.'/Nb;
    Cpm_k = Yp_k*Um_k.'/Nb;
    Cmp_k = Ym_k*Up_k.'/Nb;
    Cmm_k = Ym_k*Um_k.'/Nb;
    Rpp_k = Up_k*Up_k.'/Nb;
    Rpm_k = Up_k*Um_k.'/Nb;
    Rmp_k = Rpm_k.';
    Rmm_k = Um_k*Um_k.'/Nb;
    
    R_k = Yp_k*Ym_k.'/Nb;
    Hcov_k = R_k-[Cpp_k Cpm_k]*pinv([Rpp_k Rpm_k;Rmp_k Rmm_k])*[Cmp_k.';Cmm_k.'];
    Hcov = Hcov+Hcov_k/nb;
    Hcov_vec_k = reshape(Hcov_k,[],1);
    T(:, k) = (Hcov_vec_k-Hvec)/sqrt(nb*(nb-1));
end
disp(['Hcov sanity check: ' num2str(max(max((Hcov-Hcov0))))])
varargout{1} = Hcov;
varargout{2} = datadim;
varargout{3} = hankeldim;
varargout{4} = T;
varargout{5} = Cmp.';

