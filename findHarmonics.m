function harmf = findHarmonics(X,fs,df)
% FINDHARMONICS Find the frequency bands corresponding to harmonic, that
% is, deterministic vibration components in a time signal. This is based on
% the assumption that a harmonic signal will have a kurtosis value of less
% than 2.

% HARMF = FINDHARMONICS(X,fs,df) returns a N x 2 double HARMF with the
% harmonic bands where HARMF(:,1) are the N starting frequencies and
% HARMF(:,2) are the N stopping frequencies. X is a nxc matrix of time data
% with n samples and c channels. fs is the sampling rate and df is the
% desired frequency resolution of the harmonics search.

%% Preliminary search based on PSD peaks
%Since filtering is a computationally intensive operation, search first for
%possible harmonics as peaks in the PSD

[nSamp,nChan] = size(X);
wdwsize = floor(100/df); % Set the PSD window size so that resolution is
%twice that requested
if wdwsize > nSamp
    error('The resolution (df) must be greater than 2/size(X,1)')
end
nfft = max(256,2^nextpow2(wdwsize)); % no. of fft points should equal
% closest power of 2 of window size, but no less that 256.

% compute one-sided psd
[Pxx,fxx] = pwelch(X,wdwsize,floor(wdwsize/2),nfft,fs);

%PSD gradient search
pkm = [];
pkf = []; %frequencies of possible harmonic peaks
harmI = 0*fxx; %possible harmonic peak indeces
for j = 1:nChan
    gdnt = gradient(Pxx(:,j)); %PSD gradient to evaluate sharpness
    harmIj = gdnt > mean(gdnt)+ 1*std(gdnt); %
    pkm = [pkm;Pxx(harmIj,j)]; %possible harmonic peak frequencies
    pkf = [pkf;fxx(harmIj)]; %possible harmonic peak frequencies
    
    harmI = harmI | harmIj;
    %   Uncomment for gradient visualization
    %     figure
    %     plot(fxx,gdnt);
    %     hold on
    %     plot(fxx(harmi),gdnt(harmi),'ko');
end
peakf = fxx(harmI); %final list of possible harmonic peaks

% plot time series and psd
figure
plot(fxx,Pxx);
hold on
plot(pkf,pkm,'ko');
set(gca,'yscale','log');

if sum(harmI) == 0
    %If no possible harmonic peaks are found simply return an empty array
    harmf = [];
    return
end

%% Definitive search based on kurtosis
%The possible harmonic peaks define the start point of the search, it then
%continues until kurtosis is no longer below 2. The search then considers
%the next eligible peak which was not covered by the preceeding search

fbands = 0:df:fs/2; %full frequency range of filter bands
N = numel(fbands); %number of filter bands
harmf = zeros(500,2); %list of harmonic bands

%starting parameters
count = 0; %harmonic count
newband = 1; %indicator that search is starting over a new frequency band
f = peakf(1); %initialize starting frequency
while f < fs/2
    SBf1 = max(f,0.01*df); %First stop band freq
    SBf2 = min(f+df,fs/2-0.01*df); %Second stop band freq
    PBf1 = SBf1+df/5; %First pass band freq
    PBf2 = SBf2-df/5; %Second pass band
    
    d = designfilt('bandpassfir', ...       % Response type
        'FilterOrder',60, ...            % Filter order
        'StopbandFrequency1',SBf1, ...    % Frequency constraints
        'PassbandFrequency1',PBf1, ...
        'PassbandFrequency2',PBf2, ...
        'StopbandFrequency2',SBf2, ...
        'DesignMethod','ls', ...         % Design method
        'StopbandWeight1',1, ...         % Design method options
        'PassbandWeight', 2, ...
        'StopbandWeight2',3, ...
        'SampleRate',fs);
    
    Xfilt = filtfilt(d,X); %filter the data
    
    K = kurtosis(Xfilt); %find the kurtosis
    
    
    if min(K) < 2.2
        %kutosis check indicates harmonics
        if newband
            newband = 0;
            count = count +1;
            harmf(count,1) = SBf1;
        end
        f = f+df; % increment the frequency
        if f > fs/2
            %the next step is out of range
            harmf(count,2) = SBf2; %stop the search
        end
    else
        %no indication of harmonics
        if ~newband
            harmf(count,2) = SBf1; %stop the search
        end
        newband = 1; %signal a new search
        I = find(peakf > f); %find the next possible frequency that was not scanned
        if ~isempty(I)
            f = peakf(I(1)); %make it the next starting frequency
        else
            break
        end
    end
end
harmf = harmf(1:count,:)-df;
