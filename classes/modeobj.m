classdef modeobj
    % MODEOBJ Holds general information about modal properties and FRF
    properties
        nModes(1,1) double 
        ndof(1,1) double 
        nInput(1,1) double
        f double %natural frequencies
        z double %damping ratios
        u double %mode shapes
        pf double%modal participation factors
        df double%natural frequency standard deviation (sigma)
        dz double%damping ratio standard deviation (sigma)
        du double%mode shape standard deviation (sigma)
        ss %discrete-time state space definition (if B and D empty, 
        %this is a output-only modal identification)
        index double %[ndof x 1] sensor indeces used for merging modeObj instances
    end
    
    properties (Dependent)
        
    end
    
    methods
        %% Declaration
        function obj = modeobj(nModes,ndof,varargin)
            %declaration order corresponds to property order below
            obj.nModes = nModes; %number of modes
            obj.ndof = ndof; %number of degrees of freedom (outputs)
            obj.nInput = 0; %number of inputs
            obj.f = nan*zeros(nModes,1); %natural frequencies
            obj.z = nan*zeros(nModes,1); %damping ratios
            obj.u = nan*zeros(ndof,nModes); %mode shapes
            obj.pf = nan*zeros(nModes,1); %modal participation factors
            obj.df = nan*zeros(nModes,1); %natural frequency standard deviation (sigma)
            obj.dz = nan*zeros(nModes,1); %damping ratio standard deviation (sigma)
            obj.du = nan*zeros(ndof,nModes); %mode shape standard deviation (sigma)
            obj.ss = ss(zeros(2*nModes),[],zeros(ndof,2*nModes),[]);
            obj.index = 1:ndof; %sensor index
            
            for k =  1:nargin-2
                if k == 1
                    obj.nInput = varargin{k};
                    obj.ss.B = zeros(2*nModes,nInput);
                    obj.ss.D = zeros(nModes,nInput);
                elseif k == 2
                    obj.f = varargin{k};
                elseif k == 3
                    obj.z = varargin{k};
                elseif k == 4
                    obj.u = varargin{k};
                elseif k == 5
                    obj.pf = varargin{k};
                elseif k == 6
                    obj.df = varargin{k};
                elseif k == 7
                    obj.dz = varargin{k};
                elseif k == 8
                    obj.du = varargin{k};                
                elseif k == 9
                    obj.ss = varargin{k};
                end
            end
        end
        
        %% ------------ Getter functions ----------------------------------
       
        
        %% ------------ Setter functions -------------------------------
        %% Direct assignments
        % Direct assignments are allowed as long as the size remains
        % consistent
        function obj = set.f(obj,newVal)            
            if isequal(size(newVal),[obj.nModes,1])
                obj.f = newVal;
            else
                error(['f must be a column vector of size ' num2str([obj.nModes,1])]);
            end            
        end
        
        function obj = set.z(obj,newVal)
             if isequal(size(newVal),[obj.nModes,1])
                obj.z = newVal;
            else
                error(['z must be a column vector of size ' num2str([obj.nModes,1])]);
            end 
        end
        
        function obj = set.u(obj,newVal)
             if isequal(size(newVal),[obj.ndof,obj.nModes])
                obj.u = newVal;
            else
                error(['u must be a column vector of size ' num2str([obj.ndof,obj.nModes])]);
            end 
        end
        function obj = set.pf(obj,newVal)
             if isequal(size(newVal),[obj.nModes,1])
                obj.pf = newVal;
            else
                error(['pf must be a column vector of size ' num2str([obj.nModes,1])]);
            end 
        end
        function obj = set.df(obj,newVal)            
            if isequal(size(newVal),[obj.nModes,1])
                obj.df = newVal;
            else
                error(['df must be a column vector of size ' num2str([obj.nModes,1])]);
            end            
        end
        
        function obj = set.dz(obj,newVal)
             if isequal(size(newVal),[obj.nModes,1])
                obj.dz = newVal;
            else
                error(['dz must be a column vector of size ' num2str([obj.nModes,1])]);
            end 
        end
        
        function obj = set.du(obj,newVal)
             if isequal(size(newVal),[obj.ndof,obj.nModes])
                obj.du = newVal;
            else
                error(['du must be a column vector of size ' num2str([obj.ndof,obj.nModes])]);
            end 
        end
        
        function obj = set.ss(obj,newVal)
             if isa(newVal,'ss')
                obj.ss = newVal;
                obj.nInput = size(newVal.B,2);
            else
                error('The input must be a state space object');
            end 
        end
        
        function obj = set.index(obj,newVal)
             if length(newVal) == obj.ndof
                obj.index = newVal;
            else
                error('The input must be size ndof x 1');
            end 
        end
        
        
        %% --------------- Class Functions ------------------------------
        function [obj,i_sort] = sort(obj)
            %sorts modal parameters in ascending order by frequency value
            [f_sort,i_sort] = sort(obj.f,1,'ascend');
            obj.f = f_sort; %natural frequencies
            obj.z = obj.z(i_sort); %damping ratios
            obj.u = obj.u(:,i_sort); %mode shapes
            obj.pf = obj.pf(i_sort); %modal participation factors
            obj.df = obj.df(i_sort); %natural frequency standard deviation (sigma)
            obj.dz = obj.dz(i_sort); %damping ratio standard deviation (sigma)
            obj.du = obj.du(:,i_sort); %mode shape standard deviation (sigma)
        end
        function varargout = frf(obj,nf)
            %Returns the frequency response function with nf frequency
            % points, where f1 are the corresponding frequencies
            ssc = d2c(obj.ss);
            dt = obj.ss.Ts;
            Ac = ssc.A;
            Bc = ssc.B;
            Cc = ssc.C;
            Dc = ssc.D;
            
            % Calculate the FRF'
            h = zeros(obj.nInput,obj.ndof,nf);
            w = (0:nf-1)/(nf-1)/dt*pi;
            f1 = w/2/pi;
            for k1 = 1:nf
                h(:,:,k1) = Dc+Cc*pinv(w(k1)*1i*eye(size(Ac))-Ac)*Bc;
            end
            
            if nargout == 1
                varargout{1} = h;
            elseif nargout == 2
                varargout{1} = h;
                varargout{2} = f1;
            end
        end
    end
end
