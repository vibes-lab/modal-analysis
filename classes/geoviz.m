classdef geoviz
    % GEO defines geometry for visualizing sensor data.
    % Properties
    % nodes: [Nx3] double - specifies the 3D coordinates of each animation
    % point.
    % lines: [Mx2] double - specifies node pairs which define animation
    % lines.
    % surfs: [Px3] double - specifies node triplets which define animation
    % surfaces.
    % measNodes: [Mx1] double - A vector the nodes ID which corresponds to a
    % measurement location. All other nodes are interpolated for animation
    % purposes
    % measOrient: [Mx1] double - A vector of orientations of each
    % sensor. X = 1, Y = 2, Z = 3.
    % interpFunc: function handle which interpolates unmeasured nodes
    properties
        nodes
        lines
        surfs
        measNodes
        measOrient
        interpFunc
    end
    
    methods (Access = public)
        %% Declaration
        function obj = geoviz(N,L,S,Nm,Or)
            for k = 1:nargin
                if k == 1
                    obj.nodes = N;
                    obj.measNodes = false(N,1);
                elseif k == 2
                    obj.lines = L;
                elseif k == 3
                    obj.surfs = S;
                elseif k == 4
                    obj.measNodes = Nm;
                elseif k == 5
                    obj.measOrient = Or;
                end
            end
        end
        
        %% ====== Geometry definition functions =======
        %% Initialize using excel geometry file
        function obj = importGeoFile(obj,geoFile)
            [N0,~,~] = xlsread(geoFile,'nodes'); %graph node points
            [L,~,~] = xlsread(geoFile,'lines'); %lines connecting nodes
            [S,~,~] = xlsread(geoFile,'surfaces'); %lines connecting nodes
            N = N0(:,2:4);
            obj.nodes = N;
            obj.lines = L;
            obj.surfs = S;
        end
        
        %% Assign measurement locations (sensors) to nodes
        function obj = setMeas(obj,varargin)
            switch length(varargin)
                case 2
                    %% nodes and orientation are provided explicitly
                    measN = varargin{1};
                    measO = varargin{2};
                case 1
                    datObj = varargin{1};
                    measN = assignNodes(obj.nodes,datObj.coords);
                    measO = datObj.orient;
            end
            
            if size(measN,1) == size(measO,1) && size(measO,2) == 3
                obj.measNodes = measN;
                obj.measOrient = measO;
            else
                error('Property measNodes must be a Mx1 vector of doubles and measOrient must be Mx3.');
            end
        end
        
        
        %% ====== Visualization and animation functions =======
        %% Plot the geometry and sensor location/orientation
        function obj = plotGeo(obj,scale,trans, color, highlight)
            % Plots the geometry and sensors (as arrows showing the
            % orientation)
            %scale = length of sensor arrow
            %trans = transparency of surfaces
            %color = set color of each arrow
            % highlight = [Mx1] logical - Vector indicating which sensors to
            % highlight in plot 
            
            N = obj.nodes;
            L = obj.lines;
            S = obj.surfs;
            C = ones(size(N,1),1);
            col = lines(2);
            
            % defaults            
            switch nargin
                case 1
                    scale = 5;
                    trans = .8;
                    color = repmat(col(2,:),length(obj.measNodes),1);
                    highlight = true(numel(obj.measNodes),1);
                case 2
                    trans = 1;
                    color = repmat(col(2,:),length(obj.measNodes),1);
                    highlight = true(numel(obj.measNodes),1);
                case 3
                    color = repmat(col(2,:),length(obj.measNodes),1);
                    highlight = true(numel(obj.measNodes),1);
                otherwise
                    if isempty(color)
                        color = repmat(col(2,:),length(obj.measNodes),1);
                    end
            end
            
            sh = trisurf(S,N(:,1),N(:,2),N(:,3),C,'linestyle','none','facecolor','interp');
            alpha(sh,trans);
            hold on
            wireframe(N,L) %plot the geometry lines
            
            hold on
            xyzSens1 = N(obj.measNodes(highlight,:),:);
            uvwSens1 = scale*obj.measOrient(highlight,:);
            for k = 1:sum(highlight)
                quiver3(xyzSens1(k,1),xyzSens1(k,2),xyzSens1(k,3),...
                    uvwSens1(k,1),uvwSens1(k,2),uvwSens1(k,3),...
                    'Linewidth',5,'Color',color(k,:),'AutoScale','off');
            end
            xyzSens2 = N(obj.measNodes(~highlight,:),:);
            uvwSens2 = scale*obj.measOrient(~highlight,:);
            for k = 1:sum(~highlight)
                quiver3(xyzSens2(k,1),xyzSens2(k,2),xyzSens2(k,3),...
                    uvwSens2(k,1),uvwSens2(k,2),uvwSens2(k,3),...
                    'Linewidth',1,'Color',color(k,:),'AutoScale','off');
            end
            
            hold off
            axis equal
            axis off
        end
        
        %% Plot a mode shape
        function plotShape(obj,U,amp,varargin)
            %B is a standard complex mode shape vector (num sensors) x 1
            %amp is the scaling factor for the mode shape
            if isempty(obj.measNodes)
                error('To plot a mode you must first assign measurement locations using setMeas().');
            elseif length(obj.measNodes) ~= length(U)
                error('The length of the mode shape vector (B) must match the number of measurement locations.');
            end
            if nargin < 3
                amp = 8;
            end
            aMode = obj.buildDispMode(U);
            obj.plotDispMode(aMode,amp,varargin);
        end
        
        %% Animate a mode shape
        function Frames = animateMode(obj,U,amp,fpl,varargin)
            %B is a standard complex mode shape vector (num sensors) x 1
            %amp is the scaling factor for the mode shape
            if isempty(obj.measNodes)
                error('To plot a mode you must first assign measurement locations using setMeas().');
            elseif length(obj.measNodes) ~= length(U)
                error('The length of the mode shape vector (B) must match the number of measurement locations.');
            end
            if nargin < 3
                amp = 8;
            end
            aMode = obj.buildDispMode(U);
            Frames = obj.animateDispMode(aMode,amp,fpl,varargin);
        end
        
        %% Animate a mode shape
        function Frames = animateTimeSeries(obj,TS,amp,fpl,varargin)
            %B is a standard complex mode shape vector (num sensors) x 1
            %amp is the scaling factor for the mode shape
            TS = TS';
            [ns,nt] = size(TS);
            if isempty(obj.measNodes)
                error('To plot a mode you must first assign measurement locations using setMeas().');
            elseif length(obj.measNodes) ~= ns
                error('The length of the Time Series (TS) must match the number of measurement locations.');
            end
            if nargin < 3
                amp = 8;
            end
            
            aMode = obj.buildDispMode(TS(:,1));
            aHist = zeros(size(aMode,1),size(aMode,2),nt);
            aHist(:,:,1) = aMode;
            for k = 2:nt
                aMode = obj.buildDispMode(TS(:,k));
                
                aHist(:,:,k) = aMode;
            end
            Frames = obj.animateHist(aHist,amp,fpl,varargin);
        end
        
    end
    
    methods (Access = private)
        %% Convert standard mode to animation format (3D coordinates w/ magnitude/phase)
        function aMode = buildDispMode(obj,B)
            % B: standard mode vector (num sensors)x1
            % dir: vector of node directions (num sensors)x1, where 1 = X, 2 = Y, 3 = Z.
            % loc: vector of dim (num sensors)x3, with the x,y and z coordinates for
            % the each node
            
            N_chan = length(B);
            [N_node,~] = size(obj.nodes);
            bMode = zeros(N_node,3); % complex mode format
            
            for k = 1:N_chan
                if isnan(obj.measNodes(k))
                    %ignore unassigned nodes
                    continue
                end
                
                %get te orientation vector and normalize to mag = 1
                orient = obj.measOrient(k,:)/norm(obj.measOrient(k,:));
                
                %project the mode shape component into the cartesian axes
                %(X,Y, and Z)
                bMode(obj.measNodes(k),1) = bMode(obj.measNodes(k),1)+B(k)*orient(1);
                bMode(obj.measNodes(k),2) = bMode(obj.measNodes(k),2)+B(k)*orient(2);
                bMode(obj.measNodes(k),3) = bMode(obj.measNodes(k),3)+B(k)*orient(3);
            end
            
            if ~isempty(obj.interpFunc)
                bMode = obj.interpFunc(bMode,obj.nodes);
            end
            
            % Convert to display mode format mag/phase
            aMode = zeros(N_node,6);
            aMode(:,1) = abs(bMode(:,1));
            aMode(:,2) = angle(bMode(:,1));
            aMode(:,3) = abs(bMode(:,2));
            aMode(:,4) = angle(bMode(:,2));
            aMode(:,5) = abs(bMode(:,3));
            aMode(:,6) = angle(bMode(:,3));
        end
        
        % Plots a mode shape on geometry
        function plotDispMode(obj,aMode,amp,varargin)
            % aMode: N_node x 6 display format mode shape
            %      mode(:,1) mode mag X-axis, mode(:,2) mode phase X-axis
            %      mode(:,3:6) are for Y and Z axes
            % amp: mode amplification factor
            
            %defaults
            option = '';
            
            %optional params
            if numel(varargin) == 1
                option = char(varargin{1});
            end
            
            % azimuth and elevation of display
            az = -35;
            el = 40;
            
            obj.wireframe('k:')
            hold on
            view([az, el]);
            
            if (amp > 0)
                fak = 1;
            else
                fak = -1;
                amp = abs(amp);
            end
            P = obj.nodes;
            L = obj.lines;
            S = obj.surfs;
            
            x = aMode(:,1).*cos(aMode(:,2));
            y = aMode(:,3).*cos(aMode(:,4));
            z = aMode(:,5).*cos(aMode(:,6));
            l2 = x'*x + y'*y + z'*z;
            dP = [x, y, z]/sqrt(l2);
            dP_norm = sqrt(dP(:,1).^2 + dP(:,2).^2 + dP(:,3).^2);
            P1 = P + fak*dP*1.5.^amp;
            
            switch option
                case 'color'
                    Cmax = max(dP_norm);
                    Cmin = 0;
                    C = dP_norm; %color corresponds displament norm
                    trans = .5;
                case 'disp'
                    Cmax = max(max(dP));
                    Cmin = min(min(dP));
                    [~,I] = max(abs(dP'));
                    C = I'*0;
                    for i = 1:numel(C)
                        C(i) = dP(i,I(i));
                    end
                    trans = .5;
                case 'trans'
                    Cmax = max(dP_norm);
                    Cmin = 0;
                    C = (Cmax-Cmin)/2*ones(size(obj.nodes,1),1);
                    trans = .5;
                otherwise
                    Cmax = max(dP_norm);
                    Cmin = 0;
                    C = (Cmax-Cmin)/2*ones(size(obj.nodes,1),1);
                    trans = 1;
            end
            colormap('jet');
            sh = trisurf(S,P1(:,1),P1(:,2),P1(:,3),C,'linestyle','none','facecolor','interp');
            caxis([Cmin Cmax]);
            alpha(sh,trans);
            hold on
            wireframe(P1,L) %plot the geometry lines
            Sens1 = P1(obj.measNodes,:);% sensor locations
            scatter3(Sens1(:,1),Sens1(:,2),Sens1(:,3),80,'filled','MarkerFaceColor','k')
            hold off
            
            axis vis3d
            axis tight
            axis off
            % xlabel('x')
            % ylabel('y')
            % zlabel('z')
            figure(gcf)
        end
        
        function F = animateDispMode(obj,aMode,amp,nFrames,varargin)
            % aMode: N_node x 6 display format mode shape
            %      mode(:,1) mode mag X-axis, mode(:,2) mode phase X-axis
            %      mode(:,3:6) are for Y and Z axes
            % amp: mode amplification factor
            % fpl: frames per animation loop
            
            %defaults
            option = '';
            
            %optional params
            if numel(varargin) == 1
                option = char(varargin{1});
            end
            
            % azimuth and elevation of display
            az = -20; el = 34;
            
            %wireframe of static geometry
            obj.wireframe('k:'); hold on
            view([az, el]);
            
            if (amp > 0)
                fak = 1;
            else
                fak = -1;
                amp = abs(amp);
            end
            P = obj.nodes;
            L = obj.lines;
            S = obj.surfs;
            
            disp('adjust rotation and press any key')
            pause
            [az, el] = view;
            %disp('view stored')
            %save azel az el
            
            xlims = xlim;
            ylims = ylim;
            zlims = zlim;
            clf
            
            axis tight manual
            ax = gca;
            ax.NextPlot = 'replaceChildren';
            
            
            F(nFrames) = struct('cdata',[],'colormap',[]);
            
            %Calculate mode shape for normalization
            x = aMode(:,1);
            y = aMode(:,3);
            z = aMode(:,5);
            l2 = x'*x + y'*y + z'*z; %sqr magnitude of mode vector
            dPm = [x, y, z]/sqrt(l2); %full modal displacment
            dPm_norm = sqrt(dPm(:,1).^2 + dPm(:,2).^2 + dPm(:,3).^2); %full modal norm
            Cmax = max(dPm_norm); % maximum color intensity corresponds to max displacment
            Cmin = 0; %minimum intensity corresponds to zero displacement
            
            %Animate the mode shape
            for j = 1:nFrames
                %x = mag*cos(timestep+phase)
                x = aMode(:,1).*cos((j-1)*2*pi/nFrames+aMode(:,2));
                y = aMode(:,3).*cos((j-1)*2*pi/nFrames+aMode(:,4));
                z = aMode(:,5).*cos((j-1)*2*pi/nFrames+aMode(:,6));
                dP = [x, y, z]/sqrt(l2); %normalized instantaneous displacment
                P1 = P + fak*dP*1.5.^amp; %displaced node locations, with amplification
                
                switch option
                    case 'color'
                        C = sqrt(dP(:,1).^2 + dP(:,2).^2 + dP(:,3).^2); %color corresponds displament norm
                        trans = .5;
                    case 'trans'
                        C = (Cmax-Cmin)/2*ones(size(obj.nodes,1),1);
                        trans = .5;
                    otherwise
                        C = (Cmax-Cmin)/2*ones(size(obj.nodes,1),1);
                        trans = 1;
                end
                
                sh = trisurf(S,P1(:,1),P1(:,2),P1(:,3),C,'linestyle','none','facecolor','interp');
                caxis([Cmin Cmax]);
                alpha(sh,trans);
                hold on
                wireframe(P1,L) %plot the geometry lines
                hold off
                view(az,el)
                
                xlim(xlims+[-2 2]*amp)
                ylim(ylims+[-2 2]*amp)
                zlim(zlims+[-2 2]*amp)
                drawnow
                axis off
                set(gcf,'color','w');
                % axis vis3d
                % axis tight
                F(j) = getframe;
            end
        end
        
        function F = animateHist(obj,aHist,amp,nFrames,varargin)
            % aMode: N_node x 6 display format mode shape
            %      mode(:,1) mode mag X-axis, mode(:,2) mode phase X-axis
            %      mode(:,3:6) are for Y and Z axes
            % amp: mode amplification factor
            % fpl: frames per animation loop
            
            %defaults
            option = '';
            
            %optional params
            if numel(varargin) == 1
                option = char(varargin{1});
            end
            
            % azimuth and elevation of display
            az = -20; el = 34;
            
            %wireframe of static geometry
            obj.wireframe('k:'); hold on
            view([az, el]);
            
            P = obj.nodes;
            L = obj.lines;
            S = obj.surfs;
            
            disp('adjust rotation and press any key ')
            pause
            [az, el] = view;
            %disp('view stored')
            %save azel az el
            
            xlims = xlim;
            ylims = ylim;
            zlims = zlim;
            clf
            
            axis tight manual
            ax = gca;
            ax.NextPlot = 'replaceChildren';
            
            F(nFrames) = struct('cdata',[],'colormap',[]);
            
            %Calculate normalization factors
            x = max(aHist(:,1,:),[],3);
            y = max(aHist(:,3,:),[],3);
            z = max(aHist(:,5,:),[],3);
            dPm = max(max([x, y, z])); %max displacment
            Cmax = max(dPm); % maximum color intensity corresponds to max displacment
            Cmin = 0; %minimum intensity corresponds to zero displacement
            
            %Animate the mode shape
            for j = 1:nFrames
                %x = mag*cos(timestep+phase)
                x = aHist(:,1,j).*cos(aHist(:,2,j));
                y = aHist(:,3,j).*cos(aHist(:,4,j));
                z = aHist(:,5,j).*cos(aHist(:,6,j));
                dP = [x, y, z]; %normalized instantaneous displacment
                P1 = P + dP*amp; %displaced node locations, with amplification
                
                switch option
                    case 'color'
                        C = sqrt(dP(:,1).^2 + dP(:,2).^2 + dP(:,3).^2); %color corresponds displament norm
                        trans = .5;
                    case 'trans'
                        C = (Cmax-Cmin)/2*ones(size(obj.nodes,1),1);
                        trans = .5;
                    otherwise
                        C = (Cmax-Cmin)/2*ones(size(obj.nodes,1),1);
                        trans = 1;
                end
                
                sh = trisurf(S,P1(:,1),P1(:,2),P1(:,3),C,'linestyle','none','facecolor','interp');
                caxis([Cmin Cmax]);
                alpha(sh,trans);
                hold on
                wireframe(P1,L) %plot the geometry lines
                hold off
                view(az,el)
                
                xlim(xlims+[-dPm dPm]*amp)
                ylim(ylims+[-dPm dPm]*amp)
                zlim(zlims+[-dPm dPm]*amp)
                drawnow
                axis off
                set(gcf,'color','w');
                % axis vis3d
                % axis tight
                F(j) = getframe;
            end
        end
        
        function wireframe(obj, style)
            %Plots a series of lines specified in the point matrix P and the line
            %matrix L. The matrices are assumed to have SVS format. LInes are plotted
            %in double thickness in the specified style and color
            
            %All rights reserved. Rune Brincker, Aug 2012.
            
            if nargin < 2
                style = 'k-';
            end
            h = gca;
            P = obj.nodes;
            L = obj.lines;
            [nl, ~] = size(L);
            
            for n=1:nl
                plot3(h,[P(L(n,1), 1); P(L(n,2), 1)], [P(L(n,1), 2); P(L(n,2), 2)], [P(L(n,1), 3); P(L(n,2), 3)], style, 'LineWidth',2);
                hold on
            end
            axis equal
            hold off
        end
    end
end
