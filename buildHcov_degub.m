function [varargout] = buildHcov(y,s,Ipc,nb)
% based on Dohler et al. (2013) "Efficient multi-order uncertainty..."

% Examples:
% Without uncertainty calculation:
%   [Hcov,datadim,hankledim] = buildHcovTcov(y,s,Ipc)
% With uncertainty calculation
%   [Hcov,datadim,hankledim,Tcov] = buildHcovTcov(y,s,Ipc,nb)

[l,ny] = size(y);

q = s;
p = 2*s-q-1;
j = ny-p-q; % column dimension of full Href

%% Defaults
if nargin < 3
    Ipc = 1:ny;
    nb = 1;
elseif nargin < 4
    nb = 1;
end

r = length(Ipc); %number of references
datadim = [l,r];
hankeldim = [q,p,j];
%% Build the output matrices
%see Sec 2.3
tic
Ym = zeros(r*q,j);
Yp = zeros(l*q,j);
for k = 1:q
    Ym((q-k)*r+1:(q-k+1)*r,:) = y(Ipc,k:k+j-1);
    Yp((k-1)*l+1:k*l,:) = y(:,q+k:j+q+k-1);
end
Hcov0 = Yp*Ym.'/j; %Hankle matrix for complete set
toc

%% Find the covariance of the output covariance!
% see sec 3.2 and 5.1

Nb = floor(j/nb); %number of samples per segment
T = zeros((p+1)*q*l*r,nb);
Hvec0 = reshape(Hcov0,[],1);
Hcov = zeros((p+1)*l,q*r); %Averaged version of the Hankle matrix

ri = 6;
figure;hold on
Ryy0 = plotHcov(Hcov0,s);
plot(Ryy0(ri,:),'k','linewidth',2)
for k = 1:nb
    Yp_k = Yp(:,(k-1)*Nb+1:k*Nb);
    Ym_k = Ym(:,(k-1)*Nb+1:k*Nb);
    Hcov_k = Yp_k*Ym_k.'/Nb;
    
    Hcov = Hcov+Hcov_k/nb;
    Hcov_vec_k = reshape(Hcov_k,[],1);
    T(:, k) = (Hcov_vec_k-Hvec0)/sqrt(nb-1);
    Ryy = plotHcov(Hcov_k,s);
    plot(Ryy(ri,:))
    
end

ERyy = zeros(nb,2*s-1);
SRyy2 = zeros(1,2*s-1);
for k = 1:nb
    SH_k = reshape(T(:, k),s*l,s*l);
    SRyy_k = plotHcov(SH_k,s);
    ERyy(k,:) = SRyy_k(ri,:).^2;
    SRyy2 = SRyy2+SRyy_k(ri,:).^2;
end
SRyy = var(ERyy,[],1);
plot(Ryy0(ri,:)+3*sqrt(SRyy),'k:','linewidth',3)
plot(Ryy0(ri,:)-3*sqrt(SRyy),'k:','linewidth',3)
plot(Ryy0(ri,:)+3*sqrt(SRyy2),'r:','linewidth',3)
plot(Ryy0(ri,:)-3*sqrt(SRyy2),'r:','linewidth',3)
check = norm(Hcov-Hcov0);
disp(['Hcov sanity check: ' num2str(check)])
varargout{1} = Hcov;
varargout{2} = datadim;
varargout{3} = hankeldim;
varargout{4} = T;