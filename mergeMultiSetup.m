function mergedMode = mergeMultiSetup(modeObjArray,nDofTotal)

%MERGEMULTISETUP merges multiple modeObj instances from various partial
%tests into a single modeObj. Each instance in modeObjArray must have at
%least one index in common with the previous modeObj in the array. In 
%addition, all modObj instances must have the same number of modes.
%nDofTotal specifies the total number of degrees of freedom of the merged 
%modObj. The modes are merged by averaging frequency and damping values, 
%while rescaling and merging modeshapes at the reference locations.

nSetup = length(modeObjArray); %number of setups to merge
nModes = modeObjArray(1).nModes; %number of modes in the first instance

%% Check inputs
if nSetup < 2
    mergedMode = modeObjArray;
    warning('Length of modeObjArray must be greater that 1 for merging to occur.');
    return
end

for k = 2:nSetup
    if nModes ~= modeObjArray(k).nModes
        error('Each instance of modObjArray must contain the same number of modes.');
    end
end

%% Initialize the full mode shape using the first setup
mergedMode = modeobj(nModes,nDofTotal); %full mode object
index1 = modeObjArray(1).index; %sensor indices of the first setup
u1 = modeObjArray(1).u; %partial modeshape from the first setup
mergedMode.u(index1,:) = u1; %assign values to full mode shape (missing values are nan)
mergedMode.f = modeObjArray(1).f; %frequencies from the first setup
mergedMode.z = modeObjArray(1).z; %damping from the first setup

%% Merge additional setups
for k = 2:nSetup
    %freq and damping averages
    mergedMode.f = (mergedMode.f+modeObjArray(k).f)/2; %avg of frequency value
    mergedMode.z = (mergedMode.z+modeObjArray(k).z)/2; %avg of damping value
    
    %mode merging
    indexk = modeObjArray(k).index; %sensor indices of the kth setup
    refI = ~isnan(mergedMode.u(indexk,1)); %reference indeces in common between two setups
    uref_new = modeObjArray(k).u(refI,:); %reference mode values of new mode
    uref_merge = mergedMode.u(indexk(refI),:); %ref mode values of merged mode
    for m = 1:nModes
        uref_new_mean = mean(uref_new(:,m));
        uref_merge_mean = mean(uref_merge(:,m));
        normC = uref_merge_mean/uref_new_mean;
        u_new = nan*ones(nDofTotal,1);
        u_new(indexk) = modeObjArray(k).u(:,m)*normC;
        u_merge = mergedMode.u(:,m);
        mergedMode.u(:,m) = mean([u_merge u_new],2,'omitnan');
    end
end
