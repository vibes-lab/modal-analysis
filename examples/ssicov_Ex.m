%% Load Data
load 'AmbVibeSim_5storyBldg'
% Note standard data format (rows = samples, columns = sensors)

%% Modal Analysis opts 
%these are the recomended opts for SSI-Cov
blockRows = 40; %Number of block rows

%Options
clear opts
opts.modeRange = 5:20; %Number of modes to run
opts.projChan = 1:size(data,2); %projection channels (use all sensors for standard SSI, use a subset for reference-based SSI)

%the following are options for the spectral matrix plotted in the
%stabilization diagram. They do not affect the results of the SSI algorithm
opts.N = 1500; %Number of samples in the PSD window
opts.specLines = 4; %number of spectral lines to display

%% Run Modal Analysis
% run the SSI-Cov OMA routine
dt = 1/fs;
[modalArray,spec] = omaSSICov(data,dt,blockRows,opts);

%you can visualize the results using the stabDiagram function
f1 = figure;
ax = axes(f1);
modeRange = opts.modeRange; %specify the same mode range as OMA (can be different)
stabDiagram(ax,modalArray,modeRange,...
    'Spectrum',spec,'df',.05,'dz',0.1,'mac',0.75);          
%% Modal selection
% Since SSI returns an array of possible mode sets, it is necessary to
% reduce this to a single mode set. The modalSelection function makes this
% process interactive

modObj = modalSelection(modalArray,spec,opts);

%% ModObj
% The modObj has all modal data stored in one place, for example, access
% the natural frequencies by calling 
modObj.f

%% Use geoviz object to plot mode shapes
% See the classes_Ex.m for more details on using geoviz objects

% =========== Build Object ================
% The geometry .xlsx file describes nodes (3D points), lines connecting
% pairs of nodes, and surfaces joining node triplets.
geofile = '5storyBldg_Geometry'; %geometry file (xls)

%initialize a geoviz object and import the geometry file
bldgeo = geoviz();
bldgeo = bldgeo.importGeoFile(geofile);

% The layout file has the 3D coordinates of sensors, their orientations,
% and their names in separate worksheets
layoutfile = '5storyBldg_15_Layout';
[coords,~,~] = xlsread(layoutfile,'coordinates');
[orient,~,~] = xlsread(layoutfile,'orientation');

% Define sensor locations on the geometry
[nodeID,~] = assignNodes(bldgeo.nodes,coords);

% update the geoviz object with the sensor locations and orientations
bldgeo = bldgeo.setMeas(nodeID,orient);

% define the interpolation function
bldgeo.interpFunc = @interpNStoryBldg;

% ======== Plot =============
close all
nModes = modObj.nModes;
f_est = zeros(nModes,1);
z_est = zeros(nModes,1);
phi_est = zeros(size(data,2),nModes);
for k = 1:numel(modObj.f)
    f_k = modObj.f(k);
    z_k = modObj.z(k);
    phi_k = modObj.u(:,k);
    
    figure
    subplot(1,2,1);
    bldgeo.plotShape(phi_k,1,'color');
    subplot(1,2,2);
    plotModalPhase(phi_k,real(phi_k),'jet',gca);
    title(['Mode ' num2str(k) ': f = ' num2str(round(f_k,3))...
         ' Hz, \zeta = ' num2str(round(z_k,3))]);
    set(gca,'FontSize',14);
end