%% Load Data
load '/Users/Vast2/Documents/Repositories/dynamic-simulation/Data/WhiteNoiseSim_Bldg_5flr_XYRot'
% Note standard data format (rows = samples, columns = sensors)

%% Settings
%these are the recomended opts for SSI-Cov
blockRows = 10; %Number of block rows
nSens = size(data,2);
nb = 10;
numSeg = 32; %number of data segments to analyze
N = size(data,1)/numSeg;
comp = 4;
neighbors = [3,5];
projChan = 1:(numel(neighbors)+1);
%% Compute the Hankel matrix for the reference dataset
data0 = data(1:N,[comp neighbors]);
[Hcov0,ddim,hdim,Tcov] = buildHcov(data0',...
        blockRows,...
        projChan,...
        nb);
    
%% Find the nullspace and its covariance
m = 10;
S = nullspace_ssicov(Hcov0,m);

%Sigma = N*kron(eye(size(S,1)),S.')*(Tcov*Tcov.')*kron(eye(size(S,1)),S);
%% Residual vector test on rest of data
chi2 = zeros(numSeg,1);
faultSeg = 20;
start = tic;
for k = 1:numSeg
    datak = data(((k-1)*N+1):(k*N),:);
    if k >= faultSeg
        datak(:,5) = datak(:,5)-.5*datak(:,5);
    end
    datakc = datak(:,[comp neighbors]);
    Hcovk = buildHcov(datakc',...
        blockRows,...
        projChan,...
        nb);
    Rn = sqrt(floor(N/nb))*reshape(S.'*Hcovk,[],1); %residual vector

    A = kron(eye(size(S,1)),S.');
    Sigma2 = pinv(A*Tcov);
    alpha = Sigma2*Rn;
    chi2(k) = alpha.'*alpha;
end
fullT = toc(start);
disp(['Run Time = ' num2str(fullT)]);
%% Plot chi2
figure
plot(chi2,'linewidth',2);
xlabel('Data segment')
ylabel('Residual Index (Chi^2)')
title(['Distributed: Component ' num2str(comp)])
set(gca,'FontSize',14);
