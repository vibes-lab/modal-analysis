%% Load Data
load 'AmbVibeSim_5storyBldg'
% Note standard data format (rows = samples, columns = sensors)

%% Covert to timedat
tdat0 = timedat(data,fs);
tdat1 = tdat0.trim('SensorIndex',1:7);
tdat2 = tdat0.trim('SensorIndex',5:10);
tdat3 = tdat0.trim('SensorIndex',[7,6,11:15]);

%% Modal Analysis: data set 1 
%these are the recomended opts for SSI-Cov
blockRows = 40; %Number of block rows

%Options
clear opts
opts.modeRange = 5:20; %Number of modes to run
opts.projChan = 1:tdat1.nSensors; %projection channels (use all sensors for standard SSI, use a subset for reference-based SSI)
opts.N = 1500; %Number of samples in the PSD window
opts.specLines = 4; %number of spectral lines to display

% run the SSI-Cov OMA routine
[modalArray1,spec] = omaSSICov(tdat1,blockRows,opts);
         
% Modal selection
modObj1 = modalSelection(modalArray1,spec,opts);

%% Modal Analysis: data set 2
%these are the recomended opts for SSI-Cov
blockRows = 40; %Number of block rows

%Options
clear opts
opts.modeRange = 5:20; %Number of modes to run
opts.projChan = 1:tdat2.nSensors; %projection channels (use all sensors for standard SSI, use a subset for reference-based SSI)
opts.N = 1500; %Number of samples in the PSD window
opts.specLines = 4; %number of spectral lines to display

% run the SSI-Cov OMA routine
[modalArray2,spec] = omaSSICov(tdat2,blockRows,opts);
         
% Modal selection
modObj2 = modalSelection(modalArray2,spec,opts);

%% Modal Analysis: data set 3
%these are the recomended opts for SSI-Cov
blockRows = 40; %Number of block rows

%Options
clear opts
opts.modeRange = 5:20; %Number of modes to run
opts.projChan = 1:tdat3.nSensors; %projection channels (use all sensors for standard SSI, use a subset for reference-based SSI)
opts.N = 1500; %Number of samples in the PSD window
opts.specLines = 4; %number of spectral lines to display

% run the SSI-Cov OMA routine
[modalArray3,spec] = omaSSICov(tdat3,blockRows,opts);
         
% Modal selection
modObj3 = modalSelection(modalArray3,spec,opts);

%% Merging multiple setups
% First, create an array of modeObj corresponding to the multiple setups
modeObjArray = [modObj1 modObj2 modObj3];

% Next call the mergeMultiSetup function
nDofTotal = 15; %total number of degrees of freedom accross all setups
mergedMode = mergeMultiSetup(modeObjArray,nDofTotal);

%% Use geoviz object to plot mode shapes
% See the classes_Ex.m for more details on using geoviz objects

% =========== Build Object ================
% The geometry .xlsx file describes nodes (3D points), lines connecting
% pairs of nodes, and surfaces joining node triplets.
geofile = '5storyBldg_Geometry'; %geometry file (xls)

%initialize a geoviz object and import the geometry file
bldgeo = geoviz();
bldgeo = bldgeo.importGeoFile(geofile);

% The layout file has the 3D coordinates of sensors, their orientations,
% and their names in separate worksheets
layoutfile = '5storyBldg_15_Layout';
[coords,~,~] = xlsread(layoutfile,'coordinates');
[orient,~,~] = xlsread(layoutfile,'orientation');

% Define sensor locations on the geometry
[nodeID,~] = assignNodes(bldgeo.nodes,coords);

% update the geoviz object with the sensor locations and orientations
bldgeo = bldgeo.setMeas(nodeID,orient);

% define the interpolation function
bldgeo.interpFunc = @interpNStoryBldg;

% ======== Plot =============
%close all
nModes = mergedMode.nModes;
f_est = zeros(nModes,1);
z_est = zeros(nModes,1);
phi_est = zeros(size(data,2),nModes);
for k = 1:numel(mergedMode.f)
    f_k = mergedMode.f(k);
    z_k = mergedMode.z(k);
    phi_k = mergedMode.u(:,k);
    
    figure
    subplot(1,2,1);
    bldgeo.plotShape(phi_k,1,'color');
    subplot(1,2,2);
    plotModalPhase(phi_k,real(phi_k),'jet',gca);
    title(['Mode ' num2str(k) ': f = ' num2str(round(f_k,3))...
         ' Hz, \zeta = ' num2str(round(z_k,3))]);
    set(gca,'FontSize',14);
end