% The timedat class provides a comprehensive way to store and manipulate
% time series data from a set of sensors. The geoviz class provides a way
% to store geometric informations and visualize sensor data in 3D.

% This example how to use them effectively

%% Import a geometry file as a geoviz object
% The geometry .xlsx file describes nodes (3D points), lines connecting
% pairs of nodes, and surfaces joining node triplets.
geofile = '5storyBldg_Geometry'; %geometry file (xls)

%initialize a geoviz object and import the geometry file
bldgeo = geoviz();
bldgeo = bldgeo.importGeoFile(geofile);

% visualize the geometry, see geoviz code for input parameters
figure
bldgeo.plotGeo(.2,.6);

%% Load Time Series Data
load 'AmbVibeSim_5storyBldg'
% Note standard data format (rows = samples, columns = sensors)

%% Create a basic timedat obj from data
% Basic object requires only data and sampling rate
tdat = timedat(data,fs);

%% Add meta data
% The layout file has the 3D coordinates of sensors, their orientations,
% and their names in separate worksheets
layoutfile = '5storyBldg_15_Layout';
[coords,~,~] = xlsread(layoutfile,'coordinates');
[orient,~,~] = xlsread(layoutfile,'orientation');
[~,names,~] = xlsread(layoutfile,'sensorName');

%Add some metadata to the object, note that some metadata, like nSensors,
%is automatically generated.
tdat.startTime = datetime; %The start time of the aquisition
tdat.units = 'g'; %The units of measurement
tdat.coords = coords; %Sensor coordinates
tdat.orient = orient; %Sensor orientation in 3D
tdat.refs = false(tdat.nSensors,1); %Reference sensors (for matching to other timdat files)
tdat.serial = 1:tdat.nSensors; %serial number
tdat.info = cell(tdat.nSensors,1); %Additional info/notes specific to sensors
tdat.sensorName = names; %Display name for sensor
tdat.sensorID = names; %Unique sensor string ID 

%% Visualize the data
figure
tdat.plotTimeFreq()

%% Decimate
% Decimate by 3 since data is over sampled
tdat = tdat.decimate(3);
figure
tdat.plotTimeFreq()

%% Define sensor locations on the geometry
% Each sensor must be on a geometry node. 
% assignNodes assigns each sensor to the closest node in the geometry. 
% nodeID is the node number corresponding to each sensor. This works even
% if your geometry doesn't match your sensors positions exactly.
[nodeID,~] = assignNodes(bldgeo.nodes,tdat.coords);

% update the geoviz object with the sensor locations and orientations
bldgeo = bldgeo.setMeas(nodeID,tdat.orient);

% visualize the geometry again, but this time with sensors!
figure
bldgeo.plotGeo(.2,.6);
title('Sensor Locations')

% highlight certain sensors
highlight = false(tdat.nSensors,1);
highlight([1,5,10,13]) = true; %highlight sensors 1, 5, 10, 13.
figure
bldgeo.plotGeo(.2,.6,[],highlight);
title('Sensor Locations (with highlights)')

%% Visualize static displacements
%Can also be used to vizualize mode shapes

nSensors = size(coords,1); %number of sensors
modeShape = randn(nSensors,1)-0.5; %generate a random displacement shape

figure
bldgeo.plotShape(modeShape,1,'color');
title('Random Mode Shape')

% Note that nodes without sensors are zero.
% If you have more nodes than sensors, you can define a custom
% interpolation fuction interpFunc

bldgeo.interpFunc = @interpNStoryBldg;

%which interpolates a partial shape into a full one. Open the function to
%see how it is implemented. You will need to define a custom function
%depending on your structure and node geometry definition (in the geometry
%file).

%% Visualize dynamic displacements
%Visualize the first 100 samples of the time series, amplified by 5
figure
F = bldgeo.animateTimeSeries(tdat.data,5,100,'color');

f1 = figure;
movie(f1,F)

%% Animate mode shapes
%The animateMode fuction lets you to animate mode shapes. It accepts
%complex values which describe both magnitude and phase offset
nSensors = size(coords,1); %number of sensors
modeShape = randn(nSensors,1)-0.5; %generate a random displacement shape


figure
F = bldgeo.animateMode(modeShape,1,10,'color');

f1 = figure;
movie(f1,F,10)


