%% Load Data
load 'AmbVibeSim_5storyBldg'
% Note standard data format (rows = samples, columns = sensors)

%% Modal Analysis opts 
%these are the recomended opts for SSI-Cov
blockRows = 40; %Number of block rows

%Options
clear opts
opts.modeRange = 5:25; %Number of modes to run
opts.projChan = 1:size(data,2); %projection channels (use all sensors for standard SSI, use a subset for reference-based SSI)

%the following are options for the spectral matrix plotted in the
%stabilization diagram. They do not affect the results of the SSI algorithm
opts.N = 1500; %Number of samples in the PSD window
opts.specLines = 4; %number of spectral lines to display

%% Run Modal Analysis
% run the SSI-Cov OMA routine
dt = 1/fs;
[modalArray,spec] = omaSSICov(data,dt,blockRows,opts);

%you can visualize the results using the stabDiagram function
f1 = figure;
ax = axes(f1);
modeRange = opts.modeRange; %specify the same mode range as OMA (can be different)
stabDiagram(ax,modalArray,modeRange,...
    'Spectrum',spec,'df',.05,'dz',0.1,'mac',0.75);          
%% Automated modal selection (optional)
% You can also use an clustering algorithm to automate the process by
% specifying

opts.selectMethod = 'auto';
[modObj,clusterStat] = modalSelection(modalArray,spec,opts);

%The top right plot shows the identified modes, while the other plots show
%the three clustering stages

% NOTE: The clustering algorithm performance is highly dependent on your
% choice of opts.modeRange. In this simulated data, we expected 15 modes, 
% so we ran up to twice that number. That gives enough modes for the
% difference between structural modes and noise modes to be clear. 
% Notice how in the stage 3 plot there is a clear drop in the number of
% elements (modes) per cluster after 15. This is an indication of good
% clustering performance.

% Try re-runing the code using opts.modeRange = 5:20 and notice that the
% higher order modes are not selected, because they are not identified
% enough times.

%% Cluster statistics
% The modObj now has all the idefified structural modal parameters. In
% addition the modalSelection function provides and clusterStat structure which
% has statistical information about the clusters. Cluster standard
% deviations can be used as uncertainty bounds as well.

%show obtained clusters and 3 sigma bounds on modal parameters using cluster st. dev.
figure
n_clust = numel(clusterStat.clustSize);
col = jet(n_clust);
for i = 1:n_clust
   errorbar(modObj.f,modObj.z*100,...
    3*clusterStat.z_std*100,3*clusterStat.z_std*100,...
    3*clusterStat.f_std,3*clusterStat.f_std,'o','MarkerEdgeColor','k',...
    'MarkerSize',10); 

   % errorbar(clusterStat.clust_f{i},clusterStat.clust_z{i}*100,ydelt,ydelt,xdelt,xdelt,...
        %'o','MarkerFaceColor',col(i,:),'MarkerEdgeColor','k',...
        %'Color',col(i,:),'MarkerSize',5);
     
    hold on
    ci = mod(i*floor(n_clust/2),n_clust)+1; %increment color index by large numbers so adjacent colors are not similar
    plot(clusterStat.clust_f{i},clusterStat.clust_z{i}*100,'o',...
        'MarkerFaceColor',col(ci,:),'MarkerEdgeColor','k','MarkerSize',5);    
    
end
xlabel('Frequency [Hz]');
ylabel('Damping [%]');
title('Final Clusters')
grid on


