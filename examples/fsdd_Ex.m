%% Load Data
load 'AmbVibeSim_5storyBldg'
% Note standard data format (rows = samples, columns = sensors)

%% Modal Analysis opts 
%Options
clear opts
opts.selectMethod = 'manual'; %method to select spectral peaks
opts.projChan = 1:size(data,2); %projection channels (use all sensors for standard SSI, use a subset for reference-based SSI)
opts.N = 1500; %Number of samples in the PSD window
opts.specLines = 4; %number of spectral lines to display
%Unlike SSI, the last two options do affect the results of FSDD

%% Run Modal Analysis
% run the FSDD OMA routine
dt = 1/fs;
[modObj,spec,info] = omaFSDD(data,dt,opts);

%% ModObj
% The modObj has all modal data stored in one place, for example, access
% the natural frequencies by calling 
modObj.f

%% Array option
% Note that the info structure stores your selected frequencies in the F
% field
info.F

% You can use this to run the analysis again without manually selecting the
% spectral peaks
opts.selectMethod = 'array';
opts.F = info.F;
[modObj,spec,info] = omaFSDD(data,dt,opts);

%% Use geoviz object to plot mode shapes
% See the classes_Ex.m for more details on using geoviz objects

% =========== Build Object ================
% The geometry .xlsx file describes nodes (3D points), lines connecting
% pairs of nodes, and surfaces joining node triplets.
geofile = '5storyBldg_Geometry'; %geometry file (xls)

%initialize a geoviz object and import the geometry file
bldgeo = geoviz();
bldgeo = bldgeo.importGeoFile(geofile);

% The layout file has the 3D coordinates of sensors, their orientations,
% and their names in separate worksheets
layoutfile = '5storyBldg_15_Layout';
[coords,~,~] = xlsread(layoutfile,'coordinates');
[orient,~,~] = xlsread(layoutfile,'orientation');

% Define sensor locations on the geometry
[nodeID,~] = assignNodes(bldgeo.nodes,coords);

% update the geoviz object with the sensor locations and orientations
bldgeo = bldgeo.setMeas(nodeID,orient);

% define the interpolation function
bldgeo.interpFunc = @interpNStoryBldg;

% ======== Plot =============
close all
nModes = modObj.nModes;
f_est = zeros(nModes,1);
z_est = zeros(nModes,1);
phi_est = zeros(size(data,2),nModes);
for k = 1:numel(modObj.f)
    f_k = modObj.f(k);
    z_k = modObj.z(k);
    phi_k = modObj.u(:,k);
    
    figure
    subplot(1,2,1);
    bldgeo.plotShape(phi_k,1,'color');
    subplot(1,2,2);
    plotModalPhase(phi_k,real(phi_k),'jet',gca);
    title(['Mode ' num2str(k) ': f = ' num2str(round(f_k,3))...
         ' Hz, \zeta = ' num2str(round(z_k,3))]);
    set(gca,'FontSize',14);
end