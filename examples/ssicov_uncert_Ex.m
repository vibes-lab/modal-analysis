% This example shows how to perform SSI Cov and extract modal parameter
% uncertainties

%% Load Data
load 'AmbVibeSim_5storyBldg'
% Note standard data format (rows = samples, columns = sensors)

%% Modal Analysis opts 
%these are the recomended settings for SSI-Cov
%required inputs
blockRows = 40; %Number of block rows

%Options
clear opts
opts.UNCALC = true; %<----- Add this option to calculate uncertainties
%Be aware that this adds significant computational time to the process
opts.nb = 10; %<----- Modify this value to change no. of averages in the uncert. analysis (10 is the default)
opts.modeRange = 5:20; %Number of modes to run
opts.projChan = 1:size(data,2); %projection channels (use all sensors for standard SSI, use a subset for reference-based SSI)

%the following are options for the spectral matrix plotted in the
%stabilization diagram. They do not affect the results of the SSI algorithm
opts.N = 1500; %Number of samples in the PSD window
opts.specLines = 4; %number of spectral lines to display

%% Run Modal Analysis
% run the SSI-Cov OMA routine
dt = 1/fs;
[modalArray,spec] = omaSSICov(data,dt,blockRows,opts);

%% ModObj
% Notice that the modeObj objects now have uncertainty information populated
modObj1 = modalArray(1);
freq = modObj1.f; %identified frequencies
freq_std = modObj1.df; %frequency standard deviation
damping = modObj1.z; %identified damping 
damping_std = modObj1.dz; %damping standard deviation
table(freq, freq_std,damping,damping_std)

%% Stabilization Diagram
% You can call the stabDiagram function with the 'Errorbar' option to vizualize
% the frequency uncertainties. We specify a value of two to plot the 3
% sigma bounds on the estimates
f1 = figure;
ax = axes(f1);
modeRange = opts.modeRange; %specify the same mode range as OMA (can be different)
stabDiagram(ax,modalArray,modeRange,...
    'Spectrum',spec,'df',.05,'dz',0.1,'mac',0.75, 'Errorbar',3);
title('Error bars - 3\sigma')

% Notice that some ustable points have huge uncertainties, while most
% stable points have very low. You can suppress plotting unstable modes
% altogether by using the 'StabOnly' option
f2 = figure;
ax = axes(f2);
stabDiagram(ax,modalArray,modeRange,...
    'Spectrum',spec,'df',.05,'dz',0.1,'mac',0.75, 'Errorbar',3,'StabOnly');
title('Stable Modes Only')

%% Modal selection
% Since SSI returns an array of possible mode sets, it is necessary to
% reduce this to a single mode set. The modalSelection function makes this
% process interactive

modObj = modalSelection(modalArray,spec,opts);


%% Visualize errors
%Frequency and damping errors
figure
errorbar(modObj.f,modObj.z*100,...
    3*modObj.dz*100,3*modObj.dz*100,3*modObj.df,3*modObj.df,'o');
xlabel('Frequency (Hz)')
ylabel('Damping Ratio (%)');

%mode 1 error bounds in 1D
figure
hold on
plot(real(modObj.u(:,1)),'linewidth',2);
plot(real(modObj.u(:,1))+3*modObj.du(:,1),'k:');
plot(real(modObj.u(:,1))-3*modObj.du(:,1),'k:');
xlabel('DOF')
ylabel('Modal Amplitude');
legend('Modal Aplitude','3\sigma error bound')
