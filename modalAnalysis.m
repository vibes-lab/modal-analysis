function [modObj,spec] = modalAnalysis(u,y,dt,method,opts)
%MODALANALYSIS  Estimate modal parameters for a system with measured inputs
%   u and outputs y. Various standard modal identification methods are
%   available which must be specified by the user. Both Experimental Modal
%   Analysis (input-output) or Operational Modal Analysis (output-only)
%   frameworks are supported. 

%   NOTE: This intended to be a "user-friendly" function for
%   quick implementation; for advanced features use the method specific
%   modal analysis fuctions (e.g. omaFDD, omaSSICov, etc.).

%   MODOBJ = MODALANALYSIS(U,Y,DT,METHOD) returns a modal object with
%   the modal parameters for a system of inputs U and response Y. If U and
%   Y are the same size, EMA is performed. If U is an empty vector, OMA is
%   performed. DT is the sampling period. METHOD specifies the algorithm
%   used for modal ID, and defaults options are used.
%   The rows of U and Y correspond to time samples and the columns to
%   measurement channels.

%   MODOBJ = MODALANALYSIS(...,OPTS) allows the used to modify the modal
%   anlysis method settings by specifying specific parameters.

%INPUT PARAMETERS

%      METHOD is a string with the following options      
%     'fsdd'        Frequency Spacial Domain Decomposition, OMA only
%     'ssicov'      Covariance-based stochastic subspace identification,
%                   EMA or OMA compatible.
%     'ssidat'      Data-drive stochastic subspace identification,
%                   OMA only.

%   OPTS is a matlab structure with the following fields

%   For 'ssicov'
%   blockRows = [double, 40 (default)] number of block rows in Hankel matrix
%   modeRange = [vector,5:20 (default)] number of mode orders to calculate for
%       stabilization diagram (No. modes = No. singular values/2)
%   projChan = [vector] indeces of the channels (columns) to be
%       used as projection channels.
%   N = [double] (optional) no. of time samples used to estimate
%       autocorrelations. Used to calculate the spectral density matrix, which
%       is used in the stabilization diagram display, but does not impact the
%       SSI algorithm. Also used to calculate the correlation function matrix,
%       which is used to estimate participation factors (when specified in OPT)
%   nb = [double, 10 (default)] number of Hankel samples for uncertainty
%       estimation (when specified in OPT)
%   UNCALC = [0 (default),1] Compute modal paramater uncertainties.
%   PFCALC = [0 (default),1] Compute modal participation factors.
%   reduction = [string, 'none' (default)] Iterate models orders by using reduced
%                   order modeling, speeding up computational time. 
%                   Chose between 'none' or 'svd.' The
%                   UNCALC option is ignored if reduction is enabled.
%   geo = [geoviz] object specifying sensor geometry

%Rodrigo Sarlo, Nov 2020

%% Run modal analysis algorithms
%check if input is supplied
if isempty(u)
    %no input -> OMA
    switch method %which algorithm to use
        case 'fsdd'
            [modObj,spec] = omaFSDD(y,dt,opts,'geo',geo);
        case 'ssicov'
            % Run main algorithm
            [modalArray,spec] = omaSSICov(y,dt,opts.blockRows,opts);
            % modal selection
            
            modObj = modalSelection(modalArray, spec,opts);
    end
    
    
else
    %input and output are the same size?
    if ~(size(u,1) == size(y,1) && size(u,2) == size(y,2))
        error('U and Y must be the same size');
    end
    %input -> EMA
    
end

