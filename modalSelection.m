function [modObj, clusterStat] = modalSelection(modalArray,spec,opts)
%MODALSELECTION  Select modes from an array of various parametric models of
%   the same system. This can be done manually from a stabilization diagram
%   interface or through an automated clustering method. There is also the
%   option of selecting from multiple setup results and merging the
%   modal parameters, assuming reference sensors have been defined
%   appropriately.

%   The following opts fields are possible. Note that including some
%   of these options may result in significantly longer computation times.

%     'selectMethod'['manual' (default),'auto']
%                   Method by which to choose
%                   the final modal parameters. Manual involves selecting
%                   from a stabilization diagram. Auto uses a clustering
%                   algorithm to select the most likely modal parameters.

%     'geo'         [geoobj] Geometry object with sensor location,
%                   orientation and a path to a geometry file

%% Handle input arguments
%defaults
AUTO = false;
geo = [];
modeRange = 5:20;
nSetup = 1; %number of sensor setups

%update defaults if specified
if nargin > 2
    if isfield(opts,'selectMethod')
        switch opts.selectMethod
            case 'manual'
                AUTO = false;
            case 'auto'
                AUTO = true;
        end
    end
    if isfield(opts,'geo')
        geo = opts.geo;
    end
    if isfield(opts,'modeRange')
        modeRange = opts.modeRange;
    end
end

%% Select modes
if AUTO
    %run auto Modal Analysis algorithm
    [modObj,clusterStat,~] = modeCluster(modalArray,opts,1,'plot');
else  
    % Pick manually from stabilization diagram
    %% plot the stabilization diagram
    f1 = figure;
    ax = axes(f1);
    [~,~] = stabDiagram(ax,modalArray,modeRange,...
        'Spectrum',spec,'df',.05,'dz',0.1,'mac',0.75);

    %% ask the user to pick the modes
    exit = false;
    f = [];z = [];u = [];pf = [];df = [];dz = [];du = [];OI = [];
    disp(['Select the point whose modal information you want to store. Hit enter'...
            ' to add mode. Hit Esc to finish.']);
    while ~exit
        dcm_obj = datacursormode(f1);
        set(dcm_obj,'DisplayStyle','datatip',...
            'SnapToDataVertex','on','Enable','on')

        % Wait while the user does this.
        b = 0;
        figure(f1)
        while b == 0
            b = waitforbuttonpress;
        end
        value = double(get(f1,'CurrentCharacter'));
        exit = value == 27;

        if ~exit
            c_info = getCursorInfo(dcm_obj);
            % Make selected line wider
            c_pos = c_info.Position;

            datacursormode off
            order = c_pos(2);
            freq = c_pos(1);

            %% Find the appropriate indeces for the freq and model order
            modelOrder = modeRange;
            o = find(modelOrder == order);
            [~,md] = min(abs(freq-modalArray(o).f));
            %mpc = phaseCollinearity(modalArray(o).u(:,md))

            %% Plot modeshapes if geometry is available
            if ~isempty(geo)
                if exist('h2','var')
                    figure(h2);
                else
                    h2 = figure('position',[0,0,500,500]);
                end
                geo.plotMode(modalArray(o).u(:,md),8,'trans')
                %% Animate
                %                 if exist('h3','var')
                %                     figure(h3);
                %                 else
                %                     h3 = figure;
                %                 end
                %                 title([num2str(modalArray(o).f(md)) ' Hz']);
                %                 M = animateGoodwinColor_Modal...
                %                     (U,modalArray(o).f(md),-7,P,L,S,1,30);
                %                 fps = 16;
                %                 movie(M,4,fps)

            end

            % Add data to list of modal properties
            f = [f;freq];
            z = [z;modalArray(o).z(md)];
            u = [u,modalArray(o).u(:,md)];
            du = [du,modalArray(o).du(:,md)];
            pf = [pf;modalArray(o).pf(md)];
            df = [df;modalArray(o).df(md)];
            dz = [dz;modalArray(o).dz(md)];
            OI = [OI;[o,md]];
            disp(['Added mode at ' num2str(freq)...
                ' Hz, Mode Order ' num2str(order)]);
        end
    end
    disp('Manual selection complete.')

    if ~isempty(f)
        % if modes were selected, store the modal information
        modObj = modeobj(numel(f),size(u,1));
        modObj.f = f;
        modObj.z = z;
        modObj.u = u;
        modObj.pf = pf;
        modObj.df = df;
        modObj.dz = dz;
        modObj.du = du;
        modObj.index = modalArray(1).index;
        [modObj,i_sort] = modObj.sort();
        clusterStat.ordIndx = OI(i_sort);
    else
        modObj = [];
        clusterStat = [];
    end
       
end
end



