function S = nullspace_ssicov(Hcov,m)
%[A,C,aux] = ssicov(Hcov,m,datadim,hankeldim,varargin)
%Identitification using covariance-based reference stochastic subspace 
%identification (SSI/ref) according to the reference channel techinque by 
%Reynders et al., "Uncertainty bounds on modal parameters obtained from
%SSI."

%INPUTS
%Hcov:  Covariance Hankel matrix (from buildHcov)
%m:     Number of modes to be returned in the identication (singular
%       values restricted to n, where n=2m)

%OUTPUTS
%S:     Nullspace of Hcov

%Rodrigo Sarlo, 2021

%% Calculate SVD and truncate at nth order
[U,~,~] = svd(Hcov);
n = 2*m; %desired system order based on number of modes

S = U(:,(n+1):end);

    