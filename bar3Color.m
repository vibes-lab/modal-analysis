function bh = bar3Color(X,ticklabelx,ticklabely)
figure;
bh = bar3(X);
for i = 1:length(bh)
    set(bh(i),'cdata',get(bh(i),'zdata'));
end
colormap(jet(100));
colorbar;

if nargin > 1
    ax = gca;
    set(ax,'XTickLabel',ticklabelx,'XTick',1:numel(ticklabelx));
    set(ax,'YTickLabel',ticklabely,'YTick',1:numel(ticklabely));
    ax.XTickLabelRotation=90;
    ylim([0.5 numel(ticklabely)+.5]);
end
