# VIBEs Lab Modal Toolkit

Modal Toolkit is a MATLAB software library for modal analysis, specially designed for long-term ambient vibration monitoring and large scale data processing. It is built from published state-of-the-art algorithms. Its current capabilities include:

**Operational Modal Analysis**<br>
Frequency Spatial Domain Decomposition (FSDD) [[ref](https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.482.9063&rep=rep1&type=pdf)]<br>
Covariance Based Stochastic Subspace Identification (SSI-COV) [[ref1](https://doi.org/10.1006/mssp.1999.1249)], [[ref2](https://doi.org/10.1007/978-1-4613-0465-4)]<br>
Uncertainty Quantification for SSI-COV [[ref](https://doi.org/10.1016/j.ymssp.2013.01.012)]

**Experimental Modal Analysis**<br>
Deterimistic Subspace Identification (DSI) [[ref](http://dx.doi.org/10.1016/j.jsv.2013.08.025)] --> COMING SOON<br>
Uncertainty Quantification for DSI --> COMING SOON<br>

**Advanced Plotting**<br>
Mode shape plotting and animation<br>
Time series animation<br>
Stabilization Diagrams<br>
Phase diagrams<br>

**Custom Classes for Data Handling**<br>
Vibedat Class: A container class for time series measurements and associated sensor metadata, with built in plotting and data manipulation functions (filters, decimation). <br>
Modeobj Class: A container class for modal data, with built in functions for data manipulation. <br>
Geoviz Class: A class for sensor geometry visualization. Easily import sensor layouts and vizualize them, or plot and animate mode shapes.

**Modal Selection and Tracking** <br>

Interactive stabilization diagrams for manual mode selection (SSI-COV) <br>
Automated stabilization diagram interpretation via clustering (SSI-COV) [[ref1](https://doi.org/10.1016/j.ymssp.2012.01.007)] [[ref2](https://link.springer.com/article/10.1007/s11831-012-9069-x)]<br>
Interactive Spectral Peak selection (FSDD)<br>
Sensor Information Ranking --> COMING SOON<br>
Mode tracking for long term modal histories --> COMING SOON

## Getting Started 

The best way to get started is with examples. In order to run them (as written) you will need to access or download a [sample data set](https://code.vt.edu/vibes-lab/modal-analysis/-/tree/master/example-data).

[Example 1](https://code.vt.edu/sarlo/modal-analysis/-/blob/master/examples/classes_Ex.m): Using classes<br>
Modal Toolkit includes two classes ([timedat](https://code.vt.edu/sarlo/modal-analysis/-/blob/master/classes/timedat.m) and [geoviz](https://code.vt.edu/sarlo/modal-analysis/-/blob/master/classes/geoviz.m)) which are extremely useful for storing, manipulating, and vizualizing time domain data from a set of sensors in 3D space. This example demonstrates their functionality.

[Example 2](https://code.vt.edu/sarlo/modal-analysis/-/blob/master/examples/ssicov_Ex.m): Perfoming OMA (SSI Cov)<br>
Covariance Based Stochastic Subspace Identification is a very popular OMA technique and it is the workhorse of the Modal Toolkit. This example shows how to implement it and visualize the results. It also introduces you to the third custom class in Modal Toolkit, [modeObj](https://code.vt.edu/sarlo/modal-analysis/-/blob/master/classes/modeobj.m), which is used for storing modal data in compact way.

[Example 3](https://code.vt.edu/vibes-lab/modal-analysis/-/blob/master/examples/ssicov_uncert_Ex.m): Uncertainty Quantification (SSI Cov)<br>
SSI COV comes with an uncertainty quantification option at the price of increased computation time. This example shows how to implement it.

[Example 4](https://code.vt.edu/vibes-lab/modal-analysis/-/blob/master/examples/ssicov_auto_Ex.m): Automated Stabilization Diagram Interpretation (SSI Cov)<br>
SSI stabilization diagrams aren't trivial to interpret, but effective automation is essential for long term monitioning. [Clustering](https://code.vt.edu/vibes-lab/modal-analysis/-/blob/master/auto-id/modeCluster.m) is popular way to automate the interpretation process. This example shows how to implement a clustering-based selection algorithm.

## Classes
This is a list of custom library classes. Refer to the help documentation for each function to understand the syntax (MATLAB help command).

[geoviz](https://code.vt.edu/sarlo/modal-analysis/-/blob/master/classes/geoviz.m)<br>
[timedat](https://code.vt.edu/sarlo/modal-analysis/-/blob/master/classes/timedat.m)<br>
[modeObj](https://code.vt.edu/sarlo/modal-analysis/-/blob/master/classes/modeobj.m)<br>

## Functions
This is a list of top-level functions. Refer to the help documentation for each function to understand the syntax (MATLAB help command).

**Operational Modal Analysis**<br>
[omaSSICov](https://code.vt.edu/vibes-lab/modal-analysis/-/blob/master/omaSSICov.m)<br>
[omaFSDD](https://code.vt.edu/vibes-lab/modal-analysis/-/blob/master/omaFSDD.m)<br>

**Modal Criteria**<br>
[calculateMAC](https://code.vt.edu/vibes-lab/modal-analysis/-/blob/master/calculateMAC.m)<br>
[phaseCollinearity](https://code.vt.edu/vibes-lab/modal-analysis/-/blob/master/phaseCollinearity.m)<br>
[mtn](https://code.vt.edu/vibes-lab/modal-analysis/-/blob/master/auto-id/mtn.m) (Modal Transfer Norm)<br>

**Plotting**<br>
[stabDiagram](https://code.vt.edu/vibes-lab/modal-analysis/-/blob/master/stabDiagram.m)<br>
[plotCrossMAC](https://code.vt.edu/vibes-lab/modal-analysis/-/blob/master/plotCrossMAC.m)<br>
[plotModalPhase](https://code.vt.edu/vibes-lab/modal-analysis/-/blob/master/plotModalPhase.m)<br>
[pltspec](https://code.vt.edu/vibes-lab/modal-analysis/-/blob/master/pltspec.m)<br>

**Advanced**<br>
[modalSelection](https://code.vt.edu/vibes-lab/modal-analysis/-/blob/master/modalSelection.m)<br>
[modeCluster](https://code.vt.edu/vibes-lab/modal-analysis/-/blob/master/auto-id/modeCluster.m)<br>
[findHarmonics](https://code.vt.edu/vibes-lab/modal-analysis/-/blob/master/findHarmonics.m)<br>

## Input Data Formats
The input formats for Modal Toolkit will depend on the function being called and can be found in the documentation for each function. In addition, the toolkit classes help keep everything in a standard format. There are, however, a few conventions which are useful to keep in mind when storing data in external format. These will make it easier to import the data into the Modal Toolkit classes. The best way to learn this is seeing the examples above.

### Time Series Data
Time series data should be stored in 2D matrix format (NxM), were N is the number of time samples and M is the number of sensors/channels ( [example](https://data.lib.vt.edu/files/tx31qh898)). Sensor coordinates and orientations are defined as Nx3 matrices, as shown in this [example sensor layout](https://code.vt.edu/sarlo/modal-analysis/-/blob/master/geometry/5storyBldg_15_Layout.xlsx). Storing data in this way during a test makes it easy to create a timedat object later.

```matlab
   layoutfile = '5storyBldg_15_Layout';
    [coords,~,~] = xlsread(layoutfile,'coordinates');
    [orient,~,~] = xlsread(layoutfile,'orientation');
    [~,names,~] = xlsread(layoutfile,'sensorName');

    
    test1 = timedat(data,fs,'Units','g','Coords',coords,'Orient',orient,'SensorName',names);
```

### Geometry and Visualization
Geometries are purely for visualization. They are expressed in terms of nodes (3D coordinates with a numeric ID), lines (pairs of nodes), and surfaces (node triples). When defining a geometry file, follow this [example](https://code.vt.edu/sarlo/modal-analysis/-/blob/master/geometry/5storyBldg_Geometry.xlsx).

There are three worksheets of tabular data.
nodes: [Nx4 double], where N is the number of nodes, the first column represents the numeric ID of the node, and the remaining columns represent X, Y, and Z coordinates, respectively. <br>
lines: [Mx2 double], specifies M node pairs which define connecting lines. <br>
surfaces: [Px3 double] - specifies P node triplets which define surfaces. <br>

This format can be imported directly as a geoviz object:
```matlab
    geofile = '5storyBldg_Geometry.xlsx'; %geometry file (xls)
    bldgeo = geoviz(); %initalize object
    bldgeo = bldgeo.importGeoFile(geofile); %import the file
```

See [Example 1](https://code.vt.edu/sarlo/modal-analysis/-/blob/master/examples/classes_Ex.m) for how to use the plotting/animation functions built in to geoviz.

## Pre-processing and Data Manipulation

The timedat class enables convenient data preprocessing and manipulation in one line. In addition, these provide error checks to make sure you don't do anything funky with your data!

```matlab
data1 = timedat(data,fs,'Units','g','Coords',coords,'Orient',orient,'SensorName',names);
data1_trim = data1.trim('Key',Value)
    % Returns timedat object which has been trimmed based on the
    % specified key/value pairs:
    % 'Time', timeRange : Relative time in seconds, timeRange is a
    % [1 x 2] double with the start and end times.
    % 'DateTime', timeRange : timeRange is a [1 x 2] datetime
    % with the start and end dates/times.
    % 'SensorName', names : names is a [N x 1] cell
    % with list of N sensorNames to remove.
    % 'SensorID', names : names is a [N x 1] cell
    % with list of N sensorIDs to remove.
    % 'SensorIndex', names : names is a [N x 1] double
    % with list of N indices, corresponding to sensors to remove.

data1_ext = data1.appendSamples(data2);
    % Adds time series samples to the end of the file. data1 and data2 column
    % dimensions must match

data1_add = data1.addSensors(data2,sensorID,sensorName,coords,orient,serial,info)
    % Add new sensors and data to object
    % data1 is M x N double
    % data2 is M x P double
    % sensorID is P x 1 cell
    % sensorName is P x 1 cell
    % sensorID is P x 1 cell
    % coords is P x 3 double
    % orient is P x 3 double
    % serial is P x 1 double
    % info is P x Q cell
    
data1_filt = data1.filter(wc,type)
    % filter the time signal using a 4th order butterworth IIR,
    % forwards-backward (zero phase) filter
    % wc is cutoff frequency(ies) in Hz
    % type is the type of filter
    % Lowpass filter: type = 'low', wc = double
    % Highpass filter: type = 'high', wc = double
    % Bandpass filter: type = 'bandpass', wc = [1 x 2] double

data1_dec = data1.decimate(r)
    %decimate data (see MATLAB decimate documentation)
    % r is [1x1] integer representing decimation factor
```

To quickly check the results of the manipulation or pre-processing, you can visualize the time/frequency information using:

```matlab
figure
data1.plotTimeFreq(I,wdwsec);  
    % I [1 x N] vector of sensor indeces to plot, use [] for all
    % PSD window size in seconds, the larger the window size the smoother the PSD plots
xlim([1 10])
```

## Contact
We are actively developing new capabilities and functionality, so check back frequently for updates. Modal Toolkit is an open source library and we welcome contributions to its capabilities. Anyone can submit a merge request, which will be tested and verified before being added to the library. 

Please email Rodrigo Sarlo (sarlo@vt.edu) with any questions, suggestions, or troubleshooting requests.

**MIT License**

Copyright (c) [2020] [Rodrigo Sarlo]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
