function Ryy = plotHcov(Hcov,s)

l = size(Hcov,1)/s;
I = (0:(s-1))*l;
%figure; hold on
Ryy = zeros(l,2*s-1);
for i = 1:l
    Ri1 = Hcov(i,I+i);
    Ri2 = Hcov(i+I(s),I+i);
    Ri = [Ri1 Ri2(2:end)];
    Ryy(i,:) = Ri;
    %plot(Ri)
end