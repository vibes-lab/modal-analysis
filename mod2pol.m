% lam = mod2pol(fz)
%Calculates the continous time poles corresponding to the modal parameters
%in fz. The array fz is 2xN, where N is the number of modes and the first 
%row contains the undamped natural frequencies, the second row contains the
% damping ratios. Lam is a 1xN array containing the poles.

% All rights reserved, Rune Brincker, Oct 2010, May 2012.

function lam = mod2pol(fz)
[Nr, Nc] = size(fz);
lam = zeros(1,Nc);
for n=1:Nc,
    om = 2*pi*fz(1, n);
    z = fz(2, n);
    lam(n) = om*(-z + i*sqrt(1 - z^2));
end