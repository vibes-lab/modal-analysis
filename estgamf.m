% Estimates modal participation vectors in the frequency domain according
% to section 10.1.1 and returns the SD matrix estimate based on these.
% Inputs
% Ghy:  Empirical SD matrix
% B:    Modeshape/eigenvector matrix (column vector)
% D:    Eigenvalue diagonal matrix
% K:    2x1 matrix with frequency limits (indexes)
% dt:   Sampling time of the original signal
% Outputs
% B1:   Modeshapes normalized to unity
% G1:   Estimated modal participation vector matrix
% Ghye: Estimated SD matrix

function [B1, G1, Ghye] = estgamf(Ghy, B, D, K, dt)
[nr,nc,nnu] = size(Ghy);
N = (nnu-1)*2;
dom = 2*pi/(N*dt);
nd = length(diag(D));
[br, bc] = size(B);
Ghy1 = Ghy(:,:,K(1):K(2));
nk = K(2)-K(1)+1;
B1 = B*diag(1./sqrt(diag(B.'*B)));
H = bhankel(Ghy1, 1);
M = zeros(nd, br*nk);
dn = diag(D);
for k=K(1):K(2),
    Dk = diag(1./(i*(k-1)*dom - dn));
    M(:,(k-K(1))*br+1:(k-K(1)+1)*br) = Dk*B1.';
end
%G1 = (H*M1.')*pinv(M1*M1.');
G1 = H*pinv(M);
Hhe = G1*M;
Ghye = Ghy*0;
for k=K(1):K(2),
    Ghye(:,:,k) = Hhe(:,(k-K(1))*br+1:(k-K(1)+1)*br);
end