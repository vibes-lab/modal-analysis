%% Function to plot cross MAC error
function plotCrossMAC(MAC,modes1,modes2,ax)

if nargin < 4
    figure;
    ax = axes();
end
bh = bar3(ax,MAC);
for i=1:length(bh)
    set(bh(i),'cdata',...
        get(bh(i),'zdata'));
end
colormap(jet(100));
%caxis([xi xa]);
colorbar;
set(gca,'XTickLabel',num2cell(modes1),'XTick',1:numel(modes1))
set(gca,'YTickLabel',num2cell(modes2),'YTick',1:numel(modes2))
