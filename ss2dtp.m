function [Dd,Vdn,Wdn] = ss2dtp(A,C)
% SS2MODAL returns the discrete time poles and eigenvectors for a given state
% space system with a system matrix A and output matrix C (if given)

% OUTPUTS
% Dds: mx2 double, diagonal matrix of poles (eigenvectors of A) where m is
% the number of stable poles of A. 
% Vds: lxm double, matrix of eigenvectors of A as observed through the C 
% matrix. l corresponds to the number of outputs defined by C.

%% Extract discrete poles
[Vd,Dd,Wd] = eig(A); % distrete time right EVs, poles and left EVs
Ir = diag(Vd'*Wd).';
Vdn = Vd.*repmat(conj(Ir).^(-.5),size(A,1),1); %normalized right EVs
Wdn = Wd.*repmat(Ir.^(-.5),size(A,1),1); %normalized right EVs
%% If C matrix given, return output modes
if nargin == 2
    Vdn = C*Vdn; %convert the n-dimensional eigenvectos to m-dimensions
    Wdn = C*Wdn;
end