function colorDendogramBranch(h,node,clustTree,col)

if nargin < 4
    col ='r';
end

nModes = length(clustTree)+1;
I = node-nModes;

if I<1
    return
else
    h(I).Color = col;
    h(I).LineWidth = 1.5;
    colorDendogramBranch(h,clustTree(I,1),clustTree);
    colorDendogramBranch(h,clustTree(I,2),clustTree);
end
