function d = mpDist(a1,a2)
% Calculates non-dimensional distance d between two modal parameters a1 and
% a2. If a1 and a2 are n and m dimensional vectors, d is an n x m matrix of
% element distances

n1 = length(a1);
n2 = length(a2);
d = zeros(n1,n2);

for k1 = 1:n1
    for k2 = 1:n2
        d(k1,k2) = abs(a1(k1)-a2(k2))/max(abs(a1(k1)),abs(a2(k2)));
    end
end