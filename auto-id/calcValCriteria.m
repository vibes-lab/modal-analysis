function [valMatrix,valKey, valIdeal] = calcValCriteria(modObj,critList,dt)
% Calculates single-mode validation criteria for a modalp object
% Based on Reynders, et al. (2012) Fully automated (operational) modal
% analysis

nOrd = numel(modObj); %Number of model orders
modeCount = 0;

%defaults
if nargin < 2
    critList = {'fdist','zdist','poledist','imac','partfac','mtn','mpc','fcov','zcov'};
    dt = 1;
elseif nargin < 3
    dt = 1;
end
nCrit = numel(critList);

%% Initialize validation criteria matrices
nModes = sum([modObj(:).nModes]);
valMatrix = zeros(nModes-numel(modObj(1).f),nCrit);
valKey = cell(nCrit,1);
valIdeal = zeros(nCrit,1);

for k = 2:nOrd
    ordModes = numel(modObj(k).f); %number of modes in the current order
    modeI = modeCount+1:modeCount+ordModes;
    
    for c = 1:nCrit
        switch critList{c}
            case 'fdist'
                % Frequency distance
                fPrev = modObj(k-1).f;
                fCurr = modObj(k).f;
                fDist = mpDist(fPrev,fCurr);
                [fMin,fMinI] = min(fDist,[],1);
                fMinI = sub2ind(size(fDist),fMinI,1:length(fCurr));

                valMatrix(modeI,c) = fMin;
                valKey{c} = 'Frequency Distance';
                valIdeal(c) = 0;
                
            case 'zdist'
                % Damping distance
                zPrev = modObj(k-1).z;
                zCurr = modObj(k).z;
                zDist = mpDist(zPrev,zCurr);
                %[zMin,zMinI] = min(zDist,[],1);

                valMatrix(modeI,c) = zDist(fMinI);
                valKey{c} = 'Damping Distance';
                valIdeal(c) = 0;
                
            case 'poledist'    
                % Pole dist
                fPrev = modObj(k-1).f;
                fCurr = modObj(k).f;
                zPrev = modObj(k-1).z;
                zCurr = modObj(k).z;
                lamPrev = -abs(2*pi.*fPrev).*zPrev+2*1i*pi.*fPrev.*sqrt(1-zPrev.^2);
                lamCurr = -abs(2*pi.*fCurr).*zCurr+2*1i*pi.*fCurr.*sqrt(1-zCurr.^2);
                lamDist = mpDist(lamPrev,lamCurr);
                [lamMin,lamMinI] = min(lamDist,[],1);
                lamMinI = sub2ind(size(lamDist),lamMinI,1:length(lamCurr));

                valMatrix(modeI,c) = lamMin;
                valKey{c} = 'Pole Distance';
                valIdeal(c) = 0;
                
            case 'imac'
                % Mode inverse MAC
                uPrev = modObj(k-1).u;
                uCurr = modObj(k).u;
                uDist = 1-calculateMAC(uPrev,uCurr);
                [uMin,uMinI] = min(uDist,[],1);

                valMatrix(modeI,c) = uMin;%uDist(fMinI);
                valKey{c} = 'Inverse MAC';
                valIdeal(c) = 0;
                
            case 'partfact'
                % Participation factor
                valMatrix(modeI,c) = modObj(k).p(2,:);
                valKey{c} = 'Participation Factor';
                valIdeal(c) = 1;
                valMatrix(:,c) = normalizeVect(valMatrix(:,c));
            
            case 'mtn'
                % Modal transfer norm
                gamCurr = modObj(k).Gam_td;
                fCurr = modObj(k).f;
                zCurr = modObj(k).z;
                lamCurr = -abs(2*pi.*fCurr).*zCurr+2*1i*pi.*fCurr.*sqrt(1-zCurr.^2);
                for i = 1:ordModes
                    MTN = mtn(uCurr(:,i),gamCurr(:,i),lamCurr(i),dt,'accel');
                    valMatrix(modeI(i),c) = MTN;
                end
                valKey{c} = 'Modal Transfer Norm';
                valIdeal(c) = 1;
                valMatrix(:,c) = normalizeVect(valMatrix(:,c));
                
            case 'mpc'
                % Modal phase collinearity
                uCurr = modObj(k).u;
                for i = 1:ordModes
                    MPC = phaseCollinearity(uCurr(:,i));
                    valMatrix(modeI(i),c) = MPC;
                end
                valKey{c} = 'Modal Phase Collinearity';
                valIdeal(c) = 1;
                
    
            case 'fcov'
                % Frequency uncertainty
                fzCov = modObj(k).fz_cov;
                for i = 1:ordModes
                    f_cov = fzCov(1,1,i);
                    valMatrix(modeI(i),c) = f_cov;
                end
                valKey{c} = 'Frequency Covariance';
                valIdeal(c) = 0;
                valMatrix(:,c) = normalizeVect(valMatrix(:,c));
                
            case 'zcov'
                % Damping uncertainty
                fzCov = modObj(k).fz_cov;
                for i = 1:ordModes
                    z_cov = fzCov(2,2,i);
                    valMatrix(modeI(i),c) = z_cov;
                end
                valKey{c} = 'Damping Covariance';
                valIdeal(c) = 0;
                valMatrix(:,c) = normalizeVect(valMatrix(:,c));
        end
        
    end
    % Update count
    modeCount = modeCount + ordModes;
end
    