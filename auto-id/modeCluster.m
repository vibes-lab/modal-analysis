function [selectedModes,clusterStat,mainFig] = modeCluster(modalArray,opts,dt,varargin)
% MODECLUSTER stabilization diagram selection via clustering
% Takes a modeobj array representing a stabilization diagram and selects
% one set of modes as the likeliest "structural" modes in the array. It
% does so by clustering similar modes together, then keeping
% only the largest clusters and finally picking a representative mode from
% those clusters.

% [modeSelect] = MODECLUSTER(modalArray) takes a modeobj array representing
% a stabilization diagram and selects one set of modes (selectedModes).
% Each element of modalArray can have a different number of modes but the
% number of degrees of freedom must be the same.

% References
% [1] E. Reynders, J. Houbrechts, G. De Roeck, Fully automated (operational) modal analysis, Mechanical Systems and Signal Processing 29 (2012) 228?250. doi:10.1016/j.ymssp.2012.01.007.
% [2] E. Reynders, System Identification Methods for (Operational) Modal Analysis: Review and Comparison, Archives of Computational Methods in Engineering 19 (1) (2012) 51?124. doi:10.1007/s11831-012-9069-x.


%% Handle input arguments

%defaults
PLT = false;
if nargin < 3
    dt = 1;
end

%assignments
for k = 1:length(varargin)
    switch varargin{k}
        case 'plot'
            PLT = true;
    end
end

if PLT
    %% Set up the main figure
    %close all
    mainFig = figure('Position',[0 0 1200 800]);
    mainAx = axes();
    h11 = subplot(221);
    h12 = subplot(222);
    h21 = subplot(223);
    h22 = subplot(224);
    
else
    mainFig = [];
end

%% ==================== STAGE 1 ======================
% Identifies clearly spurious modes via 2-means clustering using general
% criteria, then removes them.

%% Initialize the modal parameter matrices
% Convert the modeobj array into matrices for each of the modal parameters:
% poles, mode shapes, frequency and damping (fz). This makes them easier to
% cluster.
[lamMat,ordIndx] = modeobj2array(modalArray,'pole',opts.modeRange);
[uMat,~] = modeobj2array(modalArray,'u',opts.modeRange);
[fzMat,~] = modeobj2array(modalArray,'fz',opts.modeRange);

%% Calculate the single mode validation criteria
% Mode validation criteria are various metrics for evaluating modes. See
% the references for more details.
critList = {'poledist','imac'};
[valMat,valKey,valIdeal] = calcValCriteria(modalArray,critList,dt);

%% 2-means clustering
% Separate the modes into two clusters: 1) possibly structural modes and 2)
% certainly spurrious modes. If the second cluster is two small due to
% strong outliers, then separate into three clusters. Only the first
% cluster is kept.

k = 2; % 2-means
start = [valIdeal';1-valIdeal']; %start one cluster at the ideal location
% and the other at the non-ideal location.
[cidx2,cmeans2] = kmeans(valMat,k,'dist','sqeuclidean','Start',start);

if sum(cidx2 == 2) < 5 % the second cluster is small
    k = 3;
    start = [valIdeal';1-valIdeal';1-valIdeal'];
    [cidx2,cmeans2] = kmeans(valMat,k,'dist','sqeuclidean','Start',start);
end

%% Stage 1 Visualization
if PLT
    axes(h12);
    ptsymb = {'bs','r^','md','go','c+'};
    if size(valMat,2) > 2
        % If more than two validation criteria, plotting must be limited to
        % 3 dimensions.
        
        % Order validation criteria using the deim ranking, keep top 3
        I = qdeim(valMat');
        for i = 1:k
            clust = find(cidx2==i);
            plot3(valMat(clust,I(1)),valMat(clust,I(2)),valMat(clust,I(3)),ptsymb{i});
            hold on
        end
        plot3(cmeans2(:,I(1)),cmeans2(:,I(2)),cmeans2(:,I(3)),'ko');
        plot3(cmeans2(:,I(1)),cmeans2(:,I(2)),cmeans2(:,I(3)),'kx');
        hold off
        xlabel(valKey{I(1)});
        ylabel(valKey{I(2)});
        zlabel(valKey{I(3)});
        
        view(-137,10);
    else
        for i = 1:k
            clust = find(cidx2==i);
            plot(valMat(clust,1),valMat(clust,2),ptsymb{i});
            hold on
        end
        plot(cmeans2(:,1),cmeans2(:,2),'ko');
        plot(cmeans2(:,1),cmeans2(:,2),'kx');
        hold off
        xlabel(valKey{1});
        ylabel(valKey{2});
    end
    
    title('Stage 1: Validation Criteria')
    legend('Likely Structural','Certainly Spurious','Cluster Centroid');
    grid on
end

%% Remove certainly spurious modes
%Find the number of modes used for validation criteria. This is the total
%number of modes minus the number of modes in the first model order (since
%these cannot be compared to a previous order)
numValModes = size(valMat,1);

%Remove the first model order modes from the mp matrix
totalModes = size(lamMat,1);
lamMat = lamMat(totalModes-numValModes+1:end,:);
fzMat = fzMat(totalModes-numValModes+1:end,:);
uMat = uMat(totalModes-numValModes+1:end,:);
ordIndx = ordIndx(totalModes-numValModes+1:end,:);

% The first cluster represents potentially physical modes from stage 1
clust1 = find(cidx2==1);
lamMat_1 = lamMat(clust1,:);
fzMat_1 = fzMat(clust1,:);
uMat_1 = uMat(clust1,:);
valMat_1 = valMat(clust1,:);
ordIndx_1 = ordIndx(clust1,:);

%% Apply hard validation criteria
crit1 = fzMat_1(:,2)<0.1; %damping upper bound
crit2 = fzMat_1(:,2)>0; %damping lower bound
critTot = crit1 & crit2;

lamMat_1 = lamMat_1(critTot,:);
fzMat_1 = fzMat_1(critTot,:);
uMat_1 = uMat_1(critTot,:);
valMat_1 = valMat_1(critTot,:);
ordIndx_1 = ordIndx_1(critTot,:);

%% ================= STAGE 2 ======================
% Separates modes into "mode clusters" based on the proximity of their
% poles using hierachical clustering. The total number of clusters is
% chosen by splitting those whose centroids are further that 3 std apart
%% Hierarchical clustering
%Calculate distance matrix between all modes extracted from stage 1.
%NOTE: In this case we must calculate distances between all modes in the
%set. In stage 1, distance was only calculated for the closest mode in the
%previous model order
lamD = mpDist(lamMat_1(:,1),lamMat_1(:,1)); %pole distances
lamD = lamD/max(max(lamD));
modeD = 1 - calculateMAC(uMat_1',uMat_1'); %Modal distance, in terms of MAC
mpD = lamD+modeD; %Complete distance matrix
mpD(mpD <= 10*eps) = 0;
mpD_vec = squareform(mpD);
clustTreeMp = linkage(mpD_vec,'average');
%% Cutoff the tree based on a distance threshold
% Calculate cluster threshold from single mode validation criteria
freqi = find(strcmp(critList,'poledist'));
imaci = find(strcmp(critList,'imac'));
distI = [freqi,imaci]; %Which criteria to use for stage 2
dSum = sum(valMat_1(:,distI),2);
dThresh = mean(dSum)+3*std(dSum);

%Cutoff
hidx = cluster(clustTreeMp,'criterion','distance','cutoff',dThresh);

%% Stage 2 visualization
if PLT
    axes(h21);
    dendrogram(clustTreeMp,0);
    hold on
    plot(xlim,[dThresh,dThresh],'r:');
    h_gca = gca;
    h_gca.TickDir = 'out';
    h_gca.TickLength = [.002 0];
    %h_gca.XTickLabel = [];
    title('Stage 2: Hierarchical Clustering')
    xlabel('Mode Number')
    ylabel('Node Distance');
end

%% =================== STAGE 3 ==========================
% Selects the final mode clusters by assuming that those sets with most
% modes contain the physical modes. Modes are separated by 2-means
% clustering on the number of cluster elements.

%% Find the number of modes (elements) in each set
nSets = max(hidx); %number of mode sets/clusters from stage 2
nElem = zeros(nSets,1); %initialize vector of set sizes
modeSetI = cell(nSets,1); %initialize cell of mode indeces

for i = 1:nSets
    modeSet = find(hidx==i); %indeces of modes belonging to set i
    modeSetI{i} = modeSet;
    nElem(i) = numel(modeSet); %number of elements in set i
end

nEmpty = sum(nElem>max(nElem)/5); %additional empty sets, for robustness
nElemx = [nElem;zeros(nEmpty,1)];

%% 2-means clustering on number of elements
k = 2;
start = [max(nElemx);0];
[cidx3,~] = kmeans(nElemx,k,'dist','sqeuclidean','Start',start);

selectI = cidx3 == 1; %index of the physical mode sets

%% Stage 3 Visualization
if PLT
    axes(h22);
    nSets = numel(nElem);
    [nElem_sort,I_sort] = sort(nElemx,'descend');
    selI_sort = selectI(I_sort);
    index = 1:nSets+nEmpty;
    
    hb1 = bar(index(selI_sort),nElem_sort(selI_sort),'b');
    hold on
    hb2 = bar(index(~selI_sort(1:nSets)),nElem_sort(~selI_sort(1:nSets)),'r');
    xlabel('Cluster Number')
    ylabel('Number of Modes')
    b_gca = gca;
    b_gca.XTickLabel = [];
    legend([hb1 hb2],{'Physical Mode Set','Non Physical Mode Set'})
    title('Stage 3: Elimination of Small Clusters ');
end

%% Chose representative modes
% Create the final modeobj of selected modes by selecting the
% representative mode for each cluster (the mode with the median damping
% value).
clear selectedModes
nSel = sum(selectI); %number of selected modes
ndof = size(uMat,2);
selectedModes = modeobj(nSel,ndof);
f_sel = zeros(nSel,1);
z_sel = zeros(nSel,1);
u_sel = zeros(ndof,nSel);
OI = zeros(ndof,2);
f_median = zeros(nSel,1);
z_median = zeros(nSel,1);
f_std = zeros(nSel,1);
z_std = zeros(nSel,1);
u_std = zeros(ndof,nSel);
clustSize = zeros(nSel,1);
clust_f = cell(nSel,1);
clust_z = cell(nSel,1);

modeSetI_sel = modeSetI(selectI);
for i = 1:nSel % for all selected mode clusters
    indxf = modeSetI_sel{i}; %get matrix indices
    
    %statistical properties of cluster
    clust_f{i} = fzMat_1(indxf,1);
    clust_z{i} = fzMat_1(indxf,2);
    f_median(i) = median(fzMat_1(indxf,1));
    z_median(i) = median(fzMat_1(indxf,2));
    f_std(i) = std(fzMat_1(indxf,1));
    z_std(i) = std(fzMat_1(indxf,2));
    u_std(:,i)= std(uMat_1(indxf,:),0,1);
    clustSize(i) = numel(indxf);
    
    
    %find the index of the representative mode (median damping)
    [~,medI] = min(abs(fzMat_1(indxf,2)-z_median(i)));
    
    f_sel(i) = fzMat_1(indxf(medI),1);
    z_sel(i) = fzMat_1(indxf(medI),2);
    u_sel(:,i) = uMat_1(indxf(medI),:);
    OI(i,:) = ordIndx_1(indxf(medI),:);
    
end

selectedModes.f = f_sel;
selectedModes.z = z_sel;
selectedModes.u = u_sel;
[selectedModes, isort] = selectedModes.sort();

clusterStat.f_median = f_median(isort);
clusterStat.z_median = z_median(isort);
clusterStat.f_std = f_std(isort);
clusterStat.z_std = z_std(isort);
clusterStat.u_std = u_std(isort);
clusterStat.ordIndx = OI(isort,:);
clusterStat.clustSize = clustSize(isort);
clusterStat.clust_f = clust_f(isort);
clusterStat.clust_z = clust_z(isort);

 %% ================ Stabilization diagram  ==================
if PLT
    stabDiagram(h11,modalArray,opts.modeRange,'StabOnly');
    hold on
    for i = 1:nSel
        fi = selectedModes.f(i);
        %zi = selectedModes.z(i);
        ordi = clusterStat.ordIndx(i,1);
        
        plot(h11,fi,ordi,'o','MarkerEdgeColor','k','MarkerSize',10);
    end
    grid on
    title('Selected Modes')
end