function index = cellContains(C,value)

index = false(1,numel(C));
for k = 1:numel(C)
    list = C{k};
    match = list == value;
    index(k) = sum(match) > 0;
end