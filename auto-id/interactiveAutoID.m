filedir = '/home/sarlo/newriver/VTSIL/Goodwin/Windy_Weekend_Apr_1st/SSI_COV/';
dataFile = 'OMA_AccelData_Apr02to03_fulltwodays_Apr-02-2016_22-30-00_2axNo5f_mode_Dur3600_1to7ssi_cov_fast_pc6.mat';
%load([filedir dataFile]);
errorbars = true;

%% Set up the main figure
%close all
mainFig = figure('Position',[0 0 1200 800]);
mainAx = axes();
h11 = subplot(221);
h12 = subplot(222);
h21 = subplot(223);
h22 = subplot(224);

%% ================ Plot stabilization diagram  ===================
axes(h11);
plotStabDiagram(OMA.modalp,OMAT.empirical,OMA.settings);
title('Stabilization Diagram');

%% ==================== STAGE 1 ======================
%% Calculate the single mode validation criteria
critI = [1:9];
modelOrder = OMA.settings.modeMin:OMA.settings.modeMax;
[lamMat, uMat, ~, ordIndx] = modalp2matrix(OMA.modalp,'pole',modelOrder);
[fzpMat, ~, covMat, ~] = modalp2matrix(OMA.modalp);
[valMat,valKey,valIdeal] = calcValCriteria(OMA.modalp,1/OMAT.fs);
valMat = valMat(:,critI);
valIdeal = valIdeal(critI);
valKey = valKey(critI);

%% 2-means clustering
k = 2;
start = [valIdeal';1-valIdeal'];
[cidx2,cmeans2] = kmeans(valMat,k,'dist','sqeuclidean','Start',start);
figure
[silh2,h] = silhouette(valMat,cidx2,'sqeuclidean');

%% Correlation
corrC = corrcoef(valMat);
disp(corrC);
[PC,I] = projChan(valMat,3,'threshold',0);

%% Stage 1 Plot
axes(h12);
ptsymb = {'bs','r^','md','go','c+'};
for i = 1:k
    clust = find(cidx2==i);
    plot3(valMat(clust,I(1)),valMat(clust,I(2)),valMat(clust,I(3)),ptsymb{i});
    hold on
end
plot3(cmeans2(:,I(1)),cmeans2(:,I(2)),cmeans2(:,I(3)),'ko');
plot3(cmeans2(:,I(1)),cmeans2(:,I(2)),cmeans2(:,I(3)),'kx');
hold off
xlabel(valKey{I(1)});
ylabel(valKey{I(2)});
zlabel(valKey{I(3)});
title('Stage 1')
view(-137,10);
grid on

%% Remove certainly spurious modes
%Find the number of modes used for validation criteria. This is the total
%number of modes minus the number of modes in the first model order (since
%these cannot be compared to a previous order)
numValModes = size(valMat,1);
%Remove the first model order modes from the mp matrix
totalModes = size(lamMat,1);
lamMat = lamMat(totalModes-numValModes+1:end,:);
fzpMat = fzpMat(totalModes-numValModes+1:end,:);
covMat = covMat(totalModes-numValModes+1:end,:);
uMat = uMat(totalModes-numValModes+1:end,:);
ordIndx = ordIndx(totalModes-numValModes+1:end,:);
% The first cluster represents potentially physical modes from stage 1
clust1 = find(cidx2==1);
lamMat_1 = lamMat(clust1,:);
fzpMat_1 = fzpMat(clust1,:);
covMat_1 = covMat(clust1,:);
uMat_1 = uMat(clust1,:);
valMat_1 = valMat(clust1,:);
ordIndx_1 = ordIndx(clust1,:);

%% Apply hard validation criteria
crit1 = fzpMat_1(:,2)<0.1; %damping upper bound
crit2 = fzpMat_1(:,2)>0.005; %damping lower bound
crit3 = valMat_1(:,7)>0.7;
critTot = crit1 & crit2;

lamMat_1 = lamMat_1(critTot,:);
fzpMat_1 = fzpMat_1(critTot,:);
uMat_1 = uMat_1(critTot,:);
valMat_1 = valMat_1(critTot,:);
ordIndx_1 = ordIndx_1(critTot,:);
covMat_1 = covMat_1(critTot,:);

%% ================= STAGE 2 ======================
%% Hierarchical clustering

%Calculate distance matrix between all modes extracted from stage 1.
%NOTE: In this case we must calculate distances between all modes in the
%set. In stage 1, distance was only calculated for the closest mode in the
%previous model order
lamD = mpDist(lamMat_1(:,1),lamMat_1(:,1)); %pole distances
modeD = 1 - calculateMAC(uMat_1',uMat_1'); %Modal distance, in terms of MAC
mpD = lamD+modeD; %Complete distance matrix
mpD_vec = squareform(mpD);
clustTreeMp = linkage(mpD_vec,'average');
cophenet(clustTreeMp,mpD_vec)

%% Cutoff the tree based on a distance threshold
% Calculate cluster threshold from single mode validation criteria
distI = [3,4]; %Which criteria to use for stage 2 (3 = pole dist,4 = mode dist)
dSum = sum(valMat_1(:,distI),2);
dThresh = mean(dSum)+2*std(dSum);

%Cutoff
hidx = cluster(clustTreeMp,'criterion','distance','cutoff',dThresh);

%% Cutoff the tree based on an inconsistency threshold
% Calculate cluster threshold from single mode validation criteria
depth = length(clustTreeMp);
Z = inconsistent(clustTreeMp,depth);
zThresh = mean(Z(:,4))+1.5*std(Z(:,4));

%Cutoff
%hidx = cluster(clustTreeMp,'cutoff',zThresh,'depth',depth);

%% Stage 2 plot
axes(h21);
[dendh,nodes,outperm] = dendrogram(clustTreeMp,0);
hold on
plot(xlim,[dThresh,dThresh],'r:');
h_gca = gca;
h_gca.TickDir = 'out';
h_gca.TickLength = [.002 0];
%h_gca.XTickLabel = [];
title('Stage 2')
xlabel('Mode Number')
ylabel('Node Distance');


%% =================== STAGE 3 ==========================
%% Find the number of modes (elements) in each set
% Presumably, sets with most modes are the most "physical"
nSets = max(hidx); %number of mode sets/clusters from stage 2
nElem = zeros(nSets,1); %initialize vector of set sizes
modeSetI = cell(nSets,1); %initialize cell of mode indeces

pltsym = {'bs','r^','md','go','c+'};
for i = 1:nSets
    modeSet = find(hidx==i); %indeces of modes belonging to set i
    modeSetI{i} = modeSet;
    nElem(i) = numel(modeSet); %number of elements in set i
end

nEmpty = sum(nElem>max(nElem)/5); %additional empty sets, for robustness
nElem = [nElem;zeros(nEmpty,1)];

%% 2-means clustering
k = 2;
start = [max(nElem);0];
[cidx3,cmeans3] = kmeans(nElem,k,'dist','sqeuclidean','Start',start);

physI = cidx3 == 1; %index of the physical mode sets

figure
[nElem_sort,I_sort] = sort(nElem,'descend');
physI_sort = physI(I_sort);
index = 1:nSets+nEmpty;

hb1 = bar(nElem_sort(physI_sort),'k');
hold on
hb2 = bar(index(~physI_sort(1:nSets)),nElem_sort(~physI_sort(1:nSets)),'w');
xlabel('Cluster Number')
ylabel('Number of Elements')
b_gca = gca;
b_gca.XTickLabel = [];
legend([hb1 hb2],{'Physical Mode Set','Non Physical Mode Set'})

%% Get the physical modes

physModeI = modeSetI(physI); %cell of mode indeces

clear physModes
physModes(sum(physI)).fzpMat = []; %structure to hold physical modes

for i = 1:sum(physI)
    indx = physModeI{i};
    
    z_feat = 2*sqrt(covMat_1(indx,2));
    f_feat = 2*sqrt(covMat_1(indx,1));
%     z_std = 2*sqrt(fzpMat_1(indx,2));
%     f_std = 2*sqrt(fzpMat_1(indx,1));

    featVec = [f_feat z_feat];
    
    [Sig,Mu,Dmcd,Outmcd] = robustcov(featVec);
    
    Dmahal = pdist2(featVec,mean(featVec,1),'mahal');
    p = size(featVec,2);
    chi2quantile = sqrt(chi2inv(0.975,p));
    
    figure
    subplot(212)
    plot(Dmahal, Dmcd, 'o')
    line([chi2quantile, chi2quantile], [0, max(Dmcd)], 'color', 'r')
    line([0, max(Dmahal)], [chi2quantile, chi2quantile], 'color', 'r')
    hold on
    plot(Dmahal(Outmcd), Dmcd(Outmcd), 'r+')
    xlabel('Mahalanobis Distance')
    ylabel('Robust Distance')
    title(['DD Plot, FMCD method, ' num2str(fzpMat_1(indx(medI),1)) ' Hz']);
    hold off
    
    subplot(211)
    gscatter(featVec(:,1),featVec(:,2),Outmcd,'br','ox')
    legend({'Not outliers','Outliers'})
    title(['Data Scatter, ' num2str(fzpMat_1(indx(medI),1)) ' Hz']);

    
    indxf = indx(~Outmcd);
    
    medz = median(fzpMat_1(indxf,2));
    [~,medI] = min(abs(fzpMat_1(indxf,2)-medz));
    
    physModeI{i} = indxf;
    physModes(i).fzpMat = fzpMat_1(indxf,:);
    physModes(i).uMat = uMat_1(indxf,:);
    physModes(i).ordIndx = ordIndx_1(indxf,:);
    
    physModes(i).repfzp = fzpMat_1(indxf(medI(1)),:);
    physModes(i).repu = uMat_1(indxf(medI(1)),:);
    physModes(i).repOrdIndx = ordIndx_1(indxf(medI(1)),:);
end

%% Final plot
axes(h22);
%col = brewermap(sum(physI),'Set3');
col = jet(sum(physI));
for i = 1:sum(physI)
    fzpMat_t = physModes(i).fzpMat;
    repMode = physModes(i).repfzp;
    ord = physModes(i).ordIndx(:,1);
    repOrd = physModes(i).repOrdIndx(:,1);
    %plot3(fzpMat_t(:,1),fzpMat_t(:,2)*100,ord,'.','MarkerEdgeColor',col(i,:));
    ydelt = 2*sqrt(covMat_1(physModeI{i},2))*100;
    xdelt = 2*sqrt(covMat_1(physModeI{i},1));
   if errorbars
        errorbar(fzpMat_t(:,1),fzpMat_t(:,2)*100,ydelt,ydelt,xdelt,xdelt,...
            'o','MarkerFaceColor',col(i,:),'MarkerEdgeColor','k',...
            'Color',col(i,:),'MarkerSize',5);
    else
        plot(fzpMat_t(:,1),fzpMat_t(:,2)*100,'o',...
            'MarkerFaceColor',col(i,:),'MarkerEdgeColor','k','MarkerSize',5);
    end
    
    hold on
    %plot3(repMode(:,1),repMode(:,2)*100,repOrd,'o','MarkerEdgeColor','k');
    plot(repMode(:,1),repMode(:,2)*100,'+','MarkerEdgeColor','k');
    
end
xlabel('Frequency [Hz]');
ylabel('Damping [%]');
title('Stage 3: Final Results')
grid on


%% Interactive
[nodeIDs,nodePos] = clustPos(clustTreeMp,outperm);
nModes = numel(outperm);
nNodes = numel(nodeIDs);

while true
    dcm_obj = datacursormode(mainFig);
    set(dcm_obj,'DisplayStyle','datatip',...
        'SnapToDataVertex','on','Enable','on')
    
    disp('--> Use data cursor to select a mode. Use ctrl C to exit.');
    % Wait while the user does this.
    pause
    
    c_info = getCursorInfo(dcm_obj);
    c_pos = c_info.Position;
    
    %datacursormode off
    y = c_pos(2);
    x = c_pos(1);
    
    switch c_info.Target.Parent
        case h11
            
        case h12
            
        case h21
            X1 = x==nodePos(:,1);
            Y1 = y==nodePos(:,2);
            nid = find(X1&Y1);
            
            if ~isempty(nid)
                if exist('branchNodes','var')
                    for j = 1:length(branchClusters)
                    dendI = branchClusters(j)-nModes;
                    dendh(dendI).Color = 'b';
                    dendh(dendI).LineWidth = 1;
                    end
                end
                [branchNodes,branchClusters] = getDendogramBranch(nid,clustTreeMp);
                %colorDendogramBranch(dendh,nid,clustTreeMp);
                for j = 1:length(branchClusters)
                    dendI = branchClusters(j)-nModes;
                    dendh(dendI).Color = 'r';
                    dendh(dendI).LineWidth = 1.5;
                end
                
                f = fzpMat_1(branchNodes,1);
                z = fzpMat_1(branchNodes,2);
                o = ordIndx_1(branchNodes,1);
                if exist('currh22','var'); delete(currh22)
                end
                axes(h22); hold on
                currh22 = plot3(f,z*100,o,'or','MarkerSize',4);
                %currh22 = plot(f,z*100,'or','MarkerSize',4);
                
                if exist('currh11','var'); delete(currh11)
                end
                axes(h11); hold on
                currh11 = plot(f,o,'+r','MarkerSize',8,...
                    'Linewidth',2);
            else
                disp('Not a valid cluster node.')
            end
            
        case h22
            %find the corresponding modal parameters and plot mode
            F1 = round(fzpMat_1(:,1),6) == round(x,6);
            Z1 = round(fzpMat_1(:,2),6) == round(y/100,6);
            T = F1&Z1;
            modeI = find(T);
            U1 = uMat_1(modeI,:);
            
            %Plot the mode
            geoFile = 'Animation Code/GoodwinSimplifiedGeometry_no5fl.xlsx';
            [mode, P, L,S] = buildGoodwinMode(U1,DATAp,geoFile);
            plotGoodwinColor(mode,8,P,L,S)
            fig = gcf;
            title([num2str(x) ' Hz']);
            
            %% Animate
%             M = animateGoodwinColor_Modal(mode,x,-5,P,L,S,1,30);
%             fps = 16;
%             movie(M,5,fps)
            
            %Mode info
            groupI = cellContains(physModeI,modeI);
            groupIs = physModeI{groupI};
            
            %Show current             
            if exist('currh21_2','var'); delete(currh21_2)
            end
            if exist('currh21_3','var'); delete(currh21_3)
            end
            axes(h21); hold on
            currh21_2 = plot(find(outperm == modeI),0,'dk','MarkerSize',10,...
            'LineWidth',2);
            pnodes = zeros(numel(groupIs));
            for k = 1:numel(groupIs)
                pnodes(k) = find(outperm == groupIs(k));
            end
            currh21_3 = plot(pnodes,0*pnodes,'xk','MarkerSize',6,...
            'LineWidth',2);
            
            if exist('currh11_2','var'); delete(currh11_2)
            end
            if exist('currh11_3','var'); delete(currh11_3)
            end
            axes(h11); hold on
            currh11_2 = plot(x,ordIndx_1(modeI,1),'dk','MarkerSize',10,...
            'LineWidth',2);
            currh11_3 = plot(fzpMat_1(groupIs,1),ordIndx_1(groupIs,1),'xk','MarkerSize',8,...
                    'Linewidth',2);
    
            disp(['Mode ID: ' num2str(modeI)]);
            disp(['Freq Error (95%): ' num2str(2*sqrt(covMat_1(modeI,1)))]);
            disp(['Damp Error (95%): ' num2str(2*sqrt(covMat_1(modeI,2)))]);
            %disp(['Rep Inverse MAC: ' num2str(1-calculateMAC(uMat_1(modeI,:)',physModes(groupI).repu'))]);
            disp(['Mean Pole Distance: ' num2str(mean(lamD(modeI,groupIs)))]);
            disp(['Mean Inverse MAC: ' num2str(mean(modeD(modeI,groupIs)))]);
            
            disp(['Cluster ID: ' num2str(find(groupI))]);
            disp(['Cluster Frequency Variation (95%): ' num2str(2*std(fzpMat_1(groupIs,1)))]);
            disp(['Cluster Damping Variation (95%): ' num2str(2*std(fzpMat_1(groupIs,2)))]);
            
    end
end
