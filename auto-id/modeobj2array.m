function [modMat, mi] = modeobj2array(modObj,feat,I)
% MODEOBJ2MAT Convert modeobj to a pair of matrices

% [modMat, mi] = MODEOBJ2MAT(modObj) Converts a modeobj into a Nx2 array of
% frequencies and damping ratios If modObj is a vector, then the elements  
% of modeObj are stacked. The vector mi denotes the mode number (mi(:,1))
% and also element number (mi(:,1)) of each entry in modMat.

% MODEOBJ2MAT(modObj,feat) converts based on a specific feature of the mode
% object:
%   'fz' (default)   : modMat is Nx2 natural frequency (rad) and damping
%       ratios
%   'poles'          : modMat is Nx1 complex poles
%   'u'              : modMat is Nxdof complex mode shapes
%   'pf'             : modMat is Nx1 participation factors
%   'dfdz'           : modMat is Nx2 natural frequency (rad) and damping
%       ratios uncertainties
%   'du'             : modMat is dofxN complex mode shape uncertainties

% MODEOBJ2MAT(modObj,feat,I) converts only for the I indeces of modObj.
% Default is all elements.

nElem = numel(modObj); %Number of modeobj

if nargin < 2
    feat = 'fz';
    I = 1:nElem;
elseif nargin < 3
    I = 1:nElem;
end

%% Initialize validation critera matrices
nModes = sum([modObj(:).nModes]); % total number of modes
dof = numel(modObj(1).u(:,1)); % number of dofs
modeCount = 0; %mode counter

switch feat
    case 'fz'
        modMat = zeros(nModes,2);
    case 'pole'
        modMat = zeros(nModes,1);
    case 'u'
        modMat = zeros(nModes,dof);
    case 'pf'
        modMat = zeros(nModes,1);
    case 'dfdz'
        modMat = zeros(nModes,2);
    case 'du'
        modMat = zeros(nModes,dof);
    case 'dpole'
        modMat = zeros(nModes,1);
end

mi = zeros(nModes,2);

for k = 1:nElem
    elemModes = numel(modObj(k).f); %number of modes in the current order
    modeI = modeCount+1:modeCount+elemModes;
    
    switch feat
        case 'fz'
            val = [modObj(k).f, modObj(k).z];
        case 'pole'
            f = modObj(k).f;
            z = modObj(k).z;
            val = -abs(2*pi.*f).*z+2*1i*pi.*f.*sqrt(1-z.^2);
        case 'u'
            val = modObj(k).u';
        case 'pf'
            val = modObj(k).pf;
        case 'dfdz'
            val = [modObj(k).df, modObj(k).dz];
        case 'du'
            val = modObj(k).du;
        case 'dpole'
            f_cov = modObj(k).df^2;
            z_cov = modObj(k).dz^2;
            val = sqrt(-abs(2*pi.*f_cov).*z_cov+2*1i*pi.*f_cov.*sqrt(1-z_cov.^2));
    end
    
    %Update modMat
    modMat(modeI,:) = val;
    %Update model/element index
    mi(modeI,1) = repmat(I(k),elemModes,1);
    mi(modeI,2) = 1:elemModes;
    
    % Update count
    modeCount = modeCount + elemModes;
end