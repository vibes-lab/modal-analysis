function MTN = mtn(phi,g,lam_c,dt,meas_type)

switch meas_type
    case 'disp'
        phi = phi/lam_c;
    case 'accel'
        phi = phi*lam_c;
    case 'vel'
        %phi = phi;
end

lam_d = exp(lam_c*dt);
w = abs(lam_c);
z = exp(1i*w*dt);
val1 = phi*g'/(z-lam_d);
val2 = phi*g'/(2*lam_d);

[~,sig,~] = svd(val1+val2);
MTN = max(diag(sig));