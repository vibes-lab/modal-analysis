function [nodes,clusters] = getDendogramBranch(node,clustTree)


nModes = length(clustTree)+1;
I = node-nModes;

if I<1
    nodes = node;
    clusters=[];
else
    [nodes1,clusters1] = getDendogramBranch(clustTree(I,1),clustTree);
    [nodes2,clusters2] = getDendogramBranch(clustTree(I,2),clustTree);
    nodes = [nodes1,nodes2];
    clusters = [clusters1,clusters2,node];
end
