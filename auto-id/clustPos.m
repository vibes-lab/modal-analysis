function [nodeID,pos] = clustPos(clustTree,outperm)

%returns the node positions (pos) on the dendrogram plot for clustTree. Can
%be used to find a cluster ID based on the cursor position on the plot.

% Initialize arrays
N = length(clustTree);
nNodes = max(max(clustTree(:,1:2)));
nodeID = 1:nNodes;
pos = zeros(nNodes,2);

nodeID(1:N+1) = 1:N+1;
[~,srtI] = sort(outperm,'ascend');
pos(1:N+1,1) = srtI;

for k = 1:N
    node1 = clustTree(k,1);
    node2 = clustTree(k,2);
    dist = clustTree(k,3);
    
    id = k+N+1;
    I1 = node1==nodeID;
    I2 = node2==nodeID;
    
    pos(id,1) = (pos(I1,1)+pos(I2,1))/2;
    pos(id,2) = dist;
end
    
    