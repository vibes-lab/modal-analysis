function D = pdistFromMean(X,dist)

mu = mean(X,1);
N = length(X);
D = zeros(N,1);
C = nancov(X);
Cinv = pinv(C);

switch dist
    case 'mahalanobis'
        for k = 1:N
            D(k) = sqrt((X(k,:)-mu)*Cinv*(X(k,:)-mu)');
        end
end