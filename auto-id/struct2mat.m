function [fzpMat, uMat] = struct2mat(struct)
% Converts the modalp structure into two matrices, one for frequency,
% damping and participation (3xN), the other for mode shapes (dofxN), where
% N is the toal number of modes. Each column corresponds to a mode. 
% Passing in type = 'pole' will return an fzp matrix
% that is 2 x N, which has complex pole instead of freq and damping
% (participation remains the same)

nOrd = numel(struct); %Number of model orders
modeCount = 0;

if nargin < 2
    type = 'fz';
end

%% Initialize validation critera matrices
nModes = numel([modalp(:).f]);
dof = numel(modalp(1).u(:,1));

if strcmp(type,'pole')
    fzpMat = zeros(nModes,2);
else
    fzpMat = zeros(nModes,3);
end
uMat = zeros(nModes,dof);

for k = 1:nOrd
    ordModes = numel(modalp(k).f); %number of modes in the current order
    modeI = modeCount+1:modeCount+ordModes;
    
    %Freq and damping
    f = modalp(k).f;
    z = modalp(k).z;
    p = modalp(k).p(1,:);
    
    if strcmp(type,'pole')
        lam = -abs(2*pi.*f).*z+2*1i*pi.*f.*sqrt(1-z.^2);
        fzpMat(modeI,1) = lam;
        fzpMat(modeI,2) = p;
    else
        fzpMat(modeI,1) = f;
        fzpMat(modeI,2) = z;
        fzpMat(modeI,3) = p;
    end
    
    u = modalp(k).u;
    uMat(modeI,:) = u';
    
    % Update count
    modeCount = modeCount + ordModes;
end
    