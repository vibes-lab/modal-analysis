% [B1,G1,Re] = estgamt(R,B,D)
%Estimates the modal participation vector and determines the best fit to 
%the correlation function (CF) matrix. Matrices R and R1 are CF 3D matrices 
%so that R(r,c,k) is the CF matrix at the discrete time lag k, where r is 
%the row entry and c is the column entry. If N modes are present, then the
%diagonal eigenvalue matrix D is 2N x 2N and the matrices B, B1 and G1 are
%all R x 2N where R is the number of measurement points. The returned mode
%shape matrix B1 has mode shapes normalized to unit length and the G1
%contains the correponding modal amplitude vectors.
%R:     Data CF matrix 
%B:     Mode shapes with any scaling
%D:     diagonal matrix containing the discrete eigenvalues (discrete time poles)
%B1:    Same mode shapes as in B but normalized to unit lenght
%G1:    Modal participation matrix correponding to B1
%Re:    Estimated CF matrix

%All rights reserved. Rune Brincker, Aug 2012.

function [B1,G1,Re] = estgamt(R,B,D)
nd = length(diag(D));
[nr,nc,nk] = size(R);
B1 = B*diag(1./sqrt(diag(B.'*B)));
H = bhankel(R, 1);
M = zeros(nd, nc*nk);
for k=1:nk,
    M(:,(k-1)*nc+1:k*nc) = D^(k-1)*B1.';
end
G1 = (H*M.')*pinv(M*M.')/(2*pi);
He = 2*pi*real(G1*M);
Re = R*0;
for k=1:nk;
    Re(:,:,k) = He(:,(k-1)*nc+1:k*nc); % eq 9.7
end
