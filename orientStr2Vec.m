function [orientVec] = orientStr2Vec(orientStr)

N = length(orientStr);
orientVec = zeros(N,3);
for k = 1:N
    switch orientStr{k}
        case 'N'
            orientVec(k,2) = 1;
        case 'S'
            orientVec(k,2) = -1;
        case 'E'
            orientVec(k,1) = 1;
        case 'W'
            orientVec(k,1) = -1;
        case 'U'
            orientVec(k,3) = 1;
        case 'D'
            orientVec(k,3) = -1;
        case 'Y'
            orientVec(k,2) = 1;
        case 'X'
            orientVec(k,1) = 1;
        case 'Z'
            orientVec(k,3) = 1;
    end
end